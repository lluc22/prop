package domini;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;


public class Sortida implements Iterable<Integer> {
	List<GrafWiki> Comunitats;
	List<String> Seguiment_alg;  
	List<List<Integer>> LlistaComunitats;
	long temps;
	
	public void AfegirComunitat() {
                GrafWiki auxGW = new GrafWiki();
                Comunitats.add(auxGW);
                List<Integer> auxLI = new ArrayList<Integer>();
                LlistaComunitats.add(auxLI);
	}
	
	public Sortida() {
		Comunitats = new ArrayList<GrafWiki>();
		Seguiment_alg = new ArrayList<String>();
		LlistaComunitats = new ArrayList<List<Integer>>();
	}

        public void reiniciar() {
                Comunitats.clear();
                Seguiment_alg.clear();
                LlistaComunitats.clear();
                temps = 0;
        }
	
	public int NombreComunitats() {
		return Comunitats.size();
	}
	
	public int OrdreComunitat(int index) {
		return Comunitats.get(index).ordre();
	}
	
	public void AfegirPas(String pas_actual) {
		Seguiment_alg.add(pas_actual);
	}
	
	public List<String> Consultar_passos () {
		return Seguiment_alg;
	}
	
	public List<GrafWiki> Comunitats_resultat() {
		return Comunitats;
	}
	
	public List<List<Integer>> LlistaComunitats() {
                return LlistaComunitats;
	}
	
	public void afegir_nodes(List<List<Integer>> llista){
		LlistaComunitats = llista;
	}
	public void Crear_comunitats(Graf<ElemWiki,Relacio,String> g1, Graf<ElemWiki,ArestaPes,Double> g2) throws Exception {

		for (int i = 0; i < LlistaComunitats.size(); i++) {

                        List<ElemWiki> l = new ArrayList<ElemWiki>();
                        for (int j = 0; j < LlistaComunitats.get(i).size(); j++) {
                                l.add(g2.vertex(LlistaComunitats.get(i).get(j)));
                        }
                        GrafWiki aux = new GrafWiki();
			for (int j = 0; j < LlistaComunitats.get(i).size(); j++) {
                                try {
                                        aux.AfegirNode(l.get(j).NomNode(), l.get(j).quisoc());
                                }
                                catch (DadesIncorrectesException e) {}
                                List<Parell<ElemWiki,List<String>>> adj = g1.ArestesAdjacentsDirigit(l.get(j));
                                for (int k = 0; k < adj.size(); k++) {
                                        if (l.contains(adj.get(k).ObtenirPrimer())) {
                                                for (int p = 0; p < adj.get(k).ObtenirSegon().size(); p++) {
                                                        LiniaWiki lw = new LiniaWiki(l.get(j).NomNode(), l.get(j).quisoc(), adj.get(k).ObtenirSegon().get(p), adj.get(k).ObtenirPrimer().NomNode(), adj.get(k).ObtenirPrimer().quisoc());
                                                        aux.AfegirLinia(lw);
                                                }
                                        }
                                }
                                
			}
			Comunitats.add(aux);
		}
	}
	
	public void AfegirTemps(long t){
		temps = t;
	}
	
	public long TempsCPU(){
		return temps;
	}
	
	public class itvert implements Iterator<Integer> {
        
                private int actual;
                private int llista;
                
                public itvert() {
                        llista = 0;
                        actual = 0;
                }
                
                public itvert(int i) {
                        llista = i;
                        actual = 0;
                }
                
                public boolean hasNext() {
                    if (actual < LlistaComunitats.get(llista).size()) return true;
                    return false;
                }
                
                public Integer next() {
                        actual++;
                        return LlistaComunitats.get(llista).get(actual-1);
                }
                
                public void remove() {
                        LlistaComunitats.get(llista).remove(actual);
                }
        }
        
        public Iterator<Integer> iterator() {
                return new itvert();
        }
        
        public Iterator<Integer> iterator(int i) {
                return new itvert(i);
	}
}
