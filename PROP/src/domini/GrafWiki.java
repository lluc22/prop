package domini;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

public class GrafWiki implements Iterable {

	private List<LiniaWiki> ConjLinies;
	private Arbre page;
	private Arbre cat;
	private int arestesB;
	private int idlinia;
	private int pagines;
	private int categories;
	private int idelem;
	
	// Constructora
	public GrafWiki(){
		ConjLinies = new ArrayList<>();
		pagines = 0;
		categories = 0;
		idlinia = 0;
		page = new Arbre();
		cat = new Arbre();
                arestesB = 0;
                idelem = 0;
	}
	
	// Consultores
	public int mida() {
		return (ConjLinies.size() - arestesB);
	}
	
	public int ordre() {
                return (pagines + categories);
	}
	
	public int ordrePags() {
                return pagines;
	}
	
	public int ordreCats() {
                return categories;
	}
	
	// Modificadores
	public void AfegirLinia(LiniaWiki l) throws DadesIncorrectesException {
	
                if (ComprovarCorrectesa(l)) {
                        if (l.ObtenirNodeOrigen().equals(l.ObtenirNodeDesti()) && l.ObtenirTipusNodeOrigen().equals(l.ObtenirTipusNodeDesti())) {
                                throw new DadesIncorrectesException("Un node no pot tenir una aresta dirigida a ell mateix");
                        }
                        if (l.ObtenirTipusNodeOrigen().equals("page")) {
                                if (page.AfegirEnterOrigen(l.ObtenirNodeOrigen(),idelem,idlinia)) {
                                        ++pagines;
                                        ++idelem;
                                }
                        }
                        else if (l.ObtenirTipusNodeOrigen().equals("cat")) {
                                if (cat.AfegirEnterOrigen(l.ObtenirNodeOrigen(),idelem,idlinia)) {
                                        ++categories;
                                        ++idelem;
                                }
                        }
                        else throw new DadesIncorrectesException("No existeix el tipus" + l.ObtenirTipusNodeOrigen());
                        
                        if (l.ObtenirTipusNodeDesti().equals("page")) {
                                if (page.AfegirEnterDesti(l.ObtenirNodeDesti(),idelem,idlinia)) {
                                        ++pagines;
                                        ++idelem;
                                } 
                        }
                        else if (l.ObtenirTipusNodeDesti().equals("cat")) {
                                if (cat.AfegirEnterDesti(l.ObtenirNodeDesti(),idelem,idlinia)) {
                                        ++categories;
                                        ++idelem;
                                }
                        }
                        else throw new DadesIncorrectesException("No existeix el tipus" + l.ObtenirTipusNodeDesti());
                        
                        ConjLinies.add(l);
                        idlinia++;
                }
                else throw new DadesIncorrectesException("L'aresta té una combinació de tipus errònia");
	}
        
        public void ModificarLinia(LiniaWiki l) throws DadesIncorrectesException {
                if (l.ObtenirContingut().equals("CsubC")) {
                        l.CanviarContingut("CsupC");
                        if (!ExisteixLinia(l)) {
                            l.CanviarContingut("CsubC");
                            Integer aux = TrobarLinia(l);
                            ConjLinies.get(aux).CanviarContingut("CsupC");
                        }
                        else throw new DadesIncorrectesException("Ja hi ha una aresta del tipus CsupC");
                }
                else if (l.ObtenirContingut().equals("CsupC")) {
                        l.CanviarContingut("CsubC");
                        if (!ExisteixLinia(l)) {
                                l.CanviarContingut("CsupC");
                                Integer aux = TrobarLinia(l);
                                ConjLinies.get(aux).CanviarContingut("CsubC");
                        }
                        else throw new DadesIncorrectesException("Ja hi ha una aresta del tipus CsubC");
                }
                else throw new DadesIncorrectesException("No es pot modificar aquest tipus de linia");
	} 
        
	public void EsborrarLinia(LiniaWiki l) throws DadesIncorrectesException {
                Integer aux = TrobarLinia(l);
                if (aux >= 0) {
                        if (l.ObtenirTipusNodeOrigen().equals("page")) page.EsborrarEnterOrigen(l.ObtenirNodeOrigen(),aux);
                        else if (l.ObtenirTipusNodeOrigen().equals("cat")) cat.EsborrarEnterOrigen(l.ObtenirNodeOrigen(),aux);
                        else throw new DadesIncorrectesException("No existeix el tipus" + l.ObtenirTipusNodeOrigen());
                        
                        if (l.ObtenirTipusNodeDesti().equals("page")) page.EsborrarEnterDesti(l.ObtenirNodeDesti(),aux);
                        else if (l.ObtenirTipusNodeDesti().equals("cat")) cat.EsborrarEnterDesti(l.ObtenirNodeDesti(),aux);
                        else throw new DadesIncorrectesException("No existeix el tipus" + l.ObtenirTipusNodeDesti());
                        
                        LiniaWiki buida = null;
                        ConjLinies.set(aux,buida);
                        arestesB++;
                }
                else throw new DadesIncorrectesException("L'aresta no existeix");
	}
	
	public boolean ExisteixLinia(LiniaWiki l) throws DadesIncorrectesException {
                int i = -1;
                try {
                        i = TrobarLinia(l);
                }
                catch (DadesIncorrectesException e) {
                        throw e;
                }
                if (i > -1) return true;
                return false;
	}
	
	public void AfegirNode(String node, String tipus) throws DadesIncorrectesException {
                if (tipus.equals("page")) {
                        if (page.AfegirNode(node,idelem)) {
                                ++pagines;
                                ++idelem;
                        }
                        else throw new DadesIncorrectesException("Pàgina ja existent");
                }
                else if (tipus.equals("cat")) {
                        if (cat.AfegirNode(node,idelem)) {
                                ++categories;
                                ++idelem;
                        }
                        else throw new DadesIncorrectesException("Categoria ja existent");
                }
                else throw new DadesIncorrectesException("No existeix el tipus" + tipus);
	}
	
	public void EsborrarNode(String node, String tipus) throws DadesIncorrectesException {
                Parell<List<Integer>,List<Integer>> p = new Parell<List<Integer>,List<Integer>>();
                if (tipus.equals("page")) {
                        int id = page.ConsultarIdentificadorNode(node);
                        if (id > -1) {
                                p = page.ConsultarEntersNode(node);
                                page.EsborrarNode(node);
                                --pagines;
                        }
                        else throw new DadesIncorrectesException("Pàgina inexistent");
                }
                else if (tipus.equals("cat")) {
                        int id = cat.ConsultarIdentificadorNode(node);
                        if (id > -1) {
                                p = cat.ConsultarEntersNode(node);
                                cat.EsborrarNode(node);
                                --categories;
                        }
                        else throw new DadesIncorrectesException("Categoria inexistent");
                }
                else throw new DadesIncorrectesException("No existeix el tipus" + tipus);
                
                for (int i = 0; i < p.ObtenirPrimer().size(); i++) {
                        int j = p.ObtenirPrimer().get(i);
                        if (ConjLinies.get(j).ObtenirTipusNodeDesti().equals("page")) {
                                page.EsborrarEnterDesti(ConjLinies.get(j).ObtenirNodeDesti(),j);
                                LiniaWiki l = null;
                                ConjLinies.set(j,l);
                                arestesB++;
                        }
                        else {
                                cat.EsborrarEnterDesti(ConjLinies.get(j).ObtenirNodeDesti(),j);
                                LiniaWiki l = null;
                                ConjLinies.set(j,l);
                                arestesB++;
                        }
                }
                for (int i = 0; i < p.ObtenirSegon().size(); i++) {
                        int j = p.ObtenirSegon().get(i);
                        if (ConjLinies.get(j).ObtenirTipusNodeOrigen().equals("page")) {
                                page.EsborrarEnterOrigen(ConjLinies.get(j).ObtenirNodeOrigen(),j);
                                LiniaWiki l = null;
                                ConjLinies.set(j,l);
                                arestesB++;
                        }
                        else {
                                cat.EsborrarEnterDesti(ConjLinies.get(j).ObtenirNodeOrigen(),j);
                                LiniaWiki l = null;
                                ConjLinies.set(j,l);
                                arestesB++;
                        }
                }
	}
	
	public void ModificarNomNode(String node, String tipus, String nounom) throws DadesIncorrectesException {
                if (tipus.equals("page")) {
                        int i = page.ModificarNomNode(node,nounom);
                        if (i > -1) {
                                Parell<List<Integer>,List<Integer>> p = page.ConsultarEntersNode(nounom);
                                for (int j = 0; j < p.ObtenirPrimer().size(); j++) {
                                        ConjLinies.get(p.ObtenirPrimer().get(j)).CanviarNodeOrigen(nounom);
                                }
                                for (int j = 0; j < p.ObtenirSegon().size(); j++) {
                                        ConjLinies.get(p.ObtenirSegon().get(j)).CanviarNodeDesti(nounom);
                                }
                        }
                        else if (i == -1) throw new DadesIncorrectesException("Pàgina inexistent");
                        else throw new DadesIncorrectesException("Nom de pàgina ja existent");
                }
                else if (tipus.equals("cat")) {
                        int i = cat.ModificarNomNode(node,nounom);
                        if (i > -1) {
                                Parell<List<Integer>,List<Integer>> p = cat.ConsultarEntersNode(nounom);
                                for (int j = 0; j < p.ObtenirPrimer().size(); j++) {
                                        ConjLinies.get(p.ObtenirPrimer().get(j)).CanviarNodeOrigen(nounom);
                                }
                                for (int j = 0; j < p.ObtenirSegon().size(); j++) {
                                        ConjLinies.get(p.ObtenirSegon().get(j)).CanviarNodeDesti(nounom);
                                }
                        }
                        else if (i == -1) throw new DadesIncorrectesException("Categoria inexistent");
                        else throw new DadesIncorrectesException("Nom de categoria ja existent");
                }
                else throw new DadesIncorrectesException("No existeix el tipus" + tipus);
	}
        
        private Integer TrobarLinia(LiniaWiki l) throws DadesIncorrectesException {
        
                Parell<List<Integer>,List<Integer>> p1;
                Parell<List<Integer>,List<Integer>> p2;
                if (l.ObtenirTipusNodeOrigen().equals("page")) {
                    p1 = page.ConsultarEntersNode(l.ObtenirNodeOrigen());
                }
                else if (l.ObtenirTipusNodeOrigen().equals("cat")) {
                    p1 = cat.ConsultarEntersNode(l.ObtenirNodeOrigen());
                }
                else throw new DadesIncorrectesException("No existeix el tipus" + l.ObtenirTipusNodeOrigen());
                
                if (l.ObtenirTipusNodeDesti().equals("page")) {
                    p2 = page.ConsultarEntersNode(l.ObtenirNodeDesti());
                }
                else if (l.ObtenirTipusNodeDesti().equals("cat")) {
                    p2 = cat.ConsultarEntersNode(l.ObtenirNodeDesti());
                }
                else throw new DadesIncorrectesException("No existeix el tipus" + l.ObtenirTipusNodeDesti());
                
                if (p1.ObtenirPrimer() != null && p2.ObtenirSegon() != null) {
                        for (int i = 0; i < p2.ObtenirSegon().size(); i++) {
                                if (p1.ObtenirPrimer().contains(p2.ObtenirSegon().get(i))) {
                                        if(ConjLinies.get(p2.ObtenirSegon().get(i)).equals(l)) return p2.ObtenirSegon().get(i);
                                }
                        }
                }
                return -1;
        }
        
        public class itArestes implements Iterator<LiniaWiki> {
        
                private final LiniaWiki lw;
                private int actual;
                
                public itArestes() {
                        lw = null;
                        actual = 0;
                }
                
                public boolean hasNext() {
                        if (ConjLinies.size() > actual) {
                                for (int i = actual; i < ConjLinies.size(); i++) {
                                        if (ConjLinies.get(i) != null) return true;
                                }
                                return false;
                        }
                        return false;
                }
                
                public LiniaWiki next() {
                        for (int i = actual; i < ConjLinies.size(); i++) {
                                if (ConjLinies.get(i) != null) {
                                        actual = i + 1;
                                        return ConjLinies.get(i);
                                }
                        }
                        return null;
                }
                
                public void remove() {
                        LiniaWiki l = null;
                        ConjLinies.set(actual,l);
                }
        }
        
        public Iterator<LiniaWiki> iterator() {
                return new itArestes();
	}
	
	public class itPags implements Iterator<String> {
        
                private final Iterator<String> it;
                
                public itPags() {
                        it = page.iterator();
                }
                
                public boolean hasNext() {
                        return it.hasNext();
                }
                
                public String next() {
                        return it.next();
                }
                
                public void remove() {}
        }
	
	public Iterator<String> iteratorPags() {
                return new itPags();
	}
	
	public class itCats implements Iterator<String> {
        
                private final Iterator<String> it;
                
                public itCats() {
                        it = cat.iterator();
                }
                
                public boolean hasNext() {
                        return it.hasNext();
                }
                
                public String next() {
                        return it.next();
                }
                
                public void remove() {}
        }
	
	public Iterator<String> iteratorCats() {
                return new itCats();
	}
	
	
	
	public boolean ComprovarCorrectesa(LiniaWiki l) throws DadesIncorrectesException {
	
                if (l.ObtenirTipusNodeOrigen().equals("page")) {
                        if (l.ObtenirContingut().equals("CsubC")) return false;
                        if (l.ObtenirContingut().equals("CsupC")) return false;
                        if (l.ObtenirContingut().equals("CP")) return false;
                }
                else if (l.ObtenirTipusNodeOrigen().equals("cat")) {
                        if (l.ObtenirContingut().equals("PC")) return false;
                        if (l.ObtenirContingut().equals("PP")) return false;
                }
                else throw new DadesIncorrectesException("No existeix aquest tipus");
                
                if (l.ObtenirTipusNodeDesti().equals("page")) {
                        if (l.ObtenirContingut().equals("CsubC")) return false;
                        if (l.ObtenirContingut().equals("CsupC")) return false;
                        if (l.ObtenirContingut().equals("PC")) return false;
                }
                else if (l.ObtenirTipusNodeDesti().equals("cat")) {
                        if (l.ObtenirContingut().equals("CP")) return false;
                        if (l.ObtenirContingut().equals("PP")) return false;
                        
                }
                else throw new DadesIncorrectesException("No existeix aquest tipus");
                return true;
	
	}
}
