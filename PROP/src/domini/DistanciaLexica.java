package domini;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class DistanciaLexica extends AlgorismeDistancia {
	
	ArrayList<Integer> arrels; //Guarda els vertex on no hi arriba cap link tipus CsubC o CP
	double MAX = 1;
	double MIN = 0;
	double maxim_pes = 1.0;
	public DistanciaLexica(Graf<ElemWiki, Relacio, String> g) {
		super(g);
		arrels = new ArrayList<Integer>();
	}
	
	public void convert() throws Exception {

		super.convert();
		int n = graf_wiki.ordre();
		int d = 0; //Aquesta variable guarda la profunditat en tot moment
		boolean[] visitats = new boolean[n];
		
		Integer d_global[] = new Integer[n]; //Guarda la profunditat mes petita entre la de tots els nodes a cada arrel
		Integer d_local[] = new Integer[n]; //Guarda la profunditat a entre els nodes i una arrel
		Integer inf = Integer.MAX_VALUE;
		Arrays.fill(d_global, inf);
		trobar_arrels_transformar_graf();

		//Comencem una cerca per cada arrel
		for(int i = 0; i < arrels.size(); ++i){
			d = 0;
			Parell<Integer,Integer> p = new Parell<Integer,Integer>(); //Parell vertex,profunditat
			p.CanviarPrimer(arrels.get(i));
			p.CanviarSegon(d);
			LinkedList<Parell<Integer,Integer>> q = new LinkedList<Parell<Integer,Integer>>(); //cua
			Arrays.fill(visitats,false);
			visitats[arrels.get(i)] = true;
			q.add(p);
			d_global[arrels.get(i)] = 0; //les arrels sempre tindran profunditat 0
			Arrays.fill(d_local, 0);
			while(!q.isEmpty()){
				
				Parell<Integer,Integer> actual = q.remove();
				List<Integer> adjacents = new ArrayList<Integer>();
				adjacents = graf_wiki.VertexAdjacentsNum(actual.ObtenirPrimer());
				
				d = actual.ObtenirSegon();
				++d;
				
				for(int j = 0; j < adjacents.size();++j){
					
					int vei = adjacents.get(j);
					List<Parell<Integer, List<String>>> adjacentsv = null;
					adjacentsv = graf_wiki.ArestesAdjacentsDirigitNum(vei);
				
					List<Parell<Integer, List<String>>> adjacentsact = null;
					adjacentsact = graf_wiki.ArestesAdjacentsDirigitNum(actual.ObtenirPrimer());
				
					if(adjacentsact.get(j).ObtenirSegon().contains("PP")){ //pagina-pagina
						 int num = 0;
						 if(!graf_pes.ExisteixAresta(graf_wiki.vertex(actual.ObtenirPrimer()), graf_wiki.vertex(vei))){
								 for(int i1 = 0; i1 < adjacentsact.size() ; ++i1){
									 for(int j1 = 0; j1 < adjacentsv.size(); ++j1){
										 if(adjacentsact.get(i1).ObtenirSegon().get(0).equals("PC")){
											 if(graf_wiki.vertex(adjacentsv.get(j1).ObtenirPrimer()).NomNode().equals(graf_wiki.vertex(adjacentsact.get(i1).ObtenirPrimer()).NomNode())) ++num;
										 }
									 }
								 }
								 double pes = ((double)num) / (Math.min(adjacentsact.size(),adjacentsv.size()));
								 graf_pes.AfegirAresta(graf_wiki.vertex(actual.ObtenirPrimer()), pes, graf_wiki.vertex(vei));
							 }
						
					}
					
					
					else{
						boolean entrar;
						if(!visitats[vei]){
							//Cas en que els vertex no els haviem visitat
							d = actual.ObtenirSegon() +1;
							d_local[vei] = d;
							visitats[vei] = true;
							Parell<Integer,Integer>pvei = new Parell<Integer,Integer>();
							pvei.CanviarPrimer(vei);
							pvei.CanviarSegon(d);
							q.add(pvei);
							entrar = (d < d_global[vei]); // actualitzarem el pes si hi ha una millora en la profunditat del node
							
						}
						
						else{ 
							//Cas en que els vertex ja son visitats
							d = actual.ObtenirSegon();
							entrar = (d <= d_global[actual.ObtenirPrimer()] && d <= d_global[vei]) && (d == d_local[vei]); //actualitzarem si la profunditat dels dos nodes es millor a la global.
						}
						
						
						
	
						if(entrar){ //actualitzarem el pes ja que la profunditat es millor que la global
							if(d!=actual.ObtenirSegon()) d_global[vei] = d; //actualitzem la global nomes si el node es nou
							
						
							double pes_actual;
							double pes_vei;
	
							if(adjacentsact.get(j).ObtenirSegon().contains("CsubC") || adjacentsact.get(j).ObtenirSegon().contains("CP") ){
								//Cas en que el link es CsubC o CP
								int vei_sup = 0; //links CsupC o CP
								int actual_sub = 0; //links CsubC o PC
								
								for(int k = 0; k < adjacentsv.size(); ++k ){
									for(int k1 = 0; k1 < adjacentsv.get(k).ObtenirSegon().size();++k1){
										if(adjacentsv.get(k).ObtenirSegon().get(k1).equals("CsupC") || adjacentsv.get(k).ObtenirSegon().get(k1).equals("PC") ){
											++vei_sup;
										}
									}
								}
								
								for(int k = 0; k < adjacentsact.size(); ++k ){
									for(int k1 = 0; k1 < adjacentsact.get(k).ObtenirSegon().size();++k1){
										if(adjacentsact.get(k).ObtenirSegon().get(k1).equals("CsubC") || adjacentsact.get(k).ObtenirSegon().get(k1).equals("CP") ){
											++actual_sub;
										}
									}
								}
								
								
								pes_actual = MAX - ((MAX-MIN)/(double) actual_sub);
								pes_vei = MAX - ((MAX-MIN)/(double) vei_sup);
	
							}
							
							else{   //Cas en que link es CsupC o PC
	
								int vei_sub = 0;
								int actual_sup = 0;
								
								for(int k = 0; k < adjacentsv.size(); ++k ){
									for(int k1 = 0; k1 < adjacentsv.get(k).ObtenirSegon().size();++k1){
										if(adjacentsv.get(k).ObtenirSegon().get(k1).equals("CsubC") || adjacentsv.get(k).ObtenirSegon().get(k1).equals("CP") ){
											++vei_sub;
										}
									}
								}
								
								
								
								for(int k = 0; k < adjacentsact.size(); ++k ){
									for(int k1 = 0; k1 < adjacentsact.get(k).ObtenirSegon().size();++k1){
										if(adjacentsact.get(k).ObtenirSegon().get(k1).equals("CsupC") || adjacentsact.get(k).ObtenirSegon().get(k1).equals("PC") ){
											++actual_sup;
										}
									}
								}
								
								
								pes_actual = MAX - ((MAX-MIN)/(double) actual_sup);
								pes_vei = MAX - ((MAX-MIN)/(double) vei_sub);
	
							}
							
							double pes = ((pes_actual + pes_vei)/ (double) d);
					
							if(pes >= maxim_pes) maxim_pes = pes;
							if(!graf_pes.ExisteixAresta(graf_wiki.vertex(actual.ObtenirPrimer()), graf_wiki.vertex(vei))){
									//L'aresta encara no existia
									graf_pes.AfegirAresta(graf_wiki.vertex(actual.ObtenirPrimer()), pes, graf_wiki.vertex(vei));
								}
								
								else{  //aresta existeix pero s'ha de modificar el pes
									graf_pes.BorrarAresta(graf_wiki.vertex(actual.ObtenirPrimer()),graf_wiki.vertex(vei));
									graf_pes.AfegirAresta(graf_wiki.vertex(actual.ObtenirPrimer()), pes, graf_wiki.vertex(vei));
								}
							
						}
					}

				}
					

				
			}
		}

		recorregut_final(); //normalitza la distancia de 0 a 1
	}
	
	//Troba arrels amb grau zero i transforma el graf per poder aplicar distancia.
	private void trobar_arrels_transformar_graf() throws Exception{
		int n = graf_wiki.ordre();
		boolean[] visitats = new boolean[n];
		Arrays.fill(visitats,false);
		int[] grau_entrada = new int[n]; //Guarda el grau d'entrada de cada vertex
		Arrays.fill(grau_entrada,0);
		
		for(int i = 0; i < n; ++i){

			LinkedList<Integer> q = new LinkedList<Integer>();
			if(!visitats[i]){
				q.add(i);
				visitats[i] = true;
			}
		
			while(!q.isEmpty()){
				int actual = q.remove();

				List<Parell<Integer,List<String>>> adjacents = graf_wiki.ArestesAdjacentsDirigitNum(actual);
				graf_pes.AfegirVertex(graf_wiki.vertex(actual));
				
				for(int j = 0; j < adjacents.size();++j){
					int vei = adjacents.get(j).ObtenirPrimer();
					
					//Si no estan visitats s'afageixen
					
					int index_CsubC = 0;
					int index_CsupC = 0;
					int index_CP = 0;
					int index_PC = 0;
					int index_PP = 0;
					for(int k = 0 ; k < adjacents.get(j).ObtenirSegon().size();++k){
						String vei_link = adjacents.get(j).ObtenirSegon().get(k);
						graf_pes.AfegirVertex(graf_wiki.vertex(vei));
						
						List<Parell<Integer,List<String>>> adjacentsv = graf_wiki.ArestesAdjacentsDirigitNum(vei);
						
						if(vei_link.equals("CsubC") || vei_link.equals("CP") ||  vei_link.equals("PP")){
							
							++grau_entrada[vei];
							if(vei_link.equals("CsubC")){	
								boolean trobat = false;
								for(int i1 = 0; i1 < adjacentsv.size(); ++i1){
									if(adjacentsv.get(i1).ObtenirPrimer()==actual){
										for(int i2 = index_CsupC; i2 < adjacentsv.get(i1).ObtenirSegon().size();++i2){
											if(adjacentsv.get(i1).ObtenirSegon().get(i2).equals("CsupC")){
												trobat = true;
												index_CsupC = i2;
												break;
											}
										}
									}
								}
								
								if(!trobat){
								
									graf_wiki.AfegirAresta(graf_wiki.vertex(vei), "CsupC", graf_wiki.vertex(actual));
								}
							}
							
							else if(vei_link.equals("CP")) {
								boolean trobat = false;
								for(int i1 = 0; i1 < adjacentsv.size(); ++i1){
									if(adjacentsv.get(i1).ObtenirPrimer()==actual){
										for(int i2 = index_PC; i2 < adjacentsv.get(i1).ObtenirSegon().size();++i2){
											if(adjacentsv.get(i1).ObtenirSegon().get(i2).equals("PC")){
												trobat = true;
												index_PC = i2;
												break;
											}
										}
									}
								}
								
								if(!trobat){
									
									graf_wiki.AfegirAresta(graf_wiki.vertex(vei), "PC", graf_wiki.vertex(actual));
								}
								
							}
							
							else if(vei_link.equals("PP")) {
								boolean trobat = false;
								for(int i1 = 0; i1 < adjacentsv.size(); ++i1){
									if(adjacentsv.get(i1).ObtenirPrimer()==actual){
										for(int i2 = index_PC; i2 < adjacentsv.get(i1).ObtenirSegon().size();++i2){
											if(adjacentsv.get(i1).ObtenirSegon().get(i2).equals("PP")){
												trobat = true;
												index_PC = i2;
												break;
											}
										}
									}
								}
								
								if(!trobat){
									
									graf_wiki.AfegirAresta(graf_wiki.vertex(vei), "PP", graf_wiki.vertex(actual));
								}
								
							}
							
						}
						else{
							
							
							if(vei_link.equals("CsupC")){	
								boolean trobat = false;
								for(int i1 = 0; i1 < adjacentsv.size(); ++i1){
									if(adjacentsv.get(i1).ObtenirPrimer()==actual){
										for(int i2 = index_CsubC; i2 < adjacentsv.get(i1).ObtenirSegon().size();++i2){
											if(adjacentsv.get(i1).ObtenirSegon().get(i2).equals("CsubC")){
												trobat = true;
												index_CsubC = i2;
												break;
											}
										}
									}
								}
								
								if(!trobat){
									
									graf_wiki.AfegirAresta(graf_wiki.vertex(vei), "CsubC", graf_wiki.vertex(actual));
								}
							}
							
							else{
								boolean trobat = false;
								for(int i1 = 0; i1 < adjacentsv.size(); ++i1){
									if(adjacentsv.get(i1).ObtenirPrimer()==actual){
										for(int i2 = index_CP; i2 < adjacentsv.get(i1).ObtenirSegon().size();++i2){
											if(adjacentsv.get(i1).ObtenirSegon().get(i2).equals("CP")){
												trobat = true;
												index_CP = i2;
												break;
											}
										}
									}
								}
								
								if(!trobat){
									
									graf_wiki.AfegirAresta(graf_wiki.vertex(vei), "CP", graf_wiki.vertex(actual));
								}
								
							}
							
							
						}
						if(!visitats[vei]){
							
							visitats[vei] = true;
							q.add(vei);
							
						}
						
					}

				}
			}
		}
		
		for(int i1 = 0; i1 < n; ++i1){
			if(grau_entrada[i1] == 0)arrels.add(i1);
		}

		if(arrels.isEmpty()){

			throw new GrafSenseArrelsException("El teu graf no te arrels, no pots aplicar distancia lexica");
			
		}
		
	}
	
	//A partir de la distancia maxima calcua la similaritud entre nodes(0 a 1) a exepcio de links PP.
	
	//recorrer nodes de 0 a n-1
	private void recorregut_final() throws DadesIncorrectesException{
		int n = graf_pes.ordre();
		for(int i = 0; i < n ; ++i){
			List<Parell<Integer, Double>> adjacents = graf_pes.ArestesAdjacentsNum(i);
			for(int j = 0; j < adjacents.size();++j){
				
				if(adjacents.get(j).ObtenirPrimer() > i && !(graf_pes.vertex(i).quisoc().equals("page") && graf_pes.vertex(adjacents.get(j).ObtenirPrimer()).quisoc().equals("page") )){
						
						graf_pes.BorrarArestaNum(i, adjacents.get(j).ObtenirPrimer());
						
						graf_pes.AfegirArestaNum(i, 1.0 - adjacents.get(j).ObtenirSegon()/maxim_pes, adjacents.get(j).ObtenirPrimer());
						
					}
			}
		}
			
	}
	



}
