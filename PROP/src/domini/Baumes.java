package domini;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Baumes extends AlgorismeCommunityDetection {
	
	public Baumes(Entrada En, Sortida S) throws Exception {
		try {
                        CercarComunitats(En,S);
                }
                catch (Exception e) {
                        throw e;
                }
	}
	
	private double FuncioDensitat(int ordre, double PesGraf) throws DadesIncorrectesException {
		
		/**	Retorna el calcul de la densitat donat l'ordre i el pes 
		 * total de les arestes
		 */
		if (ordre > 1) {
			int n = (ordre*(ordre-1))/2; 
			return ((PesGraf*ordre)/n);
		}
		else return 0;
	}
	
	private double GuanyPerNode(Graf G, List<Integer> Comunitat, int node) throws Exception {
		
		/** Retorna la suma dels pesos de les arestes de la comunitat
		 * 	que son adjacents al node que li passem com a parametre. 	  
		 */
		double aux = 0;
		List<Parell<Integer,Double>> l = new ArrayList<Parell<Integer,Double>>();
		try {
                        l = G.ArestesAdjacentsNum(node);
		}
		catch (Exception e) {
                        throw e;
		}
		for (int i = 0; i < l.size(); i++) {
			if (Comunitat.contains(l.get(i).ObtenirPrimer())) {
				aux += (double)l.get(i).ObtenirSegon();
			}
		}
		return aux;
	}
	
	private void RefinamentComunitat(Graf G, List<Integer> C, Double pes, Sortida S) throws Exception {
		
		/** Aquest metode busca l'ampliacio o reduccio de la comunitat a partir de l'addicio o esborrat
		 * de nodes de la propia comunitat o que son adjacents a ella intentant que la funcio de 
		 * modularitat augmenti. Primer crea una llista amb els nodes de la comunitat mes els adjacents
		 * que no son de la comunitat. Despres, per cadascun d'aquests comprova si afegint-lo (pels que
		 * no formen part de la comunitat) o esborrant-lo (pels que si que ho son) augmenta la funcio de 
		 * densitat. Si es aixi, mante el canvi. 
		 */
		
		int ordre = C.size();
		double w = 0.0;
		try {
                        w = FuncioDensitat(ordre,pes);
                }
                catch (DadesIncorrectesException e) {
                        throw e;
                }
		boolean increased = true;			// Ens indica si la densitat ha augmentat o no
		
		// Cada iteracio dins d'aquest bucle comprova si els veins de la comunitat poden millorar la funcio de densitat
		// Si en alguna iteracio no ho fan, s'acaba el refinament de la comunitat
		while (increased) {

			List<Integer> N = new ArrayList<Integer>(C);
			for (int j = 0; j < C.size(); j++) {
				 
				List<Parell<Integer,Double>> llista = new ArrayList<Parell<Integer,Double>>();
				try {
                                        llista = G.ArestesAdjacentsNum(C.get(j));
				}
				catch (DadesIncorrectesException e) {
                                        throw e;
				}
				for (int k = 0; k < llista.size(); k++) {
					if (!C.contains(llista.get(k).ObtenirPrimer())) {
						N.add(llista.get(k).ObtenirPrimer());
					}
				}
				// A N tenim tots els vertexs de la comunitat mes els vertexs que son adjacents a aquests i que no
				// es trobaven dins de la comunitat 
			}
			
			for (int j = 0; j < N.size(); j++) {
				
				// Per cada node que hi ha a N comprovem si millora la densitat de la comunitat. Per fer-ho, 
				// si el node que mirem de N no forma part de la comunitat, l'afegim i mirem si millora la
				// funcio. Pels nodes que si que formen part de la comunitat fem el contrari, els eliminem
				// i comprovem si millora la funcio. 
				double pesnode = 0;
				
				// Calculem el valor de les arestes que son adjacents al node N.get(j)
				List<Parell<Integer,Double>> llista = new ArrayList<Parell<Integer,Double>>();
				try {
                                        llista = G.ArestesAdjacentsNum(N.get(j));
				}
				catch (DadesIncorrectesException e) {
                                        throw e;
				}
				for (int k = 0; k < llista.size(); k++) {
					if (N.contains(llista.get(k).ObtenirPrimer())) {
						pesnode += llista.get(k).ObtenirSegon();
					}
				}
				
				boolean afegit;
				if (C.contains(N.get(j))) afegit = false;		// El node es troba dins de C
				else afegit = true;								// El node NO es troba dins de C
				
				double w1 = 0.0;
				try {
                                        w1 = FuncioDensitat(C.size(),pes);		// Calcul de funcio de densitat abans d'afegir/d'eliminar node
				}
				catch (DadesIncorrectesException e) {
                                        throw e;
                                }        
				// Calcul de funcio de densitat despres d'afegir/d'eliminar node
				double w2;
				if (afegit) {
                                        try {
                                                w2 = FuncioDensitat(C.size()+1,pes+pesnode);
                                        }
                                        catch (DadesIncorrectesException e) {
                                                throw e;
                                        }
				}	
				else {
                                        try {
                                                w2 = FuncioDensitat(C.size()-1,pes-pesnode);
                                        }
                                        catch (DadesIncorrectesException e) {
                                                throw e;
                                        }
                                }        
				// Comparar les dues funcions de densitat
				if (w2 > w1) {
					if (afegit) {
						// Afegir el node
						String s = "Afegir node " + G.vertex(N.get(j));
						S.AfegirPas(s);
						C.add(N.get(j));
						pes +=pesnode;
					}
					else {
						// Esborrar el node
						String s = "Esborrar node " + G.vertex(N.get(j));
						S.AfegirPas(s);
						C.remove((Object)N.get(j));
						pes -= pesnode;
					}
				}
			}
			
			// Comparar funcio de densitat despres del proces d'addicio i eliminacio de nodes
			double waux = 0.0;
			try {
                                waux = FuncioDensitat(C.size(),pes);
			}
			catch (DadesIncorrectesException e) {
                                throw e;
			}
			if (w == waux) increased = false;
			else w = waux;
		}
	}
	
	protected void CercarComunitats(Entrada En, Sortida S) throws Exception {
		
		/** Primer part de l'algorisme: consisteix en crear un conjunt de clusters.
		 *  Per cada node del graf mirarem si hi ha un cluster el qual amb l'addicio
		 *  del node en questio s'obtingui un resultat de la funcio de densitat mes gran 
		 *  que sense ell. Si es aixi l'afegim. Si no trobem cap cluster on aixo passi, 
		 *  creem un nou cluster amb nomes un node.
		 */
		List<List<Integer>> Comunitat = new ArrayList<List<Integer>>();
		// Llista per guardar la suma dels pesos de les arestes de la comunitat
		List<Double> PesGraf = new ArrayList<Double>();
		
		String s = "- Comenca cerca de clusters:";
		S.AfegirPas(s);
		int n = En.Graf().ordre();
		for (int i = 0; i < n; i++) {
			boolean afegit = false;
			for (int j = 0; j < Comunitat.size(); j++) {
				double aux = 0.0;
				try {
                                        aux = GuanyPerNode(En.Graf(), Comunitat.get(j), i);
				}
				catch (Exception e) {
                                        throw e;
				}
				int ordre = Comunitat.get(j).size();
				double pes = (double)PesGraf.get(j);
				double ValorVell = 0.0; 
				try {
                                        ValorVell = FuncioDensitat(ordre,pes);
				}
				catch (DadesIncorrectesException e) {
                                        throw e;
                                }
                                double ValorNou = 0.0;
                                try {
                                        ValorNou = FuncioDensitat(ordre + 1, pes + aux);
                                }
                                catch (DadesIncorrectesException e) {
                                        throw e;
                                }
				if (ValorVell < ValorNou) {		// Si es compleix la condicio podem afegir el node a la comunitat
					String s1 = "Afegir vertex " + En.Graf().vertex(i) + " al cluster " + (j+1);
					S.AfegirPas(s1);
					PesGraf.set(j, PesGraf.get(j).doubleValue() + aux);			// Incrementa el pes total de les arestes de la comunitat
					Comunitat.get(j).add(i);							// Afegeix el node a la comunitat
					afegit = true; 			// Marca que el node s'ha afegit com a minim a una comunitat
				}
			}
			if (!afegit) {		// Si es compleix la condicio hem de crear una nova comunitat (nova llista de nodes)
				String s1 = "Crear cluster " + (Comunitat.size()+1) + " amb node " + En.Graf().vertex(i);
				S.AfegirPas(s1);
				List<Integer> aux = new ArrayList<Integer>();
				aux.add(i);						
				Comunitat.add(aux);				// Afegir nova llista amb nodes que formen comunitat
				PesGraf.add(0.0);				// El pes inicial es 0
			}
		}

		/** Segona part de l'algorisme: consisteix en acabar de definir les comunitats.
		 * 	Hem de comprovar per cada cluster si afegint o eliminat nodes veins podem
		 *  obtenir un augment de la funcio de densitat. 
		 */
		
		String s2 = "- Comenca refinament de clusters per crear comunitats:";
		S.AfegirPas(s2);
		for (int i = 0; i < Comunitat.size(); i++) {
			
			String s1 = "Refinament del cluster " + (i+1);
			S.AfegirPas(s1);
			RefinamentComunitat(En.Graf(), Comunitat.get(i), PesGraf.get(i), S);
			Collections.sort(Comunitat.get(i));
			
		}
		
		
		// Per ultim cal eliminar les possibles comunitats que finalment han resultat ser exactament iguales.
		String s3 = "- Comenca la cerca i esborrat de comunitats iguals";
		S.AfegirPas(s3);
		int i = 0;
		while (i < Comunitat.size()) {
			int j = i + 1; 
			while (j < Comunitat.size()) {
				if (Comunitat.get(i).size() == Comunitat.get(j).size()) {
					int k = 0; 
					boolean iguales = true;
					while (k < Comunitat.get(i).size() && iguales) {
						if (Comunitat.get(i).get(k) != Comunitat.get(j).get(k)) iguales = false;
						k++;
					}
					if (iguales) Comunitat.remove(j);
					else j++;
				}
				else j++;
			}
			i++;
		}
		S.afegir_nodes(Comunitat);
	}
}
