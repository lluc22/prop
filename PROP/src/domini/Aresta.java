package domini;

public class Aresta<V, E> {
	V NodeOrigen;
	E contingut;
	V NodeDesti;
	
	//construcotres
	/**Es crea una aresta this.NodeOrigen = vertexO, this.NodeDesti = vertexD i contingut = cont*/
	public Aresta(V vertexO ,V vertexD, E cont){
		NodeOrigen = vertexO;
		NodeDesti = vertexD;
		contingut = cont;
	}
	/** Es crea una aresta buida*/
	public Aresta(){}
	//modificadores

	/** contingut = pes*/
	public void ModificarContingut(E c){
		contingut = c;
	}

	//consultores
	/** Retorna el contingut de l'aresta*/
	public E Contingut(){
		return contingut;
	}
	/** Retorna el NodeDesti de l'aresta*/
	public V NodeDesti(){
		return NodeDesti;
	}
	/** Retorna el NodeOrigen de l'aresta*/
	public V NodeOrigen(){
		return NodeOrigen;
	}
	public String toString(){
		return NodeOrigen.toString() + " " + contingut.toString() + " " + NodeDesti.toString();
	}
}
