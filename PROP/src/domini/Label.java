package domini;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;


public class Label extends AlgorismeCommunityDetection {

		List<Integer> Randoml;
		int size_goriginal;
		HashMap<Integer,HashSet<Integer>> Comunitats; //Guarda cada comunitat amb clau etiqueta i valor un conjunt de vertex.
		List<Integer> Labels; //a Labels.get(i) hi ha etiqueta de i.
		
		//Constructora
		public Label(Entrada En, Sortida S) throws Exception {
			CercarComunitats(En,S);

		}
		
		//Modificadores
		
		/*Operacio que omple la llista Randoml amb un ordre aletori de tots els vertex
		 */

		
		//Cerca les comunitats al graf
		protected void CercarComunitats(Entrada En, Sortida S) throws Exception {
			
			
			
			size_goriginal = En.Graf().ordre();
			int[] Labels = new int[size_goriginal];
			Comunitats = new HashMap<Integer,HashSet<Integer>>();
			//Es crea el conjunt de comunitats inicial on cada vertex es una comunitat amb una etiqueta, el numero de vertex.
			for (int i = 0; i < size_goriginal; ++i) {
				HashSet<Integer> Comunitat_label = new HashSet<Integer>();
				Comunitat_label.add(i);
				Comunitats.put(i,Comunitat_label);
				Labels[i] = i;
			}

			Randoml = new ArrayList<Integer>(); 
			boolean canvis = true;
			while (canvis) {

				Random_list();

				canvis = false;
				for (int i = 0; i < size_goriginal; ++i) {

					int actual = Randoml.get(i); //es visiten els vertex en ordre aleatori

					List<Parell<Integer, Double>> vertex_veins = null;
					vertex_veins = En.Graf().ArestesAdjacentsNum(actual);
					HashMap<Integer,Double> ocurrencies = new HashMap<Integer,Double>();
					
					//actualitza les ocurrencies O(vertex_veins.size())
					for(int j = 0; j< vertex_veins.size();++j){
						int index = Labels[vertex_veins.get(j).ObtenirPrimer()];
						if(ocurrencies.containsKey(index)){
							double valor = ocurrencies.get(index);
							ocurrencies.put(index,valor+vertex_veins.get(j).ObtenirSegon()+1.0);
						}
						else ocurrencies.put(index,vertex_veins.get(j).ObtenirSegon()+1.0);
						
					}
					
					//posa a maxims les etiquetes amb nombre de veins maxim.
					List<Integer> maxims = new ArrayList<Integer>();
					double max = 0.0;
					for(Map.Entry<Integer,Double> entry: ocurrencies.entrySet()){
						if(entry.getValue() > max){
							max = entry.getValue();
							maxims.clear();
							maxims.add(entry.getKey());
						}
						else if(entry.getValue()==max) maxims.add(entry.getKey());
					}
					
					//Calcula el valor de moure, si true s'ha de canviar el vertex d'etiqueta.
					boolean moure = true;
					for(int k = 0; k < maxims.size(); ++k){
						if(maxims.get(k) == Labels[actual]) moure = false;
					}
					
					//si hem de canviar l'etiqueta de comunitat
					if(moure){
						canvis = true;
						Random r = new Random();
						int lnova = maxims.get(r.nextInt(maxims.size())); //index de la comunitat nova.
						Comunitats.get(Labels[actual]).remove(actual);
						Comunitats.get(lnova).add(actual);
						if(Comunitats.get(Labels[actual]).size()==0) Comunitats.remove(Labels[actual]); //Si la comunitat no té vèrtex s'elimina.
						Labels[actual] = lnova; //s'actualitza el vector de correspondencies
						
					}
					
					
					
					
					
					
					
					
				}

			}
			//Es guarda el resultat a l'objecte sortida s.

			List<List<Integer>> Comunitat = new ArrayList<List<Integer>>();
			for(Map.Entry<Integer,HashSet<Integer>> entry: Comunitats.entrySet()){

				List<Integer> l = new ArrayList<Integer>(entry.getValue());
				Comunitat.add(l);
			}
			S.afegir_nodes(Comunitat);
		}
		
		private void Random_list(){
			Randoml = new ArrayList<Integer>();
			boolean afegit;
			Random r = new Random();
			for (int i = 0; i < size_goriginal; ++i) {
				afegit = false;
				while (!afegit) {
					int next = r.nextInt(size_goriginal);
					if (Randoml.indexOf(next) == -1) {
						afegit = true;
						Randoml.add(i, next);
					}
				}
			}
		}
		
}
