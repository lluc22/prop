package domini;
public class Pagina extends ElemWiki{
	
	//Creadores//
	
	/*Crea una pagina buida*/
	public Pagina(){
		super();
	}
	
	/*Crea una pagina amb nom = s*/
	public Pagina(String s){
		super(s);
	}
	
	/*Retorna pagina*/
	public String quisoc(){
		return "page";
	}
	
	/*True si this = obj, false altrament*/
	public boolean equals(Object o){
		return super.equals(o);
	}

}
