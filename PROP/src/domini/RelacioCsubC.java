package domini;

public class RelacioCsubC extends Relacio {

	public RelacioCsubC() {
		super();
	}
	
	public RelacioCsubC(ElemWiki e1, ElemWiki e2) {
		super(e1,e2,"CsubC");
	}
}