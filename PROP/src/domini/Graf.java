package domini;
import java.util.ArrayList;
import java.util.List;



/*Classe auxiliar adjacencia*/
class Adjacencia<E>{
	int adjacent;
	List<E> contingut;
	
	Adjacencia(int adj, E cont){
		adjacent = adj;
		contingut = new ArrayList<E>();
		contingut.add(cont);
	}
	Adjacencia(int adj){
		adjacent = adj;
		contingut = new ArrayList<E>();
	}
	Adjacencia(int adj, List<E> cont) {
                adjacent = adj;
                contingut = cont;
	}
	int adj(){return adjacent;}
	List<E> contingut(){return contingut;}
	void afegircontingut(E x){contingut.add(x);}
	void modificaradjacent() { --adjacent; }
	public Adjacencia<E> clone() {
                Adjacencia<E> aux = new Adjacencia<E>(adjacent, new ArrayList<E>(contingut));
                return aux;
	}
}


public class Graf<V,A extends Aresta<V,E>, E> {//La E es un parametre que representa el 
											   //contingut de l'Aresta<V,E>, pero java ens
											   //ho fa posar a graf.
	
	List<List<Adjacencia<E>>> graf; //conte els nodes i les adjacencies d'aquest del graf
	int V; //numero de vertex del graf
	int A; //numero d'arestes del graf
	boolean pes_tipusaresta; //true si volem un graf no dirigit, false si volem un graf dirigit
	TSTList<V> noms_vertex; //noms_vertex[i] conte el element del vertex i del graf
	
	
	//constructores//
	
	/*Es crea un graf buit amb un mode de representacio concret*/
	public Graf(boolean pes_aresta){
		graf = new ArrayList<List<Adjacencia<E>>>();
		V = 0;
		A = 0;
		pes_tipusaresta = pes_aresta;
		noms_vertex = new TSTList<V>();
	}
	
	//modificadores//

    /**Afegeix el vertex "vertex" al graf. Si "vertex" existeix al graf no fa re.*/
	public void AfegirVertex(V vertex) {
		if(!noms_vertex.contains(vertex)){
			graf.add(new ArrayList<Adjacencia<E>>());
			noms_vertex.add(vertex);
			++V;
		}
	}
	
	/*PRE: l'aresta a afegir no existeix.
	 POST: Afegeix una aresta de "vertexO" a "vertexD" amb pes "pes" al graf si els vertex no son al graf els hi afegeix*/
	public void AfegirAresta(V vertexO, E contingut, V vertexD) throws DadesIncorrectesException{
		
		AfegirVertex(vertexO);
		AfegirVertex(vertexD);
		
		if(pes_tipusaresta){
                        int o = noms_vertex.indexOf(vertexO);
                        int d = noms_vertex.indexOf(vertexD);
                        if(ExisteixArestaNum(o, d)) throw new DadesIncorrectesException("L'aresta ja existeix");
                        AfegirArestaNum(o,contingut,d);
		}
		else{
			int indexO = noms_vertex.indexOf(vertexO);
			int indexD = noms_vertex.indexOf(vertexD);
			Adjacencia<E> x = new Adjacencia<E>(indexD);
			List<Adjacencia<E>> adjacents_o = graf.get(indexO);
			for(Adjacencia<E> a : adjacents_o){
				if(a.adj() == indexD) {
					if(!a.contingut.contains(contingut)){
						a.afegircontingut(contingut);
						++A;
					}
					else throw new DadesIncorrectesException("L'aresta ja existeix");
					return;
				}
			}
			x.afegircontingut(contingut);
			++A;
			graf.get(indexO).add(x);
		}
	}
	
	/*PRE: vertexO i vertexD mes petits que ordre del graf, nomes per la representacio del graf no dirigit
	  POST: graf conte una aresta de vertexO a vertexD amb pes = pes*/
	public void AfegirArestaNum(int vertexO, E pes, int vertexD) throws DadesIncorrectesException{
		if(!pes_tipusaresta) throw new DadesIncorrectesException("Operacio no disponible per a aquesta representacio de la classe graf") /*posar excepcio, estas am laltre representacio*/ ;
                if((0 > vertexD || vertexD >= V) || (0 > vertexO || vertexO >= V)) throw new DadesIncorrectesException("vertexO i vertexD han de ser mes petits que el nombre de vertex del graf i mes grans o igual que zero");
                Adjacencia<E> x = new Adjacencia(vertexD, pes);	
		Adjacencia<E> y = new Adjacencia(vertexO, pes);
		List<Adjacencia<E>> adjacents_o = graf.get(vertexO);
		for(Adjacencia<E> a : adjacents_o){
			if(a.adj() == vertexD) return;//excepcions, l'aresta ja esta al graf
		}
		graf.get(vertexO).add(x);
		graf.get(vertexD).add(y);
		++A;
		
	}
	
	/*Afegeix l'aresta a al graf*/
	public void AfegirAresta(A a) throws DadesIncorrectesException {
                        try {
                                AfegirAresta(a.NodeOrigen(),a.Contingut(),a.NodeDesti());
                        }
                        catch (DadesIncorrectesException e) {
                                throw e;
                        }
	}
	
	/*Afegeix l llista d'arestes l al graf*/
	public void AfegirArestes(List<A> l) throws DadesIncorrectesException {
		for(A a : l){
			try {
                                AfegirAresta(a);
                        }
                        catch (DadesIncorrectesException e) {
                                throw e;
                        }        
		}
	}

	/*Borra el vertex "v" i totes les arestes adjacents a aquest*/
	public void BorrarVertex(V v) throws DadesIncorrectesException {
		int index = noms_vertex.indexOf(v);
                if(index == -1) throw new DadesIncorrectesException("El vertex v no existeix al graf");
		noms_vertex.delete(v);
		int restar = 0;
		Adjacencia<E> borrar = null;
		for(List<Adjacencia<E>> l : graf){
			for(Adjacencia<E> a : l){
				if(a.adj() == index){
					restar += a.contingut().size();
					borrar = a;
				}
				else if (a.adj() > index) {
                                        a.modificaradjacent();
				}
			}
			l.remove(borrar);
		}
		for(Adjacencia<E> a : graf.get(index)){
			restar += a.contingut().size();
		}
		if(pes_tipusaresta) restar /= 2;
		A = A - restar;
		graf.remove(index);
		--V;
	}
	
	/* Borra l'aresta que va de "vertexO" a "vertexD"*/
	public void BorrarAresta(V vertexO, V vertexD) throws DadesIncorrectesException {
		int indexO = noms_vertex.indexOf(vertexO);
		int indexD = noms_vertex.indexOf(vertexD);
		try {
                        BorrarArestaNum(indexO, indexD);
                }
                catch (DadesIncorrectesException e) {
                        throw e;
                }
	}
	
	/* Borra l'aresta que va de "vO" a "vD"*/
	public void BorrarArestaNum(int vO, int vD) throws DadesIncorrectesException {
		List<Adjacencia<E>> l = graf.get(vO);
		boolean borrada = false;
		Adjacencia<E> borrar = null;
		for(Adjacencia<E> a : l){
			if(a.adj() == vD){
				borrar = a;
				borrada = true;
			}
		}
		if(!borrada) throw new DadesIncorrectesException("No existeix una aresta de vO a VD al graf");
		else l.remove(borrar);
		if(pes_tipusaresta){
			l = graf.get(vD);
			for(Adjacencia<E> a : l){
				if(a.adj() == vO){
					borrar = a;
					borrada = true;
				}
			}
			l.remove(borrar);
		}
		--A;
	}
	
	public boolean ModificarVertex(V vell, V nou) {
                boolean b = noms_vertex.contains(vell);
                noms_vertex.set(vell,nou);
                
                /*boolean b = false;
                for (int i = 0; i < noms_vertex.size(); i++) {
                        if (noms_vertex.get(i).equals(vell)) {
                                noms_vertex.set(i,nou);
                                b = true;
                                break;
                        }
                }*/
                return b;
	}
	
	public void ModificarTipusAresta(V origen, V desti, E contingutnou) throws DadesIncorrectesException {
                try {
                        BorrarAresta(origen,desti);
                }
                catch (DadesIncorrectesException e) {
                        throw e;
                }
                try {
                        AfegirAresta(origen,contingutnou,desti);
                }
                catch (DadesIncorrectesException e) {
                        throw e;
                }
	}
	
	//consultores//
	
	/*Retorna el numero de vertex del graf*/
	public int ordre(){
		return V;
	}
	/*Retorna true si el graf es no dirigit i false altrament*/
	public boolean mode(){
		return pes_tipusaresta;
	}
	
	/*Retorna el numero d'arestes del graf*/
	public int mida(){
		return A;
	}
	
	/*Retorna el grau del vertex "v"*/
	public int grau(V v) throws DadesIncorrectesException{
                int i = noms_vertex.indexOf(v);                
                if(i == -1) throw new DadesIncorrectesException("El vertex v no existeix al graf");
		return graf.get(i).size();
	}
	
	/*Retorna el grau del vertex "v"*/
	public int grau(int v) throws DadesIncorrectesException{
                if(0 > v || v >= V )throw new DadesIncorrectesException("v ha de ser mes petit que el nombre de vertex del graf i més gran o igual que 0");
                return graf.get(v).size();
	}
	
	/*Retorna el objecte asociat al vertex i*/
	public V vertex(int i) throws DadesIncorrectesException{
                if(0 > i || i >= V) throw new DadesIncorrectesException("i ha de ser mes petit que el nombre de vertex del graf i més gran o igual que 0");
		return noms_vertex.get(i);
	}
	
	public int vertex(V v) {
                for (int i = 0; i < noms_vertex.size(); i++) {
                        if (noms_vertex.get(i).equals(v)) return i;
                }
                return -1;
	}
	
	/*Retorna una llista amb els vertex del graf*/
	public List<V> ListaVertex(){
		return noms_vertex.values();
	}
	
	/*PRE: pes_tipusaresta = true.
	 *POST: retorna una llista amb les adjacencies del vertex amb index "v".
	        Una adjacencia es representada per un parell<index del vertexDesti,Pes>*/
	public List<Parell<Integer,E>> ArestesAdjacentsNum(int v) throws DadesIncorrectesException {
		if(!pes_tipusaresta) throw new DadesIncorrectesException("Operacio no disponible per a aquesta representacio de la classe graf");
                if(0 > v || v >= V )throw new DadesIncorrectesException("v ha de ser mes petit que el nombre de vertex del graf i més gran o igual que 0");;
		List<Parell<Integer,E>> l = new ArrayList<Parell<Integer,E>>();
		List<Adjacencia<E>> adj = graf.get(v);
		for (Adjacencia<E> a : adj)
		{
		    Parell<Integer,E> p = new Parell<Integer,E>(a.adj(),a.contingut().get(0));
		    l.add(p);
		}
		return l;
	}
	
	/*PRE: pes_tipusaresta = true.
	 *POST: retorna una llista amb les adjacencies del vertex "v".
	        Una adjacencia es representada per un parell<index del vertexDesti,Pes>*/
	public List<Parell<Integer,E>> ArestesAdjacents(V v) throws DadesIncorrectesException{
                int i = noms_vertex.indexOf(v);                
                if(i == -1) throw new DadesIncorrectesException("El vertex v no existeix al graf");
                return ArestesAdjacentsNum(i);
	}
	
	/*retorna una llista amb els vertex adjacents a "v"*/
	public List<V> VertexAdjacents(V v) throws DadesIncorrectesException{
		int i = noms_vertex.indexOf(v);                
                if(i == -1) throw new DadesIncorrectesException("El vertex v no existeix al graf");
                List<V> l = new ArrayList<V>();
		List<Adjacencia<E>> adj = graf.get(i);
		for (Adjacencia<E> a : adj)
		{
		    l.add(noms_vertex.get(a.adj()));
		}
		return l;
	}
	
	/*retorna una llista amb els index dels vertex adjacents al index "v"*/
	public List<Integer> VertexAdjacentsNum(int v) throws DadesIncorrectesException{
                if(0 > v || v >= V )throw new DadesIncorrectesException("v ha de ser mes petit que el nombre de vertex del graf i més gran o igual que 0");;
		List<Integer> l = new ArrayList<Integer>();
		List<Adjacencia<E>> adj = graf.get(v);
		for (Adjacencia<E> a : adj)
		{
		    l.add(a.adj());
		}
		return l;
	}
	/*true si existeix una aresta de v0 a vD, false altrament*/
	public boolean ExisteixArestaNum(int vO,int vD) throws DadesIncorrectesException{
                if(0 > vO || vO >= V || 0 > vD || vD >= V)throw new DadesIncorrectesException("vO i vD han de ser mes petits que el nombre de vertex del graf i més gran o igual que 0");;
		List<Adjacencia<E>> l = graf.get(vO);
		boolean trobat = false;
		for(Adjacencia<E> a : l){
			if(a.adj() == vD){
				trobat = true;
				break;
			}
		}
		return trobat;
	}
	
	/*true si existeix una aresta de v0 a vD, false altrament*/	
	public boolean ExisteixAresta(V vO, V vD) throws DadesIncorrectesException{
		int i = noms_vertex.indexOf(vO);                
                if(i == -1) throw new DadesIncorrectesException("El vertex vO no existeix al graf");
		int ii = noms_vertex.indexOf(vD);                
                if(ii == -1) throw new DadesIncorrectesException("El vertex vD no existeix al graf");
		return ExisteixArestaNum(i,ii);
	}
	
	/*PRE: pes_tipusaresta = true
	 *POST: Retorna el pes de l'aresta entre el vertex v1 i el vertex v2*/
	public E PesAresta(int v1, int v2) throws DadesIncorrectesException{
     		if(!pes_tipusaresta) throw new DadesIncorrectesException("Operacio no disponible per a aquesta representacio de la classe graf");
                if(0 > v1 || v1 >= V || 0 > v2 || v2 >= V)throw new DadesIncorrectesException("vO i vD han de ser mes petits que el nombre de vertex del graf i més gran o igual que 0");;
		List<Adjacencia<E>> l = graf.get(v1);
		boolean trobat = false;
		E pes = null;
		for(Adjacencia<E> a : l){
			if(a.adj() == v2){
				trobat = true;
				pes = a.contingut.get(0);
				break;
			}
		}
		if(trobat) return pes;
		else {
			throw new DadesIncorrectesException("L'aresta no existeix");
		}
	}
	
	/*PRE: pes_tipusaresta = false.
	 *Retorna les arrel del graf*/
	public List<Integer> Arrels() throws DadesIncorrectesException{
		if(pes_tipusaresta) throw new DadesIncorrectesException("Operacio no disponible per a aquesta representacio de la classe graf");
                List<Boolean> lb = new ArrayList<Boolean>(V);
		for(Boolean b : lb){
			b = false;
		}
		for(List<Adjacencia<E>> l : graf){
			for(Adjacencia<E> a : l){
				lb.set(a.adj(), true);
			}
		}
		List<Integer> r = new ArrayList<Integer>();
		for(int i  = 0; i < V; ++i){
			if(!lb.get(i)) r.add(i);		
		}
		return r;		
	}
	
	/*PRE: pes_tipusaresta = false
	 * POST: retorna una llista de parells que conte els vertex adjacents a v i el/els contingut/s
	 *       de la/les aresta/es que els uneixen*/
	public List<Parell<V,List<E>>> ArestesAdjacentsDirigit(V v) throws DadesIncorrectesException{
		int i = noms_vertex.indexOf(v);                
                if(i == -1) throw new DadesIncorrectesException("El vertex v no existeix al graf");
            	if(pes_tipusaresta) throw new DadesIncorrectesException("Operacio no disponible per a aquesta representacio de la classe graf");
		List<Parell<V,List<E>>> l = new ArrayList<Parell<V,List<E>>>();
		List<Adjacencia<E>> adj = graf.get(i);
		for (Adjacencia<E> a : adj)
		{
		    Parell<V,List<E>> p = new Parell<V,List<E>>(noms_vertex.get(a.adj()),a.contingut());
		    l.add(p);
		}
		return l;
	}
	
	/*PRE: pes_tipusaresta = false
	 * POST: retorna una llista de parells que conte els vertex adjacents a v i el/els contingut/s//construcotres
	 *       de la/les aresta/es que els uneixen*/
	public List<Parell<Integer,List<E>>> ArestesAdjacentsDirigitNum(int v) throws DadesIncorrectesException{
                if(0 > v || v >= V)throw new DadesIncorrectesException("v ha de ser mes petit que el nombre de vertex del graf i més gran o igual que 0");;
            	if(pes_tipusaresta) throw new DadesIncorrectesException("Operacio no disponible per a aquesta representacio de la classe graf");
		List<Parell<Integer,List<E>>> l = new ArrayList<Parell<Integer,List<E>>>();
		List<Adjacencia<E>> adj = graf.get(v);
		for (Adjacencia<E> a : adj)
		{
		    Parell<Integer,List<E>> p = new Parell<Integer,List<E>>(a.adj(),a.contingut());
		    l.add(p);
		}
		return l;
	}
	
	/* Per fer copies d'un graf*/
	public Graf<V,A,E> clone(){
		Graf<V,A,E> g = new Graf<V,A,E>(pes_tipusaresta);
		g.A = A;
		g.V = V;
		g.noms_vertex = new TSTList<V>(noms_vertex);
		g.graf = new ArrayList<List<Adjacencia<E>>>();
		for(List<Adjacencia<E>> l : graf){
                        List<Adjacencia<E>> aux = new ArrayList<Adjacencia<E>>();
                        for (Adjacencia<E> adj : l) {
                                Adjacencia<E> adjaux = adj.clone();
                                aux.add(adjaux);
                        }
			g.graf.add(aux);
		}
		return g;
	}
}