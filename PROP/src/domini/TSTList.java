package domini;
import java.util.List;
import java.util.ArrayList;

public class TSTList<T> {
	TernarySearchTree<Integer> keys;
	TernarySearchTree<T> values;
	int nextkey;
	int size;
	List<Integer> deletes;
	
	public TSTList(){
		keys = new TernarySearchTree();
		values = new TernarySearchTree();
		nextkey = 0;
		size = 0;
		deletes = new ArrayList<Integer>();
	}
	
	public TSTList(TSTList l){
		size = l.size;
		nextkey = l.nextkey;
		keys = new TernarySearchTree<Integer>(l.keys);
		values = new TernarySearchTree<T>(l.values);
		deletes = new ArrayList<Integer>(l.deletes);
	}
	
	public void add(T t){
		keys.insert(t.toString(), nextkey);
		values.insert(((Integer)nextkey).toString(),t);
		++nextkey;
		++size;
	}
	public void set(T vell, T nou){
		int x = keys.delete(vell.toString());
		keys.insert(nou.toString(),x);
		values.insert(((Integer)x).toString(), nou);
	}
	/*esborrar un element no canvia cap index de la llista*/
	public void delete(T t){
		int x = keys.delete(t.toString());
		deletes.add(x);
		values.delete(((Integer)x).toString());
		--size;
		
	}
	public T get(int i){
		int arestar= 0;
		for(Integer inte : deletes){
			if(inte<i) ++arestar; 
		}
		return values.get(((Integer)(i-arestar)).toString());
	}
	public boolean contains(T t){
		return keys.get(t.toString()) != null;
	}
	//pre: this contains t
	public int indexOf(T t){
		int x = keys.get(t.toString());
		int arestar= 0;
		for(Integer i : deletes){
			if(i<x) ++arestar; 
		}
		return (x-arestar);
	}
	public int size(){
		return size;
	}
	public List<T> values(){
		List<T> res = new ArrayList<T>();
		for(int i = 0; i < nextkey; ++i){
			T toadd = values.get(((Integer)i).toString());
			if(toadd != null)	res.add(toadd);
		}
		return res;
	}
}