package domini;

import java.util.ArrayList;
import java.util.List;
import java.lang.Math;

class Comunitat_Louvain {
	private List<Integer> vertexs_comunitat;
	private double mida; //Pesos de les arestes que conte la comunitat
	private double arcs_totals; //mida + pesos de les arestes que entren/surten de la comunitat
	
	//Constructores
	public Comunitat_Louvain() {
		vertexs_comunitat = new ArrayList <Integer>();
		mida = 0;
		arcs_totals = 0;
	}
	
	public Comunitat_Louvain(Graf g,int v) throws Exception {
		vertexs_comunitat = new ArrayList<Integer>();
		vertexs_comunitat.add(v);
		mida = 0;
		arcs_totals = 0;
		//Calculem la suma dels pesos de les arestes del vertex de la comunitat de Louvain
		List<Parell<Integer,Double>> arestes_adj = new ArrayList<Parell<Integer,Double>>();
		try {
                        arestes_adj = g.ArestesAdjacentsNum(v);
                }
                catch (Exception e) {
                        throw e;
                }
		for (int k = 0; k < arestes_adj.size(); ++k) {
			arcs_totals = arcs_totals + (arestes_adj.get(k).ObtenirSegon() + 1);
		}
	}
	
	//Modificadores
	public void afegir(Graf g, int v) throws Exception {
		//si el vertex ja forma part de la comunitat no cal fer res
		if (vertexs_comunitat.indexOf(v) == -1) {
			List<Parell<Integer,Double>> arestes_adj =  new ArrayList<Parell<Integer,Double>>();
			try {
                                arestes_adj = g.ArestesAdjacentsNum(v);
                        }
                        catch (Exception e) {
                                throw e;
                        }
			boolean afegida;
			/*if (arestes_adj().size == 0) Exception?*/

			for (int k = 0; k < arestes_adj.size(); ++k) {
				int vertexdest = arestes_adj.get(k).ObtenirPrimer();
				double pes = arestes_adj.get(k).ObtenirSegon() +1;
				afegida = false;
				for (int i = 0; i < vertexs_comunitat.size(); ++i) {
					int vertex = vertexs_comunitat.get(i);
					if (vertexdest == vertex) {
						mida = mida + pes;
						afegida = true;
					}
				}
				if (!afegida) arcs_totals = arcs_totals + pes;
			} 
			vertexs_comunitat.add(v);
		}
	}
	
	public void eliminar(Graf g, int v) throws Exception {
		//si el vertex no forma part de la comunitat no cal fer res
		if (vertexs_comunitat.indexOf(v) != -1) {
			List<Parell<Integer,Double>> arestes_adj = new ArrayList<Parell<Integer,Double>>();
			try {
                                arestes_adj = g.ArestesAdjacentsNum(v);
                        }
                        catch (Exception e) {
                                throw e;
                        }
			boolean eliminada;
			/*if (arestes_adj().size == 0) Exception?*/
			
			for (int k = 0; k < arestes_adj.size(); ++k) {
				int vertexdest = arestes_adj.get(k).ObtenirPrimer();
				double pes = arestes_adj.get(k).ObtenirSegon() + 1;
				eliminada = false;
				for (int i = 0; i < vertexs_comunitat.size(); ++i) {
					int vertex = vertexs_comunitat.get(i);
					if (vertexdest == vertex) {
						mida = mida - pes;
						eliminada = true;
					}
				}
				if (!eliminada) arcs_totals = arcs_totals - pes;
			} 
			int index_v = vertexs_comunitat.indexOf(v);
			vertexs_comunitat.remove(index_v);
		}
	}
	
	public double relacio (Graf g, int v) throws Exception {
		List<Parell<Integer,Double>> arestes_adj = new ArrayList<Parell<Integer,Double>>();
		try {
                        arestes_adj = g.ArestesAdjacentsNum(v);
                }
                catch (Exception e) {
                        throw e;
                }
		double suma = 0;
		/*if (arestes_adj().size == 0) Exception?*/

		for (int k = 0; k < arestes_adj.size(); ++k) {
			int vertexdest = arestes_adj.get(k).ObtenirPrimer();
			double pes = arestes_adj.get(k).ObtenirSegon() + 1;
			for (int i = 0; i < vertexs_comunitat.size(); ++i) {
				int vertex = vertexs_comunitat.get(i);
				if (vertexdest == vertex) suma = suma + pes;
			}
		}
	    return suma;
	}
	
	//Consultores
	public int tamany_comunitat(){
		return vertexs_comunitat.size(); 	
	}
	
	public double mida(){
		return mida;
	}
	
	public double arestes_totals(){
		return arcs_totals;
	}
	
	public boolean conte (int v) {
		int x = vertexs_comunitat.indexOf(v);
		if (x == -1) return false;
		return true;
	}
	
	public int consultar_iessim(int i) {
		return vertexs_comunitat.get(i);
	}
}


public class Louvain extends AlgorismeCommunityDetection {
	double mida_goriginal; //Arestes del graf complet
	double size_goriginal;
	List<Comunitat_Louvain>Comunitats; 
	List<Double> modularitats;

	
	//PAS 1 algorisme = cada vertex forma una comunitat
	public Louvain(Entrada En, Sortida S) throws Exception {
		CercarComunitats(En,S);
	}
	
	private double grau_pesos(Graf g, int v) throws Exception {
		double suma = 0;
		try {
			List<Parell<Integer,Double>> arestes_adj = g.ArestesAdjacentsNum(v);
		
			for (int k = 0; k < arestes_adj.size(); ++k) {
				double pes = arestes_adj.get(k).ObtenirSegon()+1;
				suma += pes;
			}
		} catch(NullPointerException npe) {
			System.err.println("Louvain.GrauPesos: Estem consultant una variable que no te cap objecte instanciat");
			throw npe;
		} catch(IndexOutOfBoundsException ie) {
			System.err.println("Louvain.GrauPesos: Estem accedint fora d'una estructura de dades");
			throw ie;
		}
		return suma;
	}
	
	protected void CercarComunitats(Entrada En, Sortida So) throws Exception {
		Graf G = En.Graf();
		try {
			So.AfegirPas("Recollim tota la informacio necessaria per calcular la modularitat abans de comencar a aplicar l'algorisme");
			G = En.Graf();
			mida_goriginal = 0;
			modularitats = new ArrayList <Double> ();
			for (int i = 0; i < G.ordre(); ++i) {
				modularitats.add(0.0);
			}
			for (int i = 0; i < G.ordre(); ++i) {
				List<Parell<Integer,Double>> arestes_adj = G.ArestesAdjacentsNum(i);
				for (int j = 0; j < arestes_adj.size(); ++j) {
						double pes = arestes_adj.get(j).ObtenirSegon()+1;
						mida_goriginal += pes;
					}
				}
			mida_goriginal = mida_goriginal/2;
			size_goriginal = G.ordre();
			Comunitats = new ArrayList<Comunitat_Louvain>();
			So.AfegirPas("Creem un a comunitat de Louvain per a cada vertex");
			for (int k = 0; k < size_goriginal;++k){
				Comunitat_Louvain cl;
				try {
                                        cl = new Comunitat_Louvain(G,k);
                                }
                                catch (Exception e) {
                                        throw e;
                                }
				Comunitats.add(cl); 
			}
		} catch(NullPointerException npe) {
			System.err.println("Inicialitzacio Louvain: Estem consultant una variable que no te cap objecte instanciat");
			throw npe;
		} catch(IndexOutOfBoundsException ie) {
			System.err.println("Inicialitzacio Louvain: Estem accedint fora d'una estructura de dades");
			throw ie;
		}
		double m = 2*mida_goriginal;
		boolean canvis = true; //Criteri parada
		boolean trobat;
		double maxim;
		So.AfegirPas("Comencem a aplicar l'algorisme");
		while (canvis) {
			So.AfegirPas("Comencem una nova iteracio");
		
			canvis = false;
			for (int i = 0; i < size_goriginal; ++i) {
				So.AfegirPas("Analitzem el vertex " + i);
				Comunitat_Louvain candidata = new Comunitat_Louvain();

				Comunitat_Louvain actual = new Comunitat_Louvain();
				List <Integer> vertexs_veins = G.VertexAdjacentsNum(i);
				
				//CALCUL MODULARITATS
				maxim = 0;
				try {
					for (int k = 0; k < vertexs_veins.size(); ++k) {
						int j = vertexs_veins.get(k);
						So.AfegirPas("Analitzem el vertex " + j + "que es un dels adjacents a " +i);
					
						trobat = false;
						int z = 0;
						while (!trobat && z < Comunitats.size()){
							So.AfegirPas("Busquem si el vertex" + j + "esta a la comunitat de louvain" +k);
							actual = Comunitats.get(z);
							if (actual.conte(j)) trobat = true;
							++z;
						}
						So.AfegirPas("Si el vertex" + i +" i " + j + "pertanyen a comunitats diferents calculem la modularitat");
						if (!actual.conte(i)) {
							So.AfegirPas("Comencem el calcul de modularitat entre" + i + " i " + j);
							
							double connexio = 0.0;
							try {
                                                                connexio = actual.relacio(G,i);
                                                        }
                                                        catch (Exception e) {
                                                                throw e;
                                                        }
							
							double par5 = 0.0;
							try {
                                                                par5 = Math.pow((grau_pesos(G,i)/m),2.0); 
                                                        }
                                                        catch (Exception e) {
                                                                throw e;
                                                        }
							double par4 = Math.pow((actual.arestes_totals()/m),2.0);
							double par3 = actual.mida()/m;
							double par2 = 0.0;
							try {
                                                                par2 = Math.pow(((actual.arestes_totals()+grau_pesos(G,i))/m),2);
                                                        }
                                                        catch (Exception e) {
                                                                throw e;
                                                        }
							double par1 = (actual.mida() + connexio)/m;
							
							double Q = (par1-par2)-(par3-par4-par5);
							if (Q > maxim) {
								So.AfegirPas("Actualitzem la modularitat maxima" + Q + "per " + maxim);
								maxim = Q;
								candidata = actual;
							}
						}
					}
				} catch(NullPointerException npe) {
					System.err.println("Calcul modulartiat: Consultem una variable que no te cap objecte instanciat");
					throw npe;
				} catch(IndexOutOfBoundsException ie) {
					System.err.println("Calcul modularitat: Estem accedint fora d'una estructura de dades");
					throw ie;
				}
					if (maxim > modularitats.get(i)) {
						try {
							So.AfegirPas("Canviem el vertex " + i + "a la comunitat candidata");
							modularitats.add(i, maxim);
					
							So.AfegirPas("Busquem la comunitat a la que pertany i");
							int s = 0;
							trobat = false;
							Comunitat_Louvain vella = new Comunitat_Louvain();
							while (!trobat && s < Comunitats.size()){
								vella = Comunitats.get(s);
								if (vella.conte(i)) trobat = true;
								++s;
							}
							try {
                                                                vella.eliminar(G,i);
                                                        }
                                                        catch (Exception e1) {
                                                                throw e1;
                                                        }
							try {
                                                                candidata.afegir(G,i);
                                                        }
                                                        catch (Exception e2) {
                                                                throw e2;
                                                        }
							canvis = true;
						} catch(NullPointerException npe) {
							System.err.println("Canvis Comunitat: Consultem una variable que no te cap objecte instanciat");
							throw npe;
							
						} catch(IndexOutOfBoundsException ie) {
							System.err.println("Canvis Comunitat: Estem accedint fora d'una estructura de dades");
							throw ie;
						}
					}
			}
		}
		List<Integer> com_res = new ArrayList <Integer>();
		List <List<Integer>> comunitats = new ArrayList<List<Integer>>();
		for(int i = 0; i < Comunitats.size(); ++i) {
			com_res = new ArrayList <Integer>();
			if (Comunitats.get(i).tamany_comunitat() > 0) {
				for (int j = 0; j < Comunitats.get(i).tamany_comunitat(); ++j) {
					com_res.add(Comunitats.get(i).consultar_iessim(j));
				}
				comunitats.add(com_res);
			}
		}
		So.afegir_nodes(comunitats);
	}
	
}
