package domini;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

class node {
        String nom;
        int identificador;
        List<Integer> EntersO;
        List<Integer> EntersD;
        node left;
        node right;
        
        public node(String n, int i) {
                nom = n;
                identificador = i;
                left = null;
                right = null;
                EntersO = new ArrayList<Integer>();
                EntersD = new ArrayList<Integer>();
        }
        
        public node(String n, int i, List<Integer> lO, List<Integer> lD) {
                nom = n;
                identificador = i;
                left = null;
                right = null;
                EntersO = lO;
                EntersD = lD;
        }
        
        public void ModificarNom(String n) {
                nom = n;
        }
        
        public void ModificarIdentificador(int i) {
                identificador = i;
        }
        
        public void AfegirEnterOrigen(Integer x) {
                EntersO.add(x);
        }
        
        public void AfegirEnterDesti(Integer x) {
                EntersD.add(x);
        }
        
        public boolean EsborrarEnterOrigen(Integer x) {
                for (int i = 0; i < EntersO.size(); i++) {
                        if (EntersO.get(i) == x) {
                                EntersO.remove(i);
                                return true;
                        }
                }
                return false;
        }
        
        public boolean EsborrarEnterDesti(Integer x) {
                for (int i = 0; i < EntersD.size(); i++) {
                        if (EntersD.get(i) == x) {
                                EntersD.remove(i);
                                return true;
                        }
                }
                return false;
        }
        
        public boolean ExisteixEnterOrigen(Integer x) {
                for (int i = 0; i < EntersO.size(); i++) {
                        if (EntersO.get(i) == x) {
                                return true;
                        }
                }
                return false;
        }
        
        public boolean ExisteixEnterDesti(Integer x) {
                for (int i = 0; i < EntersD.size(); i++) {
                        if (EntersD.get(i) == x) {
                                return true;
                        }
                }
                return false;
        }
        
        public String ObtenirNom() {
                return nom;
        }
        
        public int ObtenirIdentificador() {
                return identificador;
        }
        
        public List<Integer> ObtenirEntersOrigen() {
                return EntersO;
        }
        
        public List<Integer> ObtenirEntersDesti() {
                return EntersD;
        }
        
        public node ObtenirFillLeft() {
                return left;
        }
        
        public node ObtenirFillRight() {
                return right;
        }
        
        public void FillLeft(String n, int i) {
                left = new node(n, i);
        }
        
        public void FillRight(String n, int i) {
                right = new node(n, i);
        }
        
        public void FillLeft(String n, int i, List<Integer> lO, List<Integer> lD) {
                left = new node(n, i, lO, lD);
        }
        
        public void FillRight(String n, int i, List<Integer> lO, List<Integer> lD) {
                right = new node(n, i, lO, lD);
        }
        
        public void FillLeft(node n) {
                left = n;
        }
        
        public void FillRight(node n) {
                right = n;
        }
}

public class Arbre implements Iterable {
        
        node arrel;
        
        // Creadores
        
        // Crea un arbre sense nodes
        public Arbre() {
                arrel = null;
        }
        
        // Retorna true si el node s'ha afegit correctament i false si el node 
        // ja existeix 
        public boolean AfegirNode(String n, int i) {
                if (arrel == null) {
                        arrel = new node(n,i);
                        return true;
                }
                else {
                        return AfegirNodeRec(arrel,n,i);
                }
        }
        
        // Funció recursiva per afegir node
        private boolean AfegirNodeRec(node k, String n, int i) {
                if (k.ObtenirNom().equals(n)) {
                        return false;
                }
                else if (k.ObtenirNom().compareTo(n) < 0) {
                        if (k.ObtenirFillLeft() == null) {
                                k.FillLeft(n,i);
                                return true;
                        }
                        else {
                                return AfegirNodeRec(k.ObtenirFillLeft(),n,i);
                        }
                }
                else {
                        if (k.ObtenirFillRight() == null) {
                                k.FillRight(n,i);
                                return true;
                        }
                        else {
                                return AfegirNodeRec(k.ObtenirFillRight(),n,i);
                        }
                }
        }
        
        // Retorna true si el node s'ha afegit correctament o false si el node
        // que es vol afegir ja existeix en l'arbre
        public boolean AfegirNode(String n, int i, List<Integer> lO, List<Integer> lD) {
                if (arrel == null) {
                        arrel = new node(n,i,lO,lD);
                        return true;
                }
                else {
                        return AfegirNodeRec(arrel,n,i,lO,lD);
                }
        }
        
        // Funcio recursiva per afegir node
        private boolean AfegirNodeRec(node k, String n, int i, List<Integer> lO, List<Integer> lD) {
                if (k.ObtenirNom().equals(n)) {
                        return false;
                }
                else if (k.ObtenirNom().compareTo(n) < 0) {
                        if (k.ObtenirFillLeft() == null) {
                                k.FillLeft(n,i,lO,lD);
                                return true;
                        }
                        else {
                                return AfegirNodeRec(k.ObtenirFillLeft(),n,i,lO,lD);
                        }
                }
                else {
                        if (k.ObtenirFillRight() == null) {
                                k.FillRight(n,i,lO,lD);
                                return true;
                        }
                        else {
                                return AfegirNodeRec(k.ObtenirFillRight(),n,i,lO,lD);
                        }
                }
        }
        
        // Afegeix l'enter al node. Si retorna true és que ha creat el node i false altrament
        public boolean AfegirEnterOrigen(String n, int i, Integer x) {
                if (arrel == null) {
                        arrel = new node(n,i);
                        arrel.AfegirEnterOrigen(x);
                        return true;
                }
                else {
                        return AfegirEnterOrigenRec(arrel,n,i,x);
                }
        }
        
        // Funcio recursiva per afegir enter
        private boolean AfegirEnterOrigenRec(node k, String n, int i, Integer x) {
                if (k.ObtenirNom().equals(n)) {
                        k.AfegirEnterOrigen(x);
                        return false;
                }
                else if (k.ObtenirNom().compareTo(n) < 0) {
                        if (k.ObtenirFillLeft() == null) {
                                k.FillLeft(n,i);
                                node aux = k.ObtenirFillLeft();
                                aux.AfegirEnterOrigen(x);
                                return true;
                        }
                        else {
                                return AfegirEnterOrigenRec(k.ObtenirFillLeft(),n,i,x);
                        }
                }
                else {
                        if (k.ObtenirFillRight() == null) {
                                k.FillRight(n,i);
                                node aux = k.ObtenirFillRight();
                                aux.AfegirEnterOrigen(x);
                                return true;
                        }
                        else {
                                return AfegirEnterOrigenRec(k.ObtenirFillRight(),n,i,x);
                        }
                }
        }
        
        public boolean AfegirEnterDesti(String n, int i, Integer x) {
                if (arrel == null) {
                        arrel = new node(n,i);
                        arrel.AfegirEnterDesti(x);
                        return true;
                }
                else {
                        return AfegirEnterDestiRec(arrel,n,i,x);
                }
        }
        
        // Funcio recursiva per afegir enter
        private boolean AfegirEnterDestiRec(node k, String n, int i, Integer x) {
                if (k.ObtenirNom().equals(n)) {
                        k.AfegirEnterDesti(x);
                        return false;
                }
                else if (k.ObtenirNom().compareTo(n) < 0) {
                        if (k.ObtenirFillLeft() == null) {
                                k.FillLeft(n,i);
                                node aux = k.ObtenirFillLeft();
                                aux.AfegirEnterDesti(x);
                                return true;
                        }
                        else {
                                return AfegirEnterDestiRec(k.ObtenirFillLeft(),n,i,x);
                        }
                }
                else {
                        if (k.ObtenirFillRight() == null) {
                                k.FillRight(n,i);
                                node aux = k.ObtenirFillRight();
                                aux.AfegirEnterDesti(x);
                                return true;
                        }
                        else {
                                return AfegirEnterDestiRec(k.ObtenirFillRight(),n,i,x);
                        }
                }
        }
        
        // Retorna la llista d'enters que conté el node passat per paràmetre a la llista Origen
        // Pre: El node ha d'existir
        public Parell<List<Integer>,List<Integer>> ConsultarEntersNode(String n) {
                if (arrel == null) return new Parell<List<Integer>,List<Integer>>(); 
                return ConsultarEntersNodeRec(arrel,n);
        }
        
        private Parell<List<Integer>,List<Integer>> ConsultarEntersNodeRec(node k, String n) {
                if (k.ObtenirNom().equals(n)) {
                        Parell<List<Integer>,List<Integer>> p = new Parell<List<Integer>,List<Integer>>(k.ObtenirEntersOrigen(),k.ObtenirEntersDesti());
                        return p;
                }
                else if (k.ObtenirNom().compareTo(n) < 0) {
                        if (k.ObtenirFillLeft() == null) return new Parell<List<Integer>,List<Integer>>();
                        return ConsultarEntersNodeRec(k.ObtenirFillLeft(),n);
                }
                else { 
                        if (k.ObtenirFillRight() == null) return new Parell<List<Integer>,List<Integer>>();
                        return ConsultarEntersNodeRec(k.ObtenirFillRight(),n);
                }
        }
        
        // retorna l'identificador del node esborrat
        public int EsborrarNode(String n) {
                int i = ConsultarIdentificadorNode(n);
                if (i > -1) {
                        arrel = EsborrarNodeRec(arrel,n);
                }
                return i;
        }
        
        // Funcio recursiva per esborrar nodes
        private node EsborrarNodeRec(node k, String n) {
                if (k.ObtenirNom().equals(n)) {
                        if (k.ObtenirFillLeft() != null) {
                                node aux = k.ObtenirFillRight();
                                k = k.ObtenirFillLeft();
                                AjuntarNodes(k,aux);
                        }
                        else if (k.ObtenirFillRight() != null) {
                                k = k.ObtenirFillRight();
                        }
                        else k = null;
                }
                else if (k.ObtenirNom().compareTo(n) < 0) {
                        k.FillLeft(EsborrarNodeRec(k.ObtenirFillLeft(),n));
                }
                else {
                        k.FillRight(EsborrarNodeRec(k.ObtenirFillRight(),n));
                }
                return k;
        }
        
        // Funcio recursiva per ajuntar arbres 
        private void AjuntarNodes(node k,node aux) {
                if (k.ObtenirFillRight() == null) {
                        k.FillRight(aux);
                }
                else {
                        AjuntarNodes(k.ObtenirFillRight(),aux);
                }
        }
        
        // Esborra l'enter indicada de la llista d'enters del node n
        public void EsborrarEnterOrigen(String n, Integer x) {
                if (arrel != null) {
                        EsborrarEnterOrigenRec(arrel,n,x);
                }
        }
        
        // Funcio recusriva per esborrar enter
        private void EsborrarEnterOrigenRec(node k, String n, Integer x) {
                if (k.ObtenirNom().equals(n)) {
                        k.EsborrarEnterOrigen(x);
                }
                else if (k.ObtenirNom().compareTo(n) < 0) {
                        if (k.ObtenirFillLeft() != null) {
                                EsborrarEnterOrigenRec(k.ObtenirFillLeft(),n,x);
                        }
                }
                else {
                        if (k.ObtenirFillRight() != null) {
                                EsborrarEnterOrigenRec(k.ObtenirFillRight(),n,x);
                        }
                }
        }
        
        // Esborra l'enter indicada de la llista d'enters del node n
        public void EsborrarEnterDesti(String n, Integer x) {
                if (arrel != null) {
                        EsborrarEnterDestiRec(arrel,n,x);
                }
        }
        
        // Funcio recusriva per esborrar enter
        private void EsborrarEnterDestiRec(node k, String n, Integer x) {
                if (k.ObtenirNom().equals(n)) {
                        k.EsborrarEnterDesti(x);
                }
                else if (k.ObtenirNom().compareTo(n) < 0) {
                        if (k.ObtenirFillLeft() != null) {
                                EsborrarEnterDestiRec(k.ObtenirFillLeft(),n,x);
                        }
                }
                else {
                        if (k.ObtenirFillRight() != null) {
                                EsborrarEnterDestiRec(k.ObtenirFillRight(),n,x);
                        }
                }
        }
        
        // true si l'enter existeix, false altrament
        public boolean ExisteixEnterOrigen(String n, Integer x) {
                if (arrel == null) return false;
                else {
                        return ExisteixEnterOrigenRec(arrel,n,x);
                }
        }
        
        // Funcio recursva per veure si existeix un enter
        private boolean ExisteixEnterOrigenRec(node k, String n, Integer x) {
                if (k.ObtenirNom().equals(n)) {
                        return k.ExisteixEnterOrigen(x);
                }
                else if (k.ObtenirNom().compareTo(n) < 0) {
                        if (k.ObtenirFillLeft() == null) return false;
                        else {
                                return ExisteixEnterOrigenRec(k.ObtenirFillLeft(),n,x);
                        }
                }
                else {
                        if (k.ObtenirFillRight() == null) return false;
                        else {
                                return ExisteixEnterOrigenRec(k.ObtenirFillRight(),n,x);
                        }
                }
        }
        
        // true si l'enter existeix, false altrament
        public boolean ExisteixEnterDesti(String n, Integer x) {
                if (arrel == null) return false;
                else {
                        return ExisteixEnterDestiRec(arrel,n,x);
                }
        }
        
        // Funcio recursva per veure si existeix un enter
        private boolean ExisteixEnterDestiRec(node k, String n, Integer x) {
                if (k.ObtenirNom().equals(n)) {
                        return k.ExisteixEnterDesti(x);
                }
                else if (k.ObtenirNom().compareTo(n) < 0) {
                        if (k.ObtenirFillLeft() == null) return false;
                        else {
                                return ExisteixEnterDestiRec(k.ObtenirFillLeft(),n,x);
                        }
                }
                else {
                        if (k.ObtenirFillRight() == null) return false;
                        else {
                                return ExisteixEnterDestiRec(k.ObtenirFillRight(),n,x);
                        }
                }
        }
        
        public int ConsultarIdentificadorNode(String n) {
                if (arrel == null) return -1;
                else {
                        return ConsultarIdentificadorNodeRec(arrel, n);
                }
        }
        
        public int ConsultarIdentificadorNodeRec(node k, String n) {
                if (k.ObtenirNom().equals(n)) return k.ObtenirIdentificador();
                else if (k.ObtenirNom().compareTo(n) < 0) {
                        if (k.ObtenirFillLeft() == null) return -1;
                        else return ConsultarIdentificadorNodeRec(k.ObtenirFillLeft(),n);
                }
                else {
                        if (k.ObtenirFillRight() == null) return -1;
                        else return ConsultarIdentificadorNodeRec(k.ObtenirFillRight(),n);
                }
        }
        
        public int ModificarNomNode(String nomvell, String nomnou) {
                int i = ConsultarIdentificadorNode(nomvell);
                if (i > -1) {
                        Parell<List<Integer>,List<Integer>> p = ConsultarEntersNode(nomvell);
                        boolean b = AfegirNode(nomnou,i,p.ObtenirPrimer(),p.ObtenirSegon());
                        if (b) {   
                                EsborrarNode(nomvell);
                                return i;
                        }
                        return -2;
                }
                return -1;
        }
        
        public class itArbre implements Iterator<String> {
        
                private List<node> l;
                private node n;
                
                public itArbre() {
                        l = new ArrayList<node>();
                        n = null;
                }
                
                public boolean hasNext() {

                        if (n == null) {
                                if (arrel != null) return true;
                                else return false;
                        }
                        else {
                                if (n.ObtenirFillLeft() != null) return true;
                                else {
                                        for (int i = l.size()-2; i >= 0; i--) {
                                                if (l.get(i).ObtenirFillRight() == l.get(i+1)) return true;
                                                if (l.get(i).ObtenirFillLeft() != null && l.get(i).ObtenirFillLeft() != l.get(i+1)) return true;
                                        }
                                        return false;
                                }
                        }
                
                }
                
                public String next() {
                        
                        if (n == null) {
                                n = arrel;
                                l.add(n);
                                while (n.ObtenirFillRight() != null) {
                                        n = n.ObtenirFillRight();
                                        node aux = n;
                                        l.add(aux);
                                }
                                return n.ObtenirNom();
                        }
                        else {
                                if (n.ObtenirFillLeft() != null) {
                                        
                                        n = n.ObtenirFillLeft();
                                        l.add(n);
                                        while(n.ObtenirFillRight() != null) {
                                                n = n.ObtenirFillRight();
                                                node aux = n;
                                                l.add(n);
                                        }
                                        return n.ObtenirNom();
                                }
                                else {
                                        for (int i = l.size()-2; i >= 0; i--) {
                                                if (l.get(i).ObtenirFillRight() == l.get(i+1)) {
                                                        l.remove(i+1);
                                                        n = l.get(i);
                                                        return n.ObtenirNom();
                                                }
                                                if (l.get(i).ObtenirFillLeft() != null && l.get(i).ObtenirFillLeft() != l.get(i+1)) {
                                                        l.remove(i+1);
                                                        n = l.get(i).ObtenirFillLeft();
                                                        l.add(n);
                                                        while (n.ObtenirFillRight() != null) {
                                                                n = n.ObtenirFillRight();
                                                                node aux = n;
                                                                l.add(n);
                                                        }
                                                        return n.ObtenirNom();
                                                }
                                                l.remove(i+1);
                                        }
                                        return "";
                                }
                        }
                }
                
                public void remove() {}
        }
        
        public Iterator<String> iterator() {
                return new itArbre();
        }
        
}
