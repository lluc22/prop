package domini;

import java.io.IOException;
import java.util.List;
import java.util.Iterator;
import persistencia.ControladorPersistencia;

public class ControladorWiki {
	Graf<ElemWiki,Relacio,String> G;
	GrafWiki GW;
	ControladorPersistencia CP;
	static final int midabloc = 1000;
	String newline = System.getProperty("line.separator");
	Iterator<LiniaWiki> itR;
	Iterator<String> itP;
	Iterator<String> itC;
	
	// Constructora
	// Crea el ControladorWiki i inicialitza certes variables
	public ControladorWiki() {
		CP = new ControladorPersistencia(midabloc);
		GW = new GrafWiki();
		G = new Graf<ElemWiki,Relacio,String>(false);
		itR = GW.iterator();
		itP = GW.iteratorPags();
		itC = GW.iteratorCats();
	}

	//Modificadores
	
	// Guarda la informació del graf actual en el fitxer que es passa per paràmetre
	public void GuardarArestes(String path) throws Exception {
		try {
			CP.ObrirEscriptura(path);
		}
		catch (Exception e) {
			throw e;
		}
		if (GW.mida() > 0) {
                        Iterator<LiniaWiki> it = GW.iterator();
			String s = it.next().ObtenirLinia();
			if (GW.mida() > 1) s += "\n";
			int i = 1;
			while (it.hasNext()) {
                                LiniaWiki lw = it.next();
				if (s.length() + lw.ObtenirLinia().length() + 1 >= midabloc) {
					try {
						CP.EscriuFitxer(s);
					}
					catch (Exception e) {
						throw e;
					}
					s = new String();
				}
				s += lw.ObtenirLinia();
				if (i != GW.mida()-1) s += "\n"; 
				i++;
			}
			if (!s.isEmpty()) {
				try  {
					CP.EscriuFitxer(s);
				}
				catch (Exception e) {
					throw e;
				}
			}	
		}	
		else throw new ConjuntBuitException("No hi ha informacio a guardar");
		try {
			CP.TancarEscriptura();
		}
		catch (Exception e) {
			throw e;
		}
	}
	
	// Carrega el fitxer que es passa per paràmetre al programa
	public void CarregarArestes(String path) throws Exception {
	
		G = new Graf<ElemWiki,Relacio,String>(false);
		GW = new GrafWiki();
		try {
			CP.ObrirLectura(path);
		}
		catch (Exception e) {
			throw e;
		}
		
		Parell<String,Integer> p;
		try {
			p = CP.LlegeixFitxer();
		}
		catch (Exception e) {
			throw e;
		}
		String s = new String();
		while (p.ObtenirSegon() > 0) {
			s += p.ObtenirPrimer();
			String[] linies = s.split("\n");
			boolean ultimalinia = false;
			for (int i = 0; i < linies.length; i++) {
				String[] aux = linies[i].split(" ");
				if ((aux.length == 5 && i < linies.length-1) || (aux.length == 5 && s.endsWith("\n"))) {
					try {
						AfegirAresta(aux[0],aux[1],aux[2],aux[3],aux[4]);
					}
					catch (DadesIncorrectesException e) {
						throw e;
					}
				}
				else if (i < linies.length-1) throw new DadesIncorrectesException("Parametres d'informacio d'aresta incorrectes -> " + aux.length);
				else ultimalinia = true;
			} 
			s = new String("");
			if (ultimalinia) s = new String(linies[linies.length-1]);
			try {
				p = CP.LlegeixFitxer();
			}
			catch (Exception e) {
				throw e;
			}
		}
		if (!s.isEmpty() && s != null) {
			String[] aux = s.split(" ");
			if (aux.length == 5) {
				if (aux[4].startsWith("page")) {
					try {
						AfegirAresta(aux[0],aux[1],aux[2],aux[3],"page");
					}
					catch (DadesIncorrectesException e) {
						throw e;
					}
				}	
				else if (aux[4].startsWith("cat")) {
					try {
						AfegirAresta(aux[0],aux[1],aux[2],aux[3],"cat");
					}
					catch (DadesIncorrectesException e) {
						throw e;
					}
				}
				else throw new DadesIncorrectesException("Tipus d'element no existent: " + aux[4]);
			}
			else if (aux.length == 1) {
                                // Error que provoca un caràcter desconegut
			}
			else throw new DadesIncorrectesException("Parametres d'informacio d'aresta incorrectes -> " + aux.length);
		}
		try {
			CP.TancarLectura();
		}
		catch (Exception e) {
			throw e;
		}
	}
	
	public void GuardarVertexs(String path) throws Exception {
		try {
			CP.ObrirEscriptura(path);
		}
		catch (Exception e) {
			throw e;
		}
		if (GW.ordrePags() > 0) {
                        Iterator<String> it = GW.iteratorPags();
			String s = it.next() + " page";
			if (GW.ordrePags() > 1) s += "\n";
			int i = 1;
			while (it.hasNext()) {
                                String aux = it.next() + " page";
				if (s.length() + aux.length() + 1 >= midabloc) {
					try {
						CP.EscriuFitxer(s);
					}
					catch (Exception e) {
						throw e;
					}
					s = new String();
				}
				s += aux;
				if (i != GW.ordrePags()-1) s += "\n"; 
				i++;
			}
			if (!s.isEmpty()) {
				try  {
					CP.EscriuFitxer(s);
				}
				catch (Exception e) {
					throw e;
				}
			}	
		}
		if (GW.ordreCats() > 0) {
                        String s = new String();
                        if (GW.ordrePags() > 0) s = "\n";
                        Iterator<String> it = GW.iteratorCats();
			s += it.next() + " cat";
			if (GW.ordreCats() > 1) s += "\n";
			int i = 1;
			while (it.hasNext()) {
                                String aux = it.next() + " cat";
				if (s.length() + aux.length() + 1 >= midabloc) {
					try {
						CP.EscriuFitxer(s);
					}
					catch (Exception e) {
						throw e;
					}
					s = new String();
				}
				s += aux;
				if (i != GW.ordreCats()-1) s += "\n"; 
				i++;
			}
			if (!s.isEmpty()) {
				try  {
					CP.EscriuFitxer(s);
				}
				catch (Exception e) {
					throw e;
				}
			}	
		}
		if (GW.ordreCats() == 0 && GW.ordrePags() == 0) throw new ConjuntBuitException("No hi ha informacio a guardar");
		try {
			CP.TancarEscriptura();
		}
		catch (Exception e) {
			throw e;
		}
	}
	
	// Carrega el fitxer que es passa per paràmetre al programa
	public void CarregarVertexs(String path) throws Exception {
	
		try {
			CP.ObrirLectura(path);
		}
		catch (Exception e) {
			throw e;
		}
		
		Parell<String,Integer> p;
		try {
			p = CP.LlegeixFitxer();
		}
		catch (Exception e) {
			throw e;
		}
		String s = new String();
		while (p.ObtenirSegon() > 0) {
			s += p.ObtenirPrimer();
			String[] linies = s.split("\n");
			boolean ultimvertex = false;
			for (int i = 0; i < linies.length; i++) {
				String[] aux = linies[i].split(" ");
				if ((aux.length == 2 && i < linies.length-1) || (aux.length == 2 && s.endsWith("\n"))) {
					try {
						AfegirNode(aux[0],aux[1]);
					}
					catch (DadesIncorrectesException e) {
						throw e;
					}
				}
				else if (i < linies.length-1) throw new DadesIncorrectesException("Parametres d'informacio de vèrtex incorrectes -> " + aux.length);
				else ultimvertex = true;
			} 
			s = new String("");
			if (ultimvertex) s = new String(linies[linies.length-1]);
			try {
				p = CP.LlegeixFitxer();
			}
			catch (Exception e) {
				throw e;
			}
		}
		if (!s.isEmpty() && s != null) {
			String[] aux = s.split(" ");
			if (aux.length == 2) {
				if (aux[1].startsWith("page")) {
					try {
						AfegirNode(aux[0],"page");
					}
					catch (DadesIncorrectesException e) {
						throw e;
					}
				}	
				else if (aux[1].startsWith("cat")) {
					try {
						AfegirNode(aux[0],"cat");
					}
					catch (DadesIncorrectesException e) {
						throw e;
					}
				}
				else throw new DadesIncorrectesException("Tipus d'element no existent: " + aux[4]);
			}
			else if (aux.length == 1) {
                                // Error que provoca un caràcter desconegut
			}
			else throw new DadesIncorrectesException("Parametres d'informacio de vèrtex incorrectes -> " + aux.length);
		}
		try {
			CP.TancarLectura();
		}
		catch (Exception e) {
			throw e;
		}
	}
	
	// true si aresta afegida, false si aresta ja existeix
	public void AfegirAresta(String NodeOrigen, String TipusNodeOrigen, String TipusRelacio, String NodeDesti, String TipusNodeDesti) throws DadesIncorrectesException {
		LiniaWiki l = new LiniaWiki(NodeOrigen,TipusNodeOrigen,TipusRelacio,NodeDesti,TipusNodeDesti);
		boolean b = false;
		try {
                        b = GW.ExisteixLinia(l);
		}
		catch (DadesIncorrectesException e) {
                        throw e;
		}
		if (!b) {
                        try {
                                GW.AfegirLinia(l);
                        }
                        catch (DadesIncorrectesException e) {
                                throw e;
                        }
		
                        ElemWiki e1;
                        if(TipusNodeOrigen.equals("page")) e1 = new Pagina(NodeOrigen);
                        else if (TipusNodeOrigen.equals("cat")) e1 = new Categoria(NodeOrigen);
                        else throw new DadesIncorrectesException("Tipus d'element no existent: " + TipusNodeOrigen);
                        
                        ElemWiki e2;
                        if(TipusNodeDesti.equals("page")) e2 = new Pagina(NodeDesti);
                        else if (TipusNodeDesti.equals("cat")) e2 = new Categoria(NodeDesti);
                        else throw new DadesIncorrectesException("Tipus d'element no existent: " + TipusNodeDesti);
                        
                        Relacio a;
                        if (TipusRelacio.equals("CP")) a = new RelacioCP(e1,e2);
                        else if (TipusRelacio.equals("PC")) a = new RelacioPC(e1,e2);
                        else if (TipusRelacio.equals("CsubC")) a = new RelacioCsubC(e1,e2);
                        else if (TipusRelacio.equals("CsupC")) a = new RelacioCsupC(e1,e2);
                        else if (TipusRelacio.equals("PP")) a = new RelacioPP(e1,e2);
                        else throw new DadesIncorrectesException("Tipus de relacio no existent: " + TipusRelacio);
                        
                        G.AfegirAresta(a);
                }
                else throw new DadesIncorrectesException("L'aresta ja existeix");
	}
	
	// true si aresta eliminada, false si no existeix
	public void EsborrarAresta(String NodeOrigen, String TipusNodeOrigen, String TipusRelacio, String NodeDesti, String TipusNodeDesti) throws DadesIncorrectesException{
		LiniaWiki l = new LiniaWiki(NodeOrigen,TipusNodeOrigen,TipusRelacio,NodeDesti,TipusNodeDesti);
		GW.EsborrarLinia(l);
		
		ElemWiki e1;
		if(TipusNodeOrigen.equals("page")) e1 = new Pagina(NodeOrigen);
		else if (TipusNodeOrigen.equals("cat")) e1 = new Categoria(NodeOrigen);
		else throw new DadesIncorrectesException("Tipus d'element no existent: " + TipusNodeOrigen);
		
		ElemWiki e2;
		if(TipusNodeDesti.equals("page")) e2 = new Pagina(NodeDesti);
		else if (TipusNodeDesti.equals("cat")) e2 = new Categoria(NodeDesti);
		else throw new DadesIncorrectesException("Tipus d'element no existent: " + TipusNodeDesti);
		
		G.BorrarAresta(e1, e2);
	}
	
	// true si node afegit, false si ja existeix
	public boolean AfegirNode(String node, String tipusNode) throws DadesIncorrectesException {
		try {
                        GW.AfegirNode(node,tipusNode);
		}
		catch (DadesIncorrectesException de) {}
                ElemWiki e;
                if (tipusNode.equals("page")) e = new Pagina(node);
                else if (tipusNode.equals("cat")) e = new Categoria(node);
                else throw new DadesIncorrectesException("Tipus d'element no existent: " + tipusNode);
                G.AfegirVertex(e);
                return true;
	}
	
	// true si node esborrat, false si no existia
	public void EliminarNode(String node, String tipusNode) throws DadesIncorrectesException {
		
		try {
                        GW.EsborrarNode(node,tipusNode);
                }
                catch (DadesIncorrectesException de) {
                        throw de;
                }
                ElemWiki e;
                if(tipusNode.equals("page")) e = new Pagina(node);
                else if (tipusNode.equals("cat")) e = new Categoria(node);
                else throw new DadesIncorrectesException("Tipus d'element no existent: " + tipusNode);
                G.BorrarVertex(e);
	}

	// true si node modificat, false si node no existeix
	public void ModificarVertex(String nomvell, String tipus, String nomnou) throws DadesIncorrectesException {
                try {
                        GW.ModificarNomNode(nomvell,tipus,nomnou);
                }
                catch (DadesIncorrectesException de) {
                        throw de;
                }
                ElemWiki e1, e2;
                if (tipus.equals("page")) {
                        e1 = new Pagina(nomvell);
                        e2 = new Pagina(nomnou);
                }
                else if (tipus.equals("cat")) {
                        e1 = new Categoria(nomvell);
                        e2 = new Categoria(nomnou);
                }
                else throw new DadesIncorrectesException("El tipus " + tipus + " no existeix");
                G.ModificarVertex(e1,e2);
	}
	
	
	public void ModificarAresta(String NodeOrigen, String TipusNodeOrigen, String TipusRelacio, String NodeDesti, String TipusNodeDesti) throws DadesIncorrectesException {
                LiniaWiki l = new LiniaWiki(NodeOrigen,TipusNodeOrigen,TipusRelacio,NodeDesti,TipusNodeDesti);
                boolean b = false;
                try {
                        b = GW.ExisteixLinia(l);
                }
                catch (DadesIncorrectesException e) {
                        throw e;
                }
                if (b) {
                        try {
                                GW.ModificarLinia(l);
                        }
                        catch (DadesIncorrectesException e) {
                                throw e;
                        }
                        
                        ElemWiki e1;
                        if (TipusNodeOrigen.equals("page")) e1 = new Pagina(NodeOrigen);
                        else if (TipusNodeOrigen.equals("cat")) e1 = new Categoria(NodeOrigen);
                        else throw new DadesIncorrectesException("Tipus d'element no existent: " + TipusNodeOrigen);
                        
                        ElemWiki e2;
                        if (TipusNodeDesti.equals("page")) e2 = new Pagina(NodeDesti);
                        else if (TipusNodeDesti.equals("cat")) e2 = new Categoria(NodeDesti);
                        else throw new DadesIncorrectesException("Tipus d'element no existent: " + TipusNodeDesti);
                        
                        try {
                                G.ModificarTipusAresta(e1, e2, TipusRelacio);
                        }
                        catch (DadesIncorrectesException de) {
                                throw de;
                        }
                }
                else throw new DadesIncorrectesException("L'aresta no existeix");
	}
	
	
	public String CarregarPrimerBlocLinies() {
                int i = 0;
                itR = GW.iterator();
                String s = new String();
                while (i < 100 && itR.hasNext()) {
                        s += itR.next().ObtenirLinia();
                        s += "\n";
                        i++;
                }
                return s;
	}
	
	
	public String CarregarBlocLiniesNext() {
                int i = 0; 
                String s = new String();
                while (i < 100 && itR.hasNext()) {
                        s += itR.next().ObtenirLinia();
                        s += "\n";
                        i++;
                }
                return s;
	}
	
	public String CarregarPrimerBlocPagines() {
                int i = 0;
                itP = GW.iteratorPags();
                String s = new String();
                while (i < 100 && itP.hasNext()) {
                        s += itP.next();
                        s += "\n";
                        i++;
                }
                return s;
	}
	
	
	public String CarregarBlocPaginesNext() {
                int i = 0; 
                String s = new String();
                while (i < 100 && itP.hasNext()) {
                        s += itP.next();
                        s += "\n";
                        i++;
                }
                return s;
	}
	
	public String CarregarPrimerBlocCategories() {
                int i = 0;
                itC = GW.iteratorCats();
                String s = new String();
                while (i < 100 && itC.hasNext()) {
                        s += itC.next();
                        s += "\n";
                        i++;
                }
                return s;
	}
	
	
	public String CarregarBlocCategoriesNext() {
                int i = 0; 
                String s = new String();
                while (i < 100 && itC.hasNext()) {
                        s += itC.next();
                        s += "\n";
                        i++;
                }
                return s;
	}
	
	
	public int mida() {
                return GW.mida();
	}
	
	public int ordrePags() {
                return GW.ordrePags();
	}
	
	public int ordreCats() {
                return GW.ordreCats();
	}
	
	public Graf<ElemWiki,Relacio,String> obtenirGraf() {
                return G;
	}
	
	public ControladorPersistencia ctrlpersistencia() {
                return CP;
	}
}
	

