package domini;

public class RelacioPP extends Relacio {

	public RelacioPP() {
		super();
	}
	
	public RelacioPP(ElemWiki e1, ElemWiki e2) {
		super(e1,e2,"PP");
	}
}