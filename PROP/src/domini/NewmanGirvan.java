package domini;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Comparator;


public class NewmanGirvan extends AlgorismeCommunityDetection {
	
	public NewmanGirvan(Entrada En, Sortida S) throws Exception {
		try {
                        CercarComunitats(En,S);
                }
                catch (Exception e) {
                        throw e;
                }        
	}
	
	static class ParellComparator implements Comparator<Parell<Integer,Double>> {
		public int compare(Parell<Integer,Double> d1, Parell<Integer,Double> d2) {
			if (d1.ObtenirSegon() > d2.ObtenirSegon()) return 1;
			else if (d1.ObtenirSegon() == d2.ObtenirSegon()) return 0;
			else return -1;
		}
	}
	
	private int MarcarCamins(Map<Integer,Map<Integer,Integer>> m, Vector<List<Integer>> camins, int node) {
		
		/**	Marca dins del Map que conte totes les arestes els camins minims que passen per
		 *  cadascuna d'elles
		 */
		if (camins.get(node).size() == 0) return 0;
		else {
			int CamTotals = 0;
			for (int i = 0; i < camins.get(node).size(); i++) {
				int nodeaux = camins.get(node).get(i);
				int CamNode = 1 + MarcarCamins(m,camins,nodeaux);
				CamTotals += CamNode;
				if (node < nodeaux) {
					CamNode += m.get(node).get(nodeaux);
					m.get(node).put(nodeaux,CamNode);
				}
			}
			// Retorna el nombre de camins totals
			return CamTotals;
		}
	}
	
	private int CalcularBetweenness(Graf G, Map<Integer,Map<Integer,Integer>> m) throws Exception {
		
		/**	Per calcular el betweenness de les arestes primer executem l'algorisme de Dijkstra per cadascun dels 
		 * nodes de graf per tal de coneixer tots els camins minims del graf. Seguidament per cada aresta li sumem
		 * el nombre de camins minims que passen a traves d'ella i d'aquesta forma obtenim el betweenness de les 
		 * arestes del graf
		 */
		int Ncamins = 0;
		int n = G.ordre();
		for (int i = 0; i < n; i++) {
			
			// Creacio de totes les estructures necessaries per executar l'algorisme
			Vector<Double> distancies = new Vector<Double>(n);
			Vector<Integer> camins1 = new Vector<Integer>(n);
			Vector<List<Integer>> camins2 = new Vector<List<Integer>>(n);
			Vector<Boolean> visitats = new Vector<Boolean>(n);
			Comparator<Parell<Integer,Double>> comparator = new ParellComparator();
			PriorityQueue<Parell<Integer,Double>> cua = new PriorityQueue<Parell<Integer,Double>>(n,comparator);
			
			// Inicialitzacio dels tres vectors i la cua de prioritats
			for (int v = 0; v < n; v++) {
				distancies.add(Double.POSITIVE_INFINITY);
				camins1.add(-1);
				List<Integer> aux = new ArrayList<Integer>();
				camins2.add(aux);
				visitats.add(false);
			}
			distancies.set(i,0.0);
			Parell<Integer,Double> p = new Parell<Integer,Double>();
			p.CanviarPrimer(i); p.CanviarSegon(0.0);
			cua.add(p);
			
			// Execucio del Dijkstra
			while (!cua.isEmpty()) {
				int node = cua.poll().ObtenirPrimer();
				if (!visitats.get(node)) {
					Ncamins++;
					List<Parell<Integer,Double>> nadj = new ArrayList<Parell<Integer,Double>>(); 
					try {
                                                nadj = G.ArestesAdjacentsNum(node);
					}
					catch (DadesIncorrectesException e) {
                                                throw e;
					}
					visitats.set(node, true);
					for (int j = 0; j < nadj.size(); j++) {
						int nodevei = nadj.get(j).ObtenirPrimer();
						double distvei = 1-nadj.get(j).ObtenirSegon();
						if (distancies.get(nodevei) > distancies.get(node) + distvei) {
							
							// En cas que la distancia ja hagi estat modificada una vegada com a minim
							// cal esborrar el node vei de la llista del node que haviem trobat en iteracions
							// anteriors
							if (distancies.get(nodevei) < Double.POSITIVE_INFINITY) {
								/*if (camins1.get(nodevei) > -1)*/ camins2.get(camins1.get(nodevei)).remove((Object)nodevei);
							}
							
							camins1.set(nodevei,node);
							distancies.set(nodevei, distancies.get(node) + distvei);
							camins2.get(node).add(nodevei);
							Parell<Integer,Double> aux = new Parell<Integer,Double>(nodevei,distancies.get(nodevei));
							cua.add(aux);
						}
					}
				}
			}
			MarcarCamins(m,camins2,i);
		}
		return Ncamins;
	}
	
	private void EliminarAresta(Graf G, Map<Integer,Map<Integer,Integer>> m, Sortida S) throws Exception {
		
		/**	Troba i elimina l'aresta amb mes betweenness del graf. */
		int n = G.ordre();
		int bwmax = -1;
		int node1 = -1;
		int node2 = -1;
		for (int i = 0; i < n; i++) {
			List<Integer> Laux = new ArrayList<Integer>();
			try {
                                Laux = G.VertexAdjacentsNum(i);
                        }
                        catch (Exception e) {
                                throw e;
                        }
			for (int j = 0; j < Laux.size(); j++) {
				if (i < Laux.get(j) && m.get(i).get(Laux.get(j)) > bwmax) {
						bwmax = m.get(i).get(Laux.get(j));
						node1 = i;
						node2 = Laux.get(j);
				}
					
			}
		}
		try {
                        G.BorrarArestaNum(node1, node2);
                }
                catch (Exception e) {
                        throw e;
                }
		String s = new String();
		try {
                        s = "Aresta Eliminada: " + G.vertex(node1) + " " + G.vertex(node2);
                }
                catch (Exception e) {
                        throw e;
                }
		S.AfegirPas(s);
	}
	
	protected void CercarComunitats(Entrada En, Sortida S) throws Exception {
		
		Graf G = new Graf(true);
		G = En.Graf().clone();
		
		/** El primer pas de l'algorisme consisteix en l'eliminacio de les arestes necessaries per tal
		 *  d'obteir tants components connexos com comunitats vol l'usuari. Per eliminar una aresta cal
		 *  calcular el betweennes de totes les arestes del graf a partir de l'algorisme de Dijkstra i 
		 *  seleccionar la que el tingui mes alt.
		 */
		
		int NComunitatsAObtindre = En.NombreComunitats();
		int NComunitatsObtingudes = 1;
		int NCaminsAnterior = -1;
		while (NComunitatsObtingudes != NComunitatsAObtindre && En.Graf().mida() > 0) {
			
			int NCaminsActual = 0;									// Nombre de camins minims entre vertexs del graf
			
			// Conte les arestes del graf i el nombre de camins minims que passen per cadascuna
			Map<Integer,Map<Integer,Integer>> m = new HashMap<Integer,Map<Integer,Integer>>();
			List<Integer> Laux = new ArrayList<Integer>();
			int n = En.Graf().ordre();
			
			// Inicialitzem el Map amb totes les arestes existents al graf amb 0 camins minims
			for (int i = 0; i < n; i++) {
				try {
                                        Laux = En.Graf().VertexAdjacentsNum(i);
				}
				catch (DadesIncorrectesException e) {
                                        throw e;
				}
				Map<Integer,Integer> Maux = new HashMap<Integer,Integer>();
				for (int j = 0; j < Laux.size(); j++) {
					if (i < Laux.get(j)) Maux.put(Laux.get(j), 0); 
				}
				if (!Maux.isEmpty()) m.put(i, Maux);
			}
			
			String saux = "- Comenca proces de calcular el betweenness de les arestes del Graf";
			S.AfegirPas(saux);
			
			// Calcular el betweenness de les arestes
			try {
                                NCaminsActual = CalcularBetweenness(En.Graf(),m);
			}
			catch (Exception e) {
                                throw e;
			}
			// Si el nombre de camins entre vertexs ha decrementat, es perque s'ha generat un nou component connex
			if (NCaminsAnterior > NCaminsActual) {
				NComunitatsObtingudes++;
				String s = "Nova Comunitat creada";
				S.AfegirPas(s);
			}
			NCaminsAnterior = NCaminsActual;
			
			// Si no s'arriba al nombre de comunitats desitjades s'elimina novament una aresta 
			if (NComunitatsObtingudes != NComunitatsAObtindre) {
				String s = "- Comenca proces d'esborrar l'aresta amb mes betweenness";
				S.AfegirPas(s);
				try {
                                        EliminarAresta(En.Graf(),m,S);	 // Elimina l'aresta amb mes betweenness
                                }
                                catch (Exception e) {
                                        throw e;
                                }
			}
		}
		
		/**	El segon pas de l'algorisme es realitzar unes quantes iteracions mes per tal de refinar una mica
		 *  els resultats, ja que potser l'algorisme ha parat en un punt on quedava poc per la generacio d'una
		 *  nova comunitat. Es segueixen els mateixos passos que abans.
		 */
		
		int it = 20;				// Fem 20 iteracions mes (eliminem 20 arestes mes)
		boolean ComTrobada = false;
		while ((it > 0 && !ComTrobada) && En.Graf().mida() > 0) {
			
			int NCaminsActual = 0;
			
			// Conte les arestes del graf i el nombre de camins minims que passen per cadascuna
			Map<Integer,Map<Integer,Integer>> m = new HashMap<Integer,Map<Integer,Integer>>();
			List<Integer> Laux = new ArrayList<Integer>();
			int n = En.Graf().ordre();
			
			// Inicialitzem el Map amb totes les arestes existents al graf amb 0 camins minims
			for (int i = 0; i < n; i++) {
				try {
                                        Laux = En.Graf().VertexAdjacentsNum(i);
				}
				catch (Exception e) {
                                        throw e;
				}  
				Map<Integer,Integer> Maux = new HashMap<Integer,Integer>();
				for (int j = 0; j < Laux.size(); j++) {
					if (i < Laux.get(j)) Maux.put(Laux.get(j), 0); 
				}
				if (!Maux.isEmpty()) m.put(i, Maux);
			}
			String saux = "- Comenca proces de calcular el betweenness de les arestes del Graf";
			S.AfegirPas(saux);
			
			// Calcular el betweenness de les arestes
			try {
                                NCaminsActual = CalcularBetweenness(En.Graf(),m);
			}
			catch (Exception e) {
                                throw e;
			}    
			// En aquest cas, si el nombre de camins es diferent significa que s'ha trobat una nova comunitat
			// i ja es pot parar el refinament
			if (NCaminsAnterior > NCaminsActual) {
				String s = "S'ha trobat una nova Comunitat en les iteracions posteriors";
				S.AfegirPas(s);		
				ComTrobada = true;
			}
			
			// Si no es troba cap comunitat es pot eliminar una nova aresta
			if (!ComTrobada) {
				String s = "- Comenca proces d'esborrar l'aresta amb mes betweenness";
				S.AfegirPas(s);
				try {
                                        EliminarAresta(En.Graf(),m,S);		// Elimina l'aresta amb mes betweenness
                                }
                                catch (Exception e) {
                                        throw e;
                                }
			}
			it--;
		}

		// Un cop executat l'algorisme, cal fer una cerca dels components connexos del graf, que son les Comunitats
		int n = En.Graf().ordre();
		List<List<Integer>> Comunitats = new ArrayList<List<Integer>>(n);
		Vector<Boolean> visitats = new Vector<Boolean>();
		for (int i = 0; i < n; i++) visitats.add(false);
		
		for (int i = 0; i < n; i++) {
			if (!visitats.get(i)) {
				visitats.set(i,true);
				List<Integer> Comunitat = new ArrayList<Integer>();
				Comunitat.add(i);
				int pos = 0;
				while(pos < Comunitat.size()) {
					int node = Comunitat.get(pos);
					List<Integer> Aux = new ArrayList<Integer>();
					try {
                                                Aux = En.Graf().VertexAdjacentsNum(node);
					}
					catch (Exception e) {
                                                throw e;
					}
					for (int j = 0; j < Aux.size(); j++) {
						if (!visitats.get(Aux.get(j))) {
							visitats.set(Aux.get(j),true);
							Comunitat.add(Aux.get(j));
						}
					}
					pos++;
				}
				Comunitats.add(Comunitat);
			}
		}
		
		// Crear comunitats
		S.afegir_nodes(Comunitats);
		
	}
}
