package domini;
import java.io.IOException;
import java.util.List;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

import persistencia.ControladorPersistencia;
public class Controlador_Algorismes {
		Graf<ElemWiki,ArestaPes,Double> G;
		int NumeroAlgorisme;
		AlgorismeCommunityDetection Alg;
		Entrada E;
		Sortida S;
		Iterator<Integer> it1, it2;
		
		
		//constructora
		public Controlador_Algorismes() {
                        S = new Sortida();
		}
		
		public void reiniciar_sortida() {
                        S.reiniciar();
		}
		
		public void AfegirComunitat() {
                        S.AfegirComunitat();
		}
		
		public void InicialitzarCtrl(Graf<ElemWiki,ArestaPes,Double> g, int NumAlg) {
                        G = g.clone();
			NumeroAlgorisme = NumAlg;
			S = new Sortida();
		}
		
		// Entrada per CliquePercolation
		public void PrepararEntrada(Graf<ElemWiki,Relacio,String> Gaux, int k, double w) throws Exception {
			OrdenacioTopologica ot= new OrdenacioTopologica();
			ot.OrdenarGraf(Gaux);
			List<Integer> pr = ot.PrimeraRepresentacio();
			List<Integer> sr = ot.SegonaRepresentacio();
			E = new Entrada(G,k,w,pr,sr);
		}
		
		// Entrada per Newman-Girvan
		public void PrepararEntrada(int Ncom) throws Exception {
			E = new Entrada(G,Ncom);
		}
		
		public void PrepararEntrada() {
			E = new Entrada(G);
		}
		
		public void PrepararSortida(Graf<ElemWiki,Relacio,String> g) throws Exception {
                                S.Crear_comunitats(g,G);
		}
		
		public void ExecutarAlgorisme() throws Exception {
			long t0 = System.currentTimeMillis();
			switch (NumeroAlgorisme){
			
			case 1:
				Alg = new CliquePercolation(E,S); 
				break;
				
			case 2:
                Alg = new NewmanGirvan(E,S);
				break;
				
			case 3:
				Alg = new Louvain(E,S);
				break;
				
			case 4:
               Alg = new Label(E,S);
				break;
				
			case 5:
				
				Alg = new Baumes(E,S);				
				break;
			}
			long temps = System.currentTimeMillis() - t0;
			S.AfegirTemps(temps);
		}
		
		public Sortida ConsultarSortida() {
			return S;
		}
		
		public void exportarArestes(int ncom, String path, ControladorPersistencia cp) throws Exception{
			
			int midabloc = 1000;
			cp.ObrirEscriptura(path);
                        GrafWiki aux = S.Comunitats_resultat().get(ncom-1);
                        
                        if (aux.mida() > 0) {
                                Iterator<LiniaWiki> it = aux.iterator();
                                String s = it.next().ObtenirLinia();
                                if (aux.mida() > 1) s += "\n";
                                int i = 1;
                                while (it.hasNext()) {
                                        LiniaWiki lw = it.next();
                                        if (s.length() + lw.ObtenirLinia().length() + 1 >= midabloc) {
                                                try {
                                                        cp.EscriuFitxer(s);
                                                }
                                                catch (Exception e) {
                                                        throw e;
                                                }
                                                s = new String();
                                        }
                                        s += lw.ObtenirLinia();
                                        if (i != aux.mida()-1) s += "\n"; 
                                        i++;
                                }
                                if (!s.isEmpty()) {
                                        try  {
                                                cp.EscriuFitxer(s);
                                        }
                                        catch (Exception e) {
                                                throw e;
                                        }
                                }	
                        }	
                        else throw new ConjuntBuitException("No hi ha informacio a guardar");
                        
			cp.TancarEscriptura();
		}
		
		public void exportarNodes(int ncom, String path, ControladorPersistencia cp) throws Exception {
		
                        int midabloc = 1000;
			cp.ObrirEscriptura(path);
                        GrafWiki auxC = S.Comunitats_resultat().get(ncom-1);
			
                        if (auxC.ordrePags() > 0) {
                                Iterator<String> it = auxC.iteratorPags();
                                String s = it.next() + " page";
                                if (auxC.ordrePags() > 1) s += "\n";
                                int i = 1;
                                while (it.hasNext()) {
                                        String aux = it.next() + " page";
                                        if (s.length() + aux.length() + 1 >= midabloc) {
                                                try {
                                                        cp.EscriuFitxer(s);
                                                }
                                                catch (Exception e) {
                                                        throw e;
                                                }
                                                s = new String();
                                        }
                                        s += aux;
                                        if (i != auxC.ordrePags()-1) s += "\n"; 
                                        i++;
                                }
                                if (!s.isEmpty()) {
                                        try  {
                                                cp.EscriuFitxer(s);
                                        }
                                        catch (Exception e) {
                                                throw e;
                                        }
                                }	
                        }
                        if (auxC.ordreCats() > 0) {
                                String s = new String();
                                if (auxC.ordrePags() > 0) s = "\n";
                                Iterator<String> it = auxC.iteratorCats();
                                s += it.next() + " cat";
                                if (auxC.ordreCats() > 1) s += "\n";
                                int i = 1;
                                while (it.hasNext()) {
                                        String aux = it.next() + " cat";
                                        if (s.length() + aux.length() + 1 >= midabloc) {
                                                try {
                                                        cp.EscriuFitxer(s);
                                                }
                                                catch (Exception e) {
                                                        throw e;
                                                }
                                                s = new String();
                                        }
                                        s += aux;
                                        if (i != auxC.ordreCats()-1) s += "\n"; 
                                        i++;
                                }
                                if (!s.isEmpty()) {
                                        try  {
                                                cp.EscriuFitxer(s);
                                        }
                                        catch (Exception e) {
                                                throw e;
                                        }
                                }	
                        }
                        if (auxC.ordreCats() == 0 && auxC.ordrePags() == 0) throw new ConjuntBuitException("No hi ha informacio a guardar");
                        
			cp.TancarEscriptura();

		}
		
		public String CarregarComunitat(int k, boolean b) throws DadesIncorrectesException {
                        int i = 0; 
                        String s = new String();
                        if (b) {
                                it1 = S.iterator(k);
                                while (i < 100 && it1.hasNext()) {
                                        s += G.vertex(it1.next()).toString();
                                        s += "\n";
                                        i++;
                                }
                        }
                        else {
                                it2 = S.iterator(k);
                                while (i < 100 && it2.hasNext()) {
                                        s += G.vertex(it2.next()).toString();
                                        s += "\n";
                                        i++;
                                }
                        }
                        return s;
		}
		
		public String CarregarComunitatNext(boolean b) throws DadesIncorrectesException {
                        int i = 0;
                        String s = new String();
                        if (b) {
                                while (i < 100 && it1.hasNext()) {
                                        s += G.vertex(it1.next()).toString();
                                        s += "\n";
                                        i++;
                                }
                        }
                        else {
                                while (i < 100 && it2.hasNext()) {
                                        s += G.vertex(it2.next()).toString();
                                        s += "\n";
                                        i++;
                                }
                        }
                        return s;
		}
		
		public long getTime() {
                        return S.TempsCPU();
		}
		
		public int NombreComunitats() {
                        return S.NombreComunitats();
		}
		
		public int ConsultarOrdre(int i) {
                        return S.OrdreComunitat(i);
		}
		
		public String ConsultarAdjacentsANode(String nom, String tipus, int comunitat) throws Exception {
                        
                        String s = new String();
                        ElemWiki e;
                        if (tipus.equals("page")) e = new Pagina(nom);
                        else e = new Categoria(nom);
                        List<ElemWiki> adj = G.VertexAdjacents(e);
                        
                        List<ElemWiki> com = new ArrayList<ElemWiki>();
                        for (int i = 0; i < S.LlistaComunitats.get(comunitat-1).size(); i++) {
                                if (G.vertex(S.LlistaComunitats.get(comunitat-1).get(i)).equals(e)) throw new DadesIncorrectesException("El node ja es troba en la comunitat");
                                com.add(G.vertex(S.LlistaComunitats.get(comunitat-1).get(i)));
                        }
                        for (int i = 0; i < adj.size(); i++) {
                                if (com.contains(adj.get(i))) {
                                        s += adj.get(i).toString();
                                        s += "\n";
                                }
                        }
                        return s;
		}
		
		public void BorrarNodeComunitat(String nom, String tipus, int comunitat) throws Exception {
		
                        ElemWiki e;
                        if (tipus.equals("page")) e = new Pagina(nom);
                        else e = new Categoria(nom);
                        
                        S.Comunitats_resultat().get(comunitat-1).EsborrarNode(nom, tipus);
                        S.LlistaComunitats.get(comunitat-1).remove((Integer)G.vertex(e));
		}
		
		public void AfegirNodeComunitat(String nom, String tipus, int comunitat, String relacions, Graf<ElemWiki,Relacio,String> GW) throws Exception {
		
                        ElemWiki e;
                        if (tipus.equals("page")) e = new Pagina(nom);
                        else e = new Categoria(nom);
                        
                        S.LlistaComunitats.get(comunitat-1).add(G.vertex(e));
                        S.Comunitats_resultat().get(comunitat-1).AfegirNode(nom, tipus);
                        
                        if (!relacions.isEmpty()) {
                                String[] linies = relacions.split("\n");
                                for (int k = 0; k < linies.length; k++) {
                                        String[] elem = linies[k].split(" ");
                                        
                                        ElemWiki aux;
                                        if (elem[1].equals("page")) aux = new Pagina(elem[0]);
                                        else aux = new Categoria(elem[0]);
                                        
                                        if (GW.ExisteixAresta(e, aux)) {
                                                List<Parell<ElemWiki,List<String>>> l = GW.ArestesAdjacentsDirigit(e);
                                                for (int i = 0; i < l.size(); i++) {
                                                        if (l.get(i).ObtenirPrimer().equals(aux)) {
                                                                for (int j = 0; j < l.get(i).ObtenirSegon().size(); j++) {
                                                                        LiniaWiki lw = new LiniaWiki(e.NomNode(), e.quisoc(), l.get(i).ObtenirSegon().get(j), aux.NomNode(), aux.quisoc());
                                                                        S.Comunitats_resultat().get(comunitat-1).AfegirLinia(lw);
                                                                }
                                                        }
                                                }
                                        }
                                        if (GW.ExisteixAresta(aux,e)) {
                                        
                                                List<Parell<ElemWiki,List<String>>> l = GW.ArestesAdjacentsDirigit(aux);
                                                for (int i = 0; i < l.size(); i++) {
                                                        if (l.get(i).ObtenirPrimer().equals(e)) {
                                                                for (int j = 0; j < l.get(i).ObtenirSegon().size(); j++) {
                                                                        LiniaWiki lw = new LiniaWiki(aux.NomNode(), aux.quisoc(), l.get(i).ObtenirSegon().get(j), e.NomNode(), e.quisoc());
                                                                        S.Comunitats_resultat().get(comunitat-1).AfegirLinia(lw);
                                                                }
                                                        }
                                                } 
                                        }
                                }
                        }
		
		}
		
}
