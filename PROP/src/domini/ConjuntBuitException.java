package domini;

public class ConjuntBuitException extends Exception {

	public ConjuntBuitException() {
		super();
	}
	
	public ConjuntBuitException(String missatge) {
		super(missatge);
	}

}
