package domini;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class Jaro_winkler extends AlgorismeDistancia{

	double p; // constant per a utilitzar jaro winkler. Entre 0 i 0,25 si es volen similituds positives.
	double llindar; // constant a partir de la qual s'utilitza winkler per a ajustar la distancia de jaro. entre 0 i 1.
	
	//Constructora amb un graf input. Per defecte p = 0.1  i llindar = 0.7
	public Jaro_winkler(Graf<ElemWiki,Relacio,String> g){
		super(g);
		p = 0.1;
		llindar = 0.7;
	}
	
	// modificadores

	public void modificar_p(double pes) throws DadesIncorrectesException{
		
		if(pes < 0.0 || pes > 0.25) throw new DadesIncorrectesException("La p de jaro winkler ha d'estar entre 0 i 0.25");
		p = pes;
	}
	
	public void modificar_llindar(double llin) throws DadesIncorrectesException{
		
		if(llin < 0.0) throw new DadesIncorrectesException("el llindar de jaro winkler ha de ser positiu o 0");
		llindar = llin;
	}
	//Posa les constants amb els seus valors per defecte
	public void valors_per_defecte(){
		llindar = 0.7;
		p = 0.1;
	}
	//Converteix el graf wikiedia en un graf no dirigit amb pesos de 0 a 1.
	public void convert() throws Exception { 
		super.convert();
		int n = graf_wiki.ordre();
		boolean[] visitats = new boolean[n];
		Arrays.fill(visitats,false);
		for(int i = 0; i < n; ++i){

			LinkedList<Integer> q = new LinkedList<Integer>();
			if(!visitats[i]){
				q.add(i);
				visitats[i] = true;
			}
			while(!q.isEmpty()){
				int actual = q.remove();

				List<Integer> adjacents = new ArrayList<Integer>();
				adjacents = graf_wiki.VertexAdjacentsNum(actual);
				graf_pes.AfegirVertex(graf_wiki.vertex(actual));
				
				for(int j = 0; j < adjacents.size();++j){
					//Si no estan visitats s'afageixen
					int vei = adjacents.get(j);
					if(!visitats[vei]){
						String origen = null;
						origen = graf_wiki.vertex(actual).NomNode();
						
						String desti = null;
						desti = graf_wiki.vertex(vei).NomNode();
						
						String A;
						String B;
						if(origen.length() >= desti.length()){
							A = origen;
							B = desti;
						}
						else{
							B = origen;
							A = desti;
						}
						
						double pes = calcularPes(A,B);
						graf_pes.AfegirVertex(graf_wiki.vertex(vei));
						
						graf_pes.AfegirAresta(graf_wiki.vertex(actual), pes, graf_wiki.vertex(vei));
						
						visitats[vei] = true;
						q.add(vei);
					}
					//Altrament es mira si ja s'ha afegit l'aresta i en cas negatiu s'afegeix
					else{
						
						
						graf_pes.AfegirVertex(graf_wiki.vertex(vei));
						
						if(!graf_pes.ExisteixAresta(graf_wiki.vertex(vei), graf_wiki.vertex(actual))){
									
								String origen = graf_wiki.vertex(actual).NomNode();
								String desti = graf_wiki.vertex(vei).NomNode();
								String A;
								String B;
								if(origen.length() >= desti.length()){
									A = origen;
									B = desti;
								}
								else{
									B = origen;
									A = desti;
								}
								
								double pes = calcularPes(A,B);
								graf_pes.AfegirAresta(graf_wiki.vertex(actual), pes,graf_wiki.vertex(vei));
							}
						
						
					}
				}
			}
			
		}
		
		
		
	}
	//consultores
	
	
	public double llindar_winkler(){
		return llindar;
	}
	
	public double p_winkler(){
		return p;
	}
	

	
	//Metodes privats
	
	//Calcula el pes d'una aresta basant-se en els noms.
	public double calcularPes(String A, String B){
			double dj = jaro(A,B);
			if(dj < llindar) return dj;
			else{
				int l = 0;
				for(int i = 0; i < 4 && i < A.length() && i < B.length() && A.charAt(i)==B.charAt(i); ++i){
					++l;
				}
				return dj + ((double)l*p*(1.0-dj)); //correció winkler
			}
			
		
	}
	//aplica la distancia de jaro.
	private double jaro(String A, String B){
		
		int a = A.length();
		int b = B.length();
		int rang = a/b -1;
		boolean[] igualA = new boolean[a];
		boolean[] igualB = new boolean[b];
		Arrays.fill(igualA, false);
		Arrays.fill(igualB, false);
		int m = 0;
		int ht = 0;
		int t = 0;
		for(int i = 0; i < a; ++i){
			int esq = Math.max(i-rang,0);
			int dre  = Math.min(i+rang+1, b);
			for(int j = esq; j < dre; ++j){
				if((A.charAt(i)==B.charAt(j)) && !igualB[j]){
					igualA[i] = true;
					igualB[j] = true;
					++m;
					break;
				}
			}
		}
		if(m == 0) return 0.0;
		int esq = 0;
		int j = 0;
		for(int i = 0; i < a; ++i){
			if(igualA[i]){
				for(j = esq; j < b ; ++j){
					if(igualB[j]){
						esq = j + 1;
						break;
					}
				}
				if(A.charAt(i) != B.charAt(j)) ++ht;
			}
		}
		t = ht/2;
		return ((double)1/(double)3)*((double)m/(double)a + (double)m/(double)b + ((double)m-(double)t)/(double)m);
	}
	



}
