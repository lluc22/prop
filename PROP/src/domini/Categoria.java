package domini;
public class Categoria extends ElemWiki {

	/*Crea una categoria buida*/
	public Categoria(){
		super();
	}
	
	/*Crea una categoria amb nom = s*/
	public Categoria(String s){
		super(s);
	}
	
	/*Retorna categoria*/
	public String quisoc(){
		return "cat";
	}
	
	/*True si this = obj, false altrament*/
	public boolean equals(Object o){
		return super.equals(o);
	}
}
