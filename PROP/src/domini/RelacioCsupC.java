package domini;

public class RelacioCsupC extends Relacio {

	public RelacioCsupC() {
		super();
	}
	
	public RelacioCsupC(ElemWiki e1, ElemWiki e2) {
		super(e1,e2,"CsupC");
	}
}