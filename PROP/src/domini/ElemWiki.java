package domini;
public class ElemWiki {
	String nom;
	
	//constructora
	/**Crea un ElemWiki buit*/
	public ElemWiki(){}
	
	/** Crea un ElemWiki amb un nom*/
	public ElemWiki(String n){
		nom = n;
	}
	//consultora
	/** Retorna el nom del ElemWiki*/
	public String NomNode(){
		return nom;
	}

	public String toString(){
		return nom + " " + quisoc();	
	}

	public String quisoc() {
		return null;
	}

	@Override
	public boolean equals(Object obj) {
		return this.toString().equals(obj.toString());
	}
	
	public ElemWiki clone() {
                String aux = new String(nom);
                ElemWiki e = new ElemWiki(aux);
                return e;
	}
	
}
