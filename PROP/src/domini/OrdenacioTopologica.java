package domini;

import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;

public class OrdenacioTopologica {
	List<Integer> PrimeraRepresentacio;
	List<Integer> SegonaRepresentacio;
	
	public OrdenacioTopologica() {
		PrimeraRepresentacio = new ArrayList<Integer>();
		SegonaRepresentacio = new ArrayList<Integer>();
	}
	
	public void OrdenarGraf(Graf G) throws Exception {
		/**	La primera part de l'algorisme consisteix en trobar les arrels
		 * del graf, les quals tenen grau d'entrada igual a 0.
		 */
		int n = G.ordre();
		List<Boolean> visitats = new ArrayList<Boolean>(n);
		for (int i = 0; i < n; i++) visitats.add(false);
		for (int i = 0; i < n; i++) {
			List<Integer> Vadj = new ArrayList<Integer>();
			try {
                                Vadj = G.VertexAdjacentsNum(i);
                        }
                        catch (Exception e) {
                                throw e;
                        }
			for (int j = 0; j < Vadj.size(); j++) {
				visitats.set(Vadj.get(j), true);
			}
		}
		
		LinkedList<Integer> arrels = new LinkedList<Integer>();
		for (int i = 0; i < n; i++) {
			if (!visitats.get(i)) arrels.add(i);
			visitats.set(i,false);
			PrimeraRepresentacio.add(-1);
		}
		
		if (arrels.isEmpty()) throw new GrafSenseArrelsException("No es pot aplicar l'ordenació topològica");
		
		int pos = 0;
		while (!arrels.isEmpty()) {
			int node = arrels.poll();
			PrimeraRepresentacio.set(node,pos);
			SegonaRepresentacio.add(node);
			pos++;
			List<Integer> Laux = new ArrayList<Integer>();
			try {
                                Laux = G.VertexAdjacentsNum(node);
                        }
                        catch (Exception e) {
                                throw e;
                        }
			for (int i = 0; i < Laux.size(); i++) {
				if (!visitats.get(Laux.get(i))) {
					arrels.add(Laux.get(i));
					visitats.set(Laux.get(i),true);
				}
			}
		}
	}
	
	public List<Integer> PrimeraRepresentacio() {
		return PrimeraRepresentacio;
	}
	
	public List<Integer> SegonaRepresentacio() {
		return SegonaRepresentacio;
	}
	
}
