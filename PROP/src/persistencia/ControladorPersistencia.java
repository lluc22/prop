package persistencia;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import domini.Parell;


public class ControladorPersistencia {
	
	BufferedWriter bw;
	BufferedReader br;
	FileReader fr;
	FileWriter fw;
	int MidaBloc;
	
	public ControladorPersistencia(int mida) {
		MidaBloc = mida;
	}	
	
	/* PRE: existeix un fitxer amb ruta = path
	 * Obra un fitxer amb ruta = path i el prepara per comensar a llegir*/
	public void ObrirLectura(String path) throws IOException{
		try {
			fr = new FileReader(path);
			br = new BufferedReader(fr);
		}
		catch (IOException e) {
			throw new IOException("Error obrint el fitxer o BufferedReader per llegir");
		}	
	}

	/*Obra un fitxer amb ruta = path i el prepara per comensar a escriure,
	  si el fitxer no existeix el crea*/
	public void ObrirEscriptura(String path) throws Exception{
		File file;
		try {
			file = new File(path);
		}
		catch (Exception e) {
			throw new Exception("Error en la creacio del nou fitxer");
		}
		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}
		
		try {
			fw = new FileWriter(file.getAbsolutePath(),false);
			fw.write("");
			bw = new BufferedWriter(fw);
		}
		catch (IOException e) {
			throw new IOException("Error obrint el fitxer o BufferedWriter per escriure");
		}
		
	}
	
	/*Si hi ha algun fitxer obert el tenca*/
	public void TancarEscriptura() throws IOException{
		try {
			bw.close();
		}
		catch (IOException e) {
			throw new IOException("Error tancant el BufferedWriter");
		}
		fw = null;
		
	}
	
	public void TancarLectura() throws IOException {
		try {
			br.close();
		}
		catch (IOException e) {
			throw new IOException("Error tancant el BufferedWriter");
		}
		fr = null;
	}
	
	/* PRE: s'ha obert un fitxer amb mode escriptura 
	 * Escriu el string s al final del fitxer obert previament*/
	public void EscriuFitxer(String s) throws Exception {
		if (s.length() < MidaBloc) {
			try {
				bw.write(s,0,s.length());
			}
			catch (IOException e) {
				throw new IOException("Error en l'escriptura d'un bloc");
			}
		}	
		else throw new MidaBlocException("Mida de String massa gran");
	}
	
	public Parell<String,Integer> LlegeixFitxer() throws IOException {
		String s;
		int n;
		char[] c = new char[MidaBloc];
		try {
			n = br.read(c,0,MidaBloc);
			s = new String(c);
		}
		catch (IOException e) {
			throw new IOException("Error en la lectura d'un bloc");
		}
		Parell p = new Parell(s,n);
		return p;
	}	
	
	// Modificar la mida del bloc de línies
	public void ModificarMidaBloc(int mida) throws MidaBlocException {
		if (mida > 0) MidaBloc = mida;
		else throw new MidaBlocException("La mida de bloc no pot ser negativa");
	}	
	
}
