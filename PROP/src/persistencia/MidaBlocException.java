package persistencia;

public class MidaBlocException extends Exception {
  
	public MidaBlocException() {
		super();
	}
	
	public MidaBlocException(String missatge) {
		super(missatge);
	}
	
	
}
