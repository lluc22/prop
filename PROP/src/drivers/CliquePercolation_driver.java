package drivers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class CliquePercolation_driver {
	
	public static void main (String[] args) throws Exception {
		
		Scanner sc = new Scanner(System.in);
		int k = sc.nextInt();
		double w = sc.nextDouble();
		Graf<String,Aresta<String,Double>,Double> G1 = new Graf<String,Aresta<String,Double>,Double>(true);
		Graf<String,Aresta<String,String>,String> G2 = new Graf<String,Aresta<String,String>,String>(false);
		String tipus = "t";
		while (sc.hasNext()) {
			String s1 = sc.next();
			double pes = sc.nextDouble();
			String s2 = sc.next();
			G1.AfegirAresta(s1, pes, s2);
			G2.AfegirAresta(s1, tipus, s2);
		}
		sc.close();
		
		OrdenacioTopologica<String,Aresta<String,String>,String> ot = new OrdenacioTopologica<String,Aresta<String,String>,String>();
		ot.OrdenarGraf(G2);
		List<Integer> pr = ot.PrimeraRepresentacio();
		List<Integer> sr = ot.SegonaRepresentacio();

		
		CliquePercolation X;
		Entrada E = new Entrada(G1,k,w,pr,sr);
		Sortida S = new Sortida();
		try {
			X = new CliquePercolation(E,S);
		}
		catch (Exception e) {
			throw new Exception("Error en cerca de comunitats");
		}	

		List<String> seguiment = S.Consultar_passos();
		for (int i = 0; i < seguiment.size(); i++) {
			System.out.println(seguiment.get(i));
		}
		
		List<Graf> Comunitats = new ArrayList<Graf>();
		Comunitats = S.Comunitats_resultat();
		for (int i = 0; i < Comunitats.size(); i++) {
			System.out.println("Comunitat " + (i+1) + ":");
			List<String> Components = new ArrayList<String>();
			Components = Comunitats.get(i).ListaVertex();
			for (int j = 0; j < Components.size(); j++) {
				System.out.println("Component " + (j+1) + ": " + Components.get(j));
			}
		}
	}
}
