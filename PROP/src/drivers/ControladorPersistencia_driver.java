package drivers;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import domini.*;

public class ControladorPersistencia_driver {
	static ControladorPersistencia CP;
	static void menu_inici(){
		System.out.println("Creadores:");
		System.out.println("1: ControladorPersistencia");
		System.out.println("2, path: void obrirLectura(String path)");
		System.out.println("3, path: void obrirEscriptura(String path)");
		System.out.println("4: void tencar()");
		System.out.println("5, linies: String llegir(int linies)");
		System.out.println("6, s: void escriure(String s)");
		System.out.println("-1: sortir");
	}
	public static void main(String[] args)throws IOException {
		menu_inici();
		Scanner s = new Scanner(System.in);
		int x = s.nextInt();
		while (x != -1){
			switch (x){
				case 1:
					CP = new ControladorPersistencia();
					break;
				case 2:
					CP.obrirLectura(s.next());
					break;
				case 3:
					CP.obrirEscriptura(s.next());
					break;
				case 4:
					CP.tencar();		
					break;
				case 5:
					System.out.println(CP.llegir(s.nextInt()));
					break;
				case 6:
					CP.escriure(s.next());
					break;
			}
			x = s.nextInt();
		}
		s.close();
	}
}
