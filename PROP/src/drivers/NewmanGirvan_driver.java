package drivers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class NewmanGirvan_driver {
	
	public static void main (String[] args) throws FileNotFoundException {
		
		Scanner sc = new Scanner(System.in);
		Graf<String,Aresta<String,Double>,Double> G = new Graf<String,Aresta<String,Double>,Double>(true);
		while (sc.hasNext()) {
			String s1 = sc.next();
			double pes = sc.nextDouble();
			String s2 = sc.next();
			G.AfegirAresta(s1, pes, s2);
		}
		sc.close();
		
		NewmanGirvan X;
		Entrada E = new Entrada(G,2);
		Sortida S = new Sortida();
		try {
			X = new NewmanGirvan(E,S);
		}
		catch (Exception e) {
		
		}

		List<String> seguiment = S.Consultar_passos();
		for (int i = 0; i < seguiment.size(); i++) {
			System.out.println(seguiment.get(i));
		}
		
		List<Graf> Comunitats = new ArrayList<Graf>();
		Comunitats = S.Comunitats_resultat();
		for (int i = 0; i < Comunitats.size(); i++) {
			System.out.println("Comunitat " + (i+1) + ":");
			List<String> Components = new ArrayList<String>();
			Components = Comunitats.get(i).ListaVertex();
			for (int j = 0; j < Components.size(); j++) {
				System.out.println("Component " + (j+1) + ": " + Components.get(j));
			}
		}
	}
}
