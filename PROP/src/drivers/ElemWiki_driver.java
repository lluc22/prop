package drivers;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import domini.*;


public class ElemWiki_driver {
	static void menu_inici(){
		System.out.println("Creadores:");
		System.out.println("1: ElemWiki()");
		System.out.println("2, nom: ElemWiki(String nom)");
		System.out.println("Consultores:");
		System.out.println("3: String NomNode()");
		System.out.println("4: String toString()");		
		System.out.println("-1: sortir");
		
	}

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		menu_inici();
		Scanner s = new Scanner(System.in);
		int i = s.nextInt();
		ElemWiki e = new ElemWiki();
		while (i != -1){
			switch (i){
				case 1:
					e = new ElemWiki();
					break;
				case 2:
					e = new ElemWiki(s.next());
					break;
				case 3:
					System.out.println(e.NomNode());
					break;
				case 4:
					System.out.println(e.toString());
					break;
			}
			i = s.nextInt();
		}
		s.close();
		
	}

}
