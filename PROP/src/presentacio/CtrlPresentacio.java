package presentacio;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import domini.Controlador_Algorismes;
import domini.ControladorTransformacioGraf;
import domini.ControladorWiki;

public class CtrlPresentacio {
        //Inicialitzar nous controladors domini
    
        //Inicialitzar nous subcontroladors presentacio
        Controlador_Algorismes CA;
        ControladorTransformacioGraf CT;
        ControladorWiki CW;
        CtrlPresentacioCerca CPC;
        CtrlPresentacioGestio CPGG;
        CtrlPresentacioFitxer CPF;
        CtrlPresentacioGrafPanell CPG;
        VistaPrincipal v;
        
        public CtrlPresentacio () {
                CA = new Controlador_Algorismes();
                CT = new ControladorTransformacioGraf();
                CW = new ControladorWiki();
                CPG = new CtrlPresentacioGrafPanell(CW,CA);
                CPC = new CtrlPresentacioCerca(CA,CT,CW,CPG);
                CPGG = new CtrlPresentacioGestio(CW,CPG);
                CPF = new CtrlPresentacioFitxer(CW,CA,CPG);
                v = new VistaPrincipal(this);
        }
        
        public void inicialitzarPresentacio() {
                v.pack();
                v.setVisible(true);
        }
        
        public CtrlPresentacioCerca getCPC() {
                return CPC;
        }
        
        public CtrlPresentacioGestio getCPGG() {
                CA.reiniciar_sortida();
                CPC.ModificarValorRepresentacio(0);
                return CPGG;
        }
        
        public CtrlPresentacioFitxer getCPF() {
                return CPF;
        }
        
        public CtrlPresentacioGrafPanell getCPG() {
                return CPG;
        }
        
        public static void main(String[] args) {
                CtrlPresentacio c = new CtrlPresentacio();
        }
        
    

}
