package presentacio;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import domini.Controlador_Algorismes;
import domini.ControladorTransformacioGraf;
import domini.ControladorWiki;
import domini.GrafSenseArrelsException;
import domini.DadesIncorrectesException;

public class CtrlPresentacioCerca {
    
    Controlador_Algorismes CA;
    ControladorTransformacioGraf CT;
    ControladorWiki CW;
    CtrlPresentacioGrafPanell CPGP;
    int graf_a_dibuixar;
    
    public CtrlPresentacioCerca (Controlador_Algorismes ca, ControladorTransformacioGraf ct, ControladorWiki cw, CtrlPresentacioGrafPanell cpgp) {
            CA = ca;
            CT = ct;
            CW = cw;
            CPGP = cpgp;
            graf_a_dibuixar = 0;
    }
    
    public int ValorComun() {
            return graf_a_dibuixar;
    }
    
    public void ModificarValorRepresentacio(int i) {
            graf_a_dibuixar = i;
    }
    
    public boolean executaTransformacio(int numDist, double pes, double llindar) throws Exception {
            
            boolean b = true;
            
            CT.TriarAlgorismeDistancia(numDist, CW.obtenirGraf());
            
            if (numDist == 1) CT.parametres_jaro(pes,llindar);
            
            try {
                    CT.AplicarAlgorismeDistancia();
            }
            catch (GrafSenseArrelsException ge) {
                    b = false;
                    throw ge;
            }
            catch (Exception e) {
                    throw e;
            }
            return b;
    }
    
    public void executarCercaComunitats(int numAlg, int numcom, double pesllindar, int kcliques) throws Exception {
            
            CA.InicialitzarCtrl(CT.obtenir_graf(), numAlg);
            
            if (numAlg == 1) {
                    try {
                            CA.PrepararEntrada(CW.obtenirGraf(), kcliques, pesllindar);
                    }
                    catch (Exception e) {
                            throw e;
                    }
            }
            else if (numAlg == 2) {
                    try {
                            CA.PrepararEntrada(numcom);
                    }
                    catch (Exception e) {
                            throw e;
                    }
            }
            else {
                    CA.PrepararEntrada();
            }
            
            CA.ExecutarAlgorisme();
            
            CA.PrepararSortida(CW.obtenirGraf());
    }
    
    public String CarregarComunitat(int i, boolean b) throws DadesIncorrectesException {
            return CA.CarregarComunitat(i, b);
    }
    
    public String CarregarComunitatNext(boolean b) throws DadesIncorrectesException {
            return CA.CarregarComunitatNext(b);
    }
    
    public void AfegirComunitat() {
            CA.AfegirComunitat();
            CPGP.crear_comunitat();
    }
    
    public int ConsultarOrdre(int i) {
            return CA.ConsultarOrdre(i);
    }
    
    public int NombreComunitats() {
            return CA.NombreComunitats();
    }
    
    public Cerca getPanel() {
	Cerca c = new Cerca(this);
	return c;
    }
    
    public long getTime() {
            return CA.getTime();
    }
    
    public String ConsultarAdjacentsANode(String nom, String tipus, int comunitat) throws Exception {
            return CA.ConsultarAdjacentsANode(nom, tipus, comunitat);
    }
    
    public void BorrarNodeComunitat(String nom, String tipus, int comunitat) throws Exception {
            CA.BorrarNodeComunitat(nom, tipus, comunitat);
    }
    
    public void AfegirNodeComunitat(String nom, String tipus, int comunitat1, int comunitat2, String relacions) throws Exception {
            CA.AfegirNodeComunitat(nom, tipus, comunitat2, relacions, CW.obtenirGraf());
            CPGP.Intercanviar_comunitats(nom,tipus,comunitat1, comunitat2);
    }
    
    public void DibuixarComunitats() throws DadesIncorrectesException {
            
            int n = CA.NombreComunitats();
            CPGP.inicialitzar_comunitats(n);
            
            for (int i = 0; i < n; i++) {
            
                    int ordC = CA.ConsultarOrdre(i);
                    String s = new String();
                    int nodes = 0;
                    while (ordC > nodes) {
                            if (nodes == 0) s = CarregarComunitat(i,true);
                            else s = CarregarComunitatNext(true);
                            String[] linies = s.split("\n");
                            for (int j = 0; j < linies.length; j++) {
                                    String[] aux = linies[j].split(" ");
                                    CPGP.Pintar_node_comunitat(i+1,aux[0],aux[1]);
                            }
                            nodes += 100;
                    }
            }
            
    }

}
