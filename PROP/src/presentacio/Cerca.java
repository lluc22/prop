package presentacio;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import domini.DadesIncorrectesException;

public class Cerca extends DosPanels {
        
        CtrlPresentacioCerca CPC;
	JButton buscar, moure, novacom;
	JTextField pes;
	JComboBox comuni;
	String[] ll3 = {"Tria un algorisme", "Jaro-Winkler", "Damerau-Levenshtein", "Distancia Lexica"};
	String[] ll4 = {"Tria un algorisme", "Clique-Percolation", "Newman-Girvan", "Louvain", "Label", "Baumes"};
	
	public Cerca(CtrlPresentacioCerca cpc) {
                
                CPC = cpc;
                
		//Panell LLISTA
		pan2 = new JPanel();
		
		cb1 = new JComboBox();
		cb1.addItem("Tria una comunitat");
		cb1.addItemListener(new combo1());
		cb1.setMinimumSize(new Dimension(200,25));
		cb1.setPreferredSize(new Dimension(250,25));
		cb1.setMaximumSize(new Dimension(300,25));
		pan2.add(cb1);
		
		cb2 = new JComboBox();
		cb2.addItem("Tria una comunitat");
		cb2.addItemListener(new combo2());
		cb2.setMinimumSize(new Dimension(200,25));
		cb2.setPreferredSize(new Dimension(250,25));
		cb2.setMaximumSize(new Dimension(300,25));
		pan2.add(cb2);
		
		llista1 = new JList();
		scroll1 = new JScrollPane(llista1);
		scroll1.setMinimumSize(new Dimension(200,200));
		scroll1.setPreferredSize(new Dimension(250,700));
		scroll1.setMaximumSize(new Dimension(300,800));
                sb1 = new JScrollBar();
                sb1.addAdjustmentListener(new Scrolling1());
                scroll1.setVerticalScrollBar(sb1);
		pan2.add(scroll1);
		
		llista2 = new JList();
		scroll2 = new JScrollPane(llista2);
		scroll2.setMinimumSize(new Dimension(200,200));
		scroll2.setPreferredSize(new Dimension(250,700));
		scroll2.setMaximumSize(new Dimension(300,800));
                sb2= new JScrollBar();
                sb2.addAdjustmentListener(new Scrolling2());
                scroll2.setVerticalScrollBar(sb2);
		pan2.add(scroll2);
		
		moure = new JButton ("Traspassar element ->");
		moure.addActionListener(new traspas());
		pan2.add(moure);
		
		novacom = new JButton("Crear Nova Comunitat");
		novacom.addActionListener(new CrearComunitat());
		pan2.add(novacom);
		
		add(pan2);

		GroupLayout pan2layout = new GroupLayout(pan2);
		pan2.setLayout(pan2layout);
		pan2layout.setHorizontalGroup(
			pan2layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(pan2layout.createSequentialGroup()
				.addGap(10, 15, 20)
				.addGroup(pan2layout.createParallelGroup(GroupLayout.Alignment.LEADING)
					.addComponent(cb1)
					.addComponent(scroll1))
				.addGap(10, 15, 20)
				.addGroup(pan2layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(cb2)
                                        .addComponent(scroll2))
                                .addGap(10, 15, 20))        
                        .addGroup(pan2layout.createSequentialGroup()
                                .addGap(10,20,1000)
                                .addGroup(pan2layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(moure)
                                        .addComponent(novacom))
                                .addGap(10,20,1000))
		);
		pan2layout.setVerticalGroup(
			pan2layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(pan2layout.createSequentialGroup()
                                .addGap(10, 15, 20)
                                .addGroup(pan2layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(cb1)
                                        .addComponent(cb2))
                                .addGap(10, 15, 20)
                                .addGroup(pan2layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(scroll1)
                                        .addComponent(scroll2))
                                .addGap(10, 15, 20)
                                .addComponent(moure)
                                .addGap(10, 15, 20)
                                .addComponent(novacom)
                                .addContainerGap())
		);


		//Panell ALGORISMES
		pan1 = new JPanel();
		
		etiq1 = new JLabel("Algorisme de Distancia:", Label.RIGHT); 
		etiq1.setBackground(Color.yellow);
		pan1.add(etiq1);
		
		cb3 = new JComboBox();
		for (int i = 0; i < ll3.length; i++) {
			cb3.addItem(ll3[i]);
		}
		cb3.addItemListener(new Oient1Combo());
		cb3.setBackground(Color.white);
		cb3.addMouseListener(new Pulsarcbd());
		pan1.add(cb3);
		
		etiq2 = new JLabel("Pes (Enter amb decimals entre 0 i 0.25)", Label.RIGHT);
		pan1.add(etiq2);
		
		pes = new JTextField();
		pes.setVisible(true);
		pes.setEnabled(false);
		pes.setBackground(Color.lightGray);
		pan1.add(pes);
		
		etiq3 = new JLabel("Llindar (Enter amb decimals positiu)", Label.RIGHT);
		pan1.add(etiq3);
		
		text1 = new JTextField();
		text1.setEnabled(false);
		text1.setBackground(Color.lightGray);
		pan1.add(text1);
		
		etiq4 = new JLabel("Alg. Community Detection:", Label.RIGHT);
		pan1.add(etiq4);
		
		cb4 = new JComboBox();
		for (int i = 0; i < ll4.length; i++) {
			cb4.addItem(ll4[i]);
		}
		cb4.addItemListener(new Oient2Combo());
		cb4.setBackground(Color.white);
		cb3.addMouseListener(new Pulsarcba());
		pan1.add(cb4);
		
		etiq5 = new JLabel("Pes Llindar: (Enter entre 0 i 1)", Label.RIGHT);
		pan1.add(etiq5);
		
		text4 = new JTextField();
		text4.setVisible(true);
		text4.setEnabled(false);
		text4.setBackground(Color.lightGray);
		pan1.add(text4);
		
		etiq6 = new JLabel("Cliques: (Enter sense decimals entre 3 i 6)", Label.RIGHT);
		pan1.add(etiq6);
		
		text3 = new JTextField();
		text3.setEnabled(false);
		text3.setBackground(Color.lightGray);
		pan1.add(text3);
		
		etiq7 = new JLabel("Nombre Comunitats: (Enter sense decimals positiu)", Label.RIGHT);
		pan1.add(etiq7);
		
		text2 = new JTextField();
		text2.setEnabled(false);
		text2.setBackground(Color.lightGray);
		pan1.add(text2);
		
		buscar = new JButton ("Buscar Comunitats");
		buscar.addActionListener(new BuscarComunitats());
		pan1.add(buscar);
		
		etiq8 = new JLabel();
		etiq8.setMinimumSize(new Dimension(400,25));
		etiq8.setPreferredSize(new Dimension(500,25));
		etiq8.setMaximumSize(new Dimension(600,25));
		pan1.add(etiq8);
		
		etiq9 = new JLabel("Seleccionar el graf a visualitzar:");
		etiq9.setMinimumSize(new Dimension(250,25));
		etiq9.setPreferredSize(new Dimension(250,25));
		etiq9.setMaximumSize(new Dimension(250,25));
		pan1.add(etiq9);
		
		comuni = new JComboBox();
		comuni.addItem("Graf sencer");
		comuni.addActionListener(new combo3());
		comuni.setMinimumSize(new Dimension(200,25));
		comuni.setPreferredSize(new Dimension(250,25));
		comuni.setMaximumSize(new Dimension(300,25));
		
		InicialitzacioLlistes();
		
		GroupLayout pan1layout = new GroupLayout(pan1);
		pan1.setLayout(pan1layout);
		pan1layout.setHorizontalGroup(
			pan1layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(pan1layout.createSequentialGroup()
				.addGap(10, 20, 30)
				.addGroup(pan1layout.createParallelGroup(GroupLayout.Alignment.LEADING)
					.addComponent(etiq1)
					.addComponent(etiq2)
					.addComponent(etiq3)
					.addComponent(etiq4)
					.addComponent(etiq5)
					.addComponent(etiq6)
					.addComponent(etiq7))
				.addGap(10, 50, 100)
				.addGroup(pan1layout.createParallelGroup(GroupLayout.Alignment.LEADING)
					.addComponent(cb3)
					.addComponent(pes)
					.addComponent(text1)
					.addComponent(cb4)
					.addComponent(text4)
					.addComponent(text3)
					.addComponent(text2))
				.addGap(10, 20, 30))
                        .addGroup(pan1layout.createSequentialGroup()
                                .addGap(10, 20, 1000)
                                .addGroup(pan1layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addComponent(buscar)
                                        .addComponent(etiq8)
                                        .addComponent(etiq9)
                                        .addComponent(comuni))
                                .addGap(10, 20, 1000))
		);
		pan1layout.setVerticalGroup(
			pan1layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(pan1layout.createSequentialGroup()
                                .addGap(10, 50, 100)
                                .addGroup(pan1layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(etiq1)
                                        .addComponent(cb3))
                                .addGap(10, 50, 100)
                                .addGroup(pan1layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(etiq2)
                                        .addComponent(pes))
                                .addGap(10, 50, 100)
                                .addGroup(pan1layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(etiq3)
                                        .addComponent(text1))
                                .addGap(10, 50, 100)
                                .addGroup(pan1layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(etiq4)
                                        .addComponent(cb4))
                                .addGap(10, 50, 100)
                                .addGroup(pan1layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(etiq5)
                                        .addComponent(text4))
                                .addGap(10, 50, 100)
                                .addGroup(pan1layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(etiq6)
                                        .addComponent(text3))
                                .addGap(10, 50, 100)
                                .addGroup(pan1layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(etiq7)
                                        .addComponent(text2))
                                .addGap(10, 50, 100)
                                .addGroup(pan1layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addComponent(buscar))
                                .addGap(10, 50, 100)
                                .addGroup(pan1layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addComponent(etiq8))
                                .addGap(10, 20, 30)
                                .addGroup(pan1layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addComponent(etiq9))
                                .addGap(10, 20, 30)
                                .addGroup(pan1layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addComponent(comuni))
                                .addContainerGap())
		);
		
		add(pan1);
		
		GroupLayout cercalayout = new GroupLayout(this);
		setLayout(cercalayout);
		cercalayout.setHorizontalGroup(
			cercalayout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(cercalayout.createSequentialGroup()
				.addGroup(cercalayout.createParallelGroup(GroupLayout.Alignment.LEADING)
					.addComponent(pan2))
				.addGroup(cercalayout.createParallelGroup(GroupLayout.Alignment.LEADING)
					.addComponent(pan1))
				.addContainerGap())
		);
		cercalayout.setVerticalGroup(
			cercalayout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(cercalayout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addGroup(cercalayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(pan2)
                                        .addComponent(pan1))
                                .addContainerGap())
		);
		
		
		updateUI();
	}
	
	class Pulsarcbd implements MouseListener{
            public void mousePressed(MouseEvent e) {
                cb3.setBackground(Color.white);
            }
            public void mouseEntered(MouseEvent e){}
            public void mouseExited(MouseEvent e){}
            public void mouseReleased(MouseEvent e){}
            public void mouseClicked(MouseEvent e){}
        }
        
        class Pulsarcba implements MouseListener{
            public void mousePressed(MouseEvent e) {
                cb4.setBackground(Color.white);
            }
            public void mouseEntered(MouseEvent e){}
            public void mouseExited(MouseEvent e){}
            public void mouseReleased(MouseEvent e){}
            public void mouseClicked(MouseEvent e){}
        }
	class Oient1Combo implements ItemListener {
	      
		public void itemStateChanged(ItemEvent e) {
			if ((String)cb3.getSelectedItem() == "Jaro-Winkler") {
				pes.setEnabled(true);
				text1.setEnabled(true);
				pes.setBackground(Color.white);
				text1.setBackground(Color.white);
			}
			else {
				pes.setEnabled(false);
				text1.setEnabled(false);
				pes.setBackground(Color.lightGray);
				text1.setBackground(Color.lightGray);
				pes.setText("");
				text1.setText("");
			}
		}
	}
	
	class Oient2Combo implements ItemListener {
	
		public void itemStateChanged(ItemEvent e) {
                        
                        cb4.setBackground(Color.white);
			if ((String)cb4.getSelectedItem() == "Newman-Girvan") {
				text4.setEnabled(false);
				text4.setBackground(Color.lightGray);
				text4.setText("");
				text3.setEnabled(false);
				text3.setBackground(Color.lightGray);
				text3.setText("");
				text2.setEnabled(true);
				text2.setBackground(Color.white);
			}
			else if ((String)cb4.getSelectedItem() == "Clique-Percolation"){
				text4.setEnabled(true);
				text4.setBackground(Color.white);
				text3.setEnabled(true);
				text3.setBackground(Color.white);
				text2.setEnabled(false);
				text2.setBackground(Color.lightGray);
				text2.setText("");
			}
			else {
				text4.setEnabled(false);
				text4.setBackground(Color.lightGray);
				text4.setText("");
				text3.setEnabled(false);
				text3.setBackground(Color.lightGray);
				text3.setText("");
				text2.setEnabled(false);
				text2.setBackground(Color.lightGray);
				text2.setText("");
			}
		}
	}
	
	class BuscarComunitats implements ActionListener {
	
		public void actionPerformed(ActionEvent e) {
                        
                        boolean executar = true;
                        double pesjaro = 0.0;
                        double llindarjaro = 0.0;
                        double llindarcp = 0.0;
                        int cliquepercolation = 0;
                        int numcomnewman = 0;
                        
                        if (cb3.getSelectedIndex() == 0) {
                                cb3.setBackground(Color.red);
                                executar = false;
                        }
                        else if (cb3.getSelectedIndex() == 1) {
                                if (pes.getText().isEmpty() || text1.getText().isEmpty()) {
                                        if (pes.getText().isEmpty()) pes.setBackground(Color.red);
                                        else pes.setBackground(Color.lightGray);
                                        if (text1.getText().isEmpty()) text1.setBackground(Color.red);
                                        else text1.setBackground(Color.lightGray);
                                        executar = false;
                                }
                                else {
                                        pes.setBackground(Color.lightGray);
                                        text1.setBackground(Color.lightGray);
                                        try {
                                                pesjaro = Double.parseDouble(pes.getText());
                                        }
                                        catch (Exception e1) {
                                                executar = false;
                                                pes.setBackground(Color.red);
                                        }
                                        try {
                                                llindarjaro = Double.parseDouble(text1.getText());
                                        }
                                        catch (Exception e2) {
                                                executar = false;
                                                text1.setBackground(Color.red);
                                        }
                                }
                        }
                        
                        if (cb4.getSelectedIndex() == 0) {
                                cb4.setBackground(Color.red);
                                executar = false;
                        }
                        else if (cb4.getSelectedIndex() == 2) {
                                if(text2.getText().isEmpty()) {
                                        text2.setBackground(Color.red);
                                        executar = false;
                                }
                                else {
                                        text2.setBackground(Color.lightGray);
                                        try {
                                                numcomnewman = Integer.parseInt(text2.getText());
                                        }
                                        catch (Exception e3) {
                                                executar = false;
                                                text2.setBackground(Color.red);
                                                etiq8.setForeground(Color.red);
                                                etiq8.setText("L'enter no ha de tenir decimals");
                                        }
                                }
                        }
                        else if (cb4.getSelectedIndex() == 1) {
                                if (text4.getText().isEmpty() || text3.getText().isEmpty()) {
                                        if (text4.getText().isEmpty()) text4.setBackground(Color.red);
                                        else text4.setBackground(Color.lightGray);
                                        if (text3.getText().isEmpty()) text3.setBackground(Color.red);
                                        else text3.setBackground(Color.lightGray);
                                        executar = false;
                                }
                                else {
                                        text4.setBackground(Color.lightGray);
                                        text3.setBackground(Color.lightGray);
                                        try {
                                                cliquepercolation = Integer.parseInt(text3.getText());
                                        }
                                        catch (Exception e4) {
                                                executar = false;
                                                text3.setBackground(Color.red);
                                                etiq8.setForeground(Color.red);
                                                etiq8.setText("L'enter no ha de tenir decimals");
                                                
                                        }
                                        try {
                                                llindarcp = Double.parseDouble(text4.getText());
                                        }
                                        catch (Exception e5) {
                                                executar = false;
                                                text4.setBackground(Color.red);
                                        }
                                }
                        }
                        
                        boolean b = false;
                        
                        if (executar) {
                                try {
                                        b = CPC.executaTransformacio(cb3.getSelectedIndex(),pesjaro,llindarjaro);
                                }
                                catch (Exception e1) {
                                        etiq8.setForeground(Color.red);
                                        etiq8.setText(e1.getMessage());
                                }
                                if (b) {
                                        try {
                                                CPC.executarCercaComunitats(cb4.getSelectedIndex(),numcomnewman,llindarcp,cliquepercolation);
                                        }
                                        catch (Exception e2) {
                                                e2.printStackTrace();
                                                etiq8.setForeground(Color.red);
                                                etiq8.setText(e2.getMessage());
                                                b = false;
                                        }
                                }
                        }
                        
                        if (b) {
                                etiq8.setForeground(Color.green);
                                etiq8.setText("Execució realitzada en: " + CPC.getTime() + " ms");
                                RecarregarLlistes();
                                try {
                                        CPC.DibuixarComunitats();
                                }
                                catch (DadesIncorrectesException de) {
                                        etiq8.setForeground(Color.red);
                                        etiq8.setText(de.getMessage());
                                }
                                pan2.updateUI();
                        }
                        
		}
	}
	
	class combo1 implements ItemListener {
	
		public void itemStateChanged(ItemEvent e) {
                    
                        try {
                                CarregarLlistaEsquerra();
                        }
                        catch (DadesIncorrectesException de) {
                                etiq8.setForeground(Color.red);
                                etiq8.setText(de.getMessage());
                        }
                        
		}
		
        }
        
	class combo2 implements ItemListener {
	
		public void itemStateChanged(ItemEvent e) {
                        
                        try {
                                CarregarLlistaDreta();
                        }
                        catch (DadesIncorrectesException de) {
                                etiq8.setForeground(Color.red);
                                etiq8.setText(de.getMessage());
                        }
                        
		}
		
        }
        
        class combo3 implements ActionListener {
	
		public void actionPerformed(ActionEvent e) {
                        
                        CPC.ModificarValorRepresentacio(comuni.getSelectedIndex());
 
		}
		
        }

	private void InicialitzacioLlistes() {
                
                ent1 = 70;
                ent2 = 70;
                int numcom = CPC.NombreComunitats();
                
                for (int i = 1; i <= numcom; i++) {
                        cb1.addItem("Comunitat " + i);
                        cb2.addItem("Comunitat " + i);
                        comuni.addItem("Comunitat " + i);
                }
                
                comuni.setSelectedIndex(CPC.ValorComun());
                
	}
	
	private void RecarregarLlistes() {
                
                int numcom = CPC.NombreComunitats();
                
                for (int i = cb1.getItemCount()-1; i > 0; i--) {
                        cb1.removeItemAt(i);
                }
                for (int i = cb2.getItemCount()-1; i > 0; i--) {
                        cb2.removeItemAt(i);
                }
                for (int i = comuni.getItemCount()-1; i > 0; i--) {
                        comuni.removeItemAt(i);
                }
                for (int i = 1; i <= numcom; i++) {
                        cb1.addItem("Comunitat " + i);
                        cb2.addItem("Comunitat " + i);
                        comuni.addItem("Comunitat " + i);
                }
                
                
	}
        
	private void CarregarLlistaEsquerra() throws DadesIncorrectesException {
	
                if (cb1.getSelectedIndex() != 0) {
            
                        int ordre = CPC.ConsultarOrdre(cb1.getSelectedIndex()-1);
                        ll1 = new String[ordre];
                        for (int i = 0; i < ordre; i++) ll1[i] = "-";
                        String s = CPC.CarregarComunitat(cb1.getSelectedIndex()-1, true);
                        String[] Linies = s.split("\n");
                        if (ordre > 0) {
                                for (int i = 0; i < Linies.length; i++) {
                                        ll1[i] = Linies[i];
                                }
                        }
                        llista1.setListData(ll1);
                        scroll1.updateUI();
                }
                else {
                        ll1 = new String[0];
                        llista1.setListData(ll1);
                        scroll1.updateUI();
                }
	}
	
	private void CarregarLlistaDreta() throws DadesIncorrectesException {

                if (cb2.getSelectedIndex() != 0) {
                
                        int ordre = CPC.ConsultarOrdre(cb2.getSelectedIndex()-1);
                        ll2 = new String[ordre];
                        for (int i = 0; i < ordre; i++) ll2[i] = "-";
                        String s = CPC.CarregarComunitat(cb2.getSelectedIndex()-1, false);
                        String[] Linies = s.split("\n");
                        if (ordre > 0) {
                                for (int i = 0; i < Linies.length; i++) {
                                        ll2[i] = Linies[i];
                                }
                        }
                        llista2.setListData(ll2);
                        scroll2.updateUI();
                }
                else {
                        ll2 = new String[0];
                        llista2.setListData(ll2);
                        scroll2.updateUI();
                }
	}
	
	private void CarregarLlistaEsquerraNext() throws DadesIncorrectesException {
	
                
                String s = CPC.CarregarComunitatNext(true);
                String[] Linies = s.split("\n");
                for (int i = 0; i < Linies.length; i++) {
                        ll1[i+30+ent1] = Linies[i];
                }
                llista1.setListData(ll1);
                scroll1.updateUI();
                ent1 += 100;
	}
	
	private void CarregarLlistaDretaNext() throws DadesIncorrectesException {
                
                String s = CPC.CarregarComunitatNext(false);
                String[] Linies = s.split("\n");
                for (int i = 0; i < Linies.length; i++) {
                        ll2[i+30+ent2] = Linies[i];
                }
                llista2.setListData(ll2);
                scroll2.updateUI();
                ent2 += 100;
	}
	
	class Scrolling1 implements AdjustmentListener {
        
                public void adjustmentValueChanged(AdjustmentEvent e) {

                        if (ent1 < llista1.getLastVisibleIndex() && CPC.ConsultarOrdre(cb1.getSelectedIndex()-1) > ent1 + 30) {
                                try {
                                        CarregarLlistaEsquerraNext();
                                }
                                catch(DadesIncorrectesException de) {
                                        etiq8.setForeground(Color.red);
                                        etiq8.setText(de.getMessage());
                                }
                        }
                }
        
        }
        
        class Scrolling2 implements AdjustmentListener {
        
                public void adjustmentValueChanged(AdjustmentEvent e) {
        
                        if (ent2 < llista2.getLastVisibleIndex() && CPC.ConsultarOrdre(cb2.getSelectedIndex()-1) > ent2 + 30) {
                                try {
                                        CarregarLlistaDretaNext();
                                }
                                catch(DadesIncorrectesException de) {
                                        etiq8.setForeground(Color.red);
                                        etiq8.setText(de.getMessage());
                                }
                        }
                }
        
        }
        
        class traspas implements ActionListener {
	
		public void actionPerformed(ActionEvent e) {
		
                        if (cb1.getSelectedIndex() == 0 || cb2.getSelectedIndex() == 0) {
                                etiq8.setForeground(Color.red);
                                etiq8.setText("No has seleccionat comunitat");
                        }
                        else {
                                int i = cb1.getSelectedIndex();
                                int j = cb2.getSelectedIndex();
                                if (llista1.getSelectedIndex() == -1) {
                                        etiq8.setForeground(Color.red);
                                        etiq8.setText("No has seleccionat pàgina/categoria");
                                }
                                else {
                                        try {
                                                TraspassarNode(llista1.getSelectedValue().toString(),i,j);
                                                CarregarLlistaEsquerra();
                                                CarregarLlistaDreta();
                                        }
                                        catch (Exception ex) {
                                                etiq8.setForeground(Color.red);
                                                etiq8.setText(ex.getMessage());
                                        }
                                }
                        }
		
		}
		
        }
        
        class CrearComunitat implements ActionListener {
        
                public void actionPerformed(ActionEvent e) {
                
                        CPC.AfegirComunitat();
                        RecarregarLlistes();
                
                }
        
        }
        
        private void TraspassarNode(String s, int com1, int com2) throws Exception {
                String[] aux = s.split(" ");
                String st = CPC.ConsultarAdjacentsANode(aux[0],aux[1],com2);
                CPC.BorrarNodeComunitat(aux[0],aux[1],com1);
                CPC.AfegirNodeComunitat(aux[0],aux[1],com1,com2,st);
        }
}
