package presentacio;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import domini.DadesIncorrectesException;

public class Gestio extends DosPanels {

        JButton AfegirV, EsborrarV, ModificarNomV, AfegirA, EsborrarA, ModAresta;
        String[] llistaV  = {"Tria tipus","Pàgina","Categoria"};
        String[] llistaVo  = {"Tria tipus","Pàgina","Categoria"};
        String[] llistaVd  = {"Tria tipus","Pàgina","Categoria"};
        
        JList llr;
        JScrollPane spr;
        JScrollBar sbr;
        
        int visibleP;
        int visibleC;
        int visibleA;
        
        CtrlPresentacioGestio CPG;

        public Gestio(CtrlPresentacioGestio cpg) {
                CPG = cpg;
                // Panell de gestio
                
                pan2 = new JPanel();
                
                etiq4 = new JLabel("Paràmetres de la Pàgina/Categoria:");
                pan2.add(etiq4);
                
                text1 = new JTextField("Nom");
                text1.setMinimumSize(new Dimension(100,25));
                text1.setMaximumSize(new Dimension(150,25));
                text1.addMouseListener(new PulsarNomV());
                pan2.add(text1);
            
                cb1 = new JComboBox();
                cb1.setMinimumSize(new Dimension(100,25));
                cb1.setMaximumSize(new Dimension(150,25));
                for (int i = 0; i < llistaV.length; i++) {
                        cb1.addItem(llistaV[i]);
                }
                cb1.addMouseListener(new PulsarTipusV());
                pan2.add(cb1);
                
                text2 = new JTextField("Nom nou");
                text2.setMinimumSize(new Dimension(100,25));
                text2.setMaximumSize(new Dimension(150,25));
                text2.addMouseListener(new PulsarNouNomV());
                pan2.add(text2);
                
                etiq5 = new JLabel("Operacions per la Pàgina/Categoria:");
                pan2.add(etiq5);
                
                AfegirV = new JButton("Afegir pàg/cat");
                AfegirV.setMinimumSize(new Dimension(100,25));
                AfegirV.setMaximumSize(new Dimension(150,25));
                AfegirV.addActionListener(new AfegirNode());
                pan2.add(AfegirV);
                
                EsborrarV = new JButton("Eliminar pàg/cat");
                EsborrarV.setMinimumSize(new Dimension(100,25));
                EsborrarV.setMaximumSize(new Dimension(150,25));
                EsborrarV.addActionListener(new EsborrarNode());
                pan2.add(EsborrarV);
                
                ModificarNomV = new JButton("Modificar pàg/cat");
                ModificarNomV.setMinimumSize(new Dimension(150,25));
                ModificarNomV.setMaximumSize(new Dimension(200,25));
                ModificarNomV.addActionListener(new ModificarNode());
                pan2.add(ModificarNomV);
            
                etiq8 = new JLabel();
                etiq8.setMinimumSize(new Dimension(400,25));
                etiq8.setMaximumSize(new Dimension(400,25));
                etiq8.setForeground(Color.red);
                pan2.add(etiq8);
                
                etiq6 = new JLabel("Paràmetres de la Relació:");
                pan2.add(etiq6);
                
                text3 = new JTextField("Pàg/cat origen");
                text3.setMinimumSize(new Dimension(100,25));
                text3.setMaximumSize(new Dimension(150,25));
                text3.addMouseListener(new PulsarVo());
                pan2.add(text3);
                
                cb2 = new JComboBox();
                cb2.setMinimumSize(new Dimension(100,25));
                cb2.setMaximumSize(new Dimension(150,25));
                for (int i = 0; i < llistaVo.length; i++) {
                        cb2.addItem(llistaVo[i]);
                }
                cb2.setBackground(Color.white);
                cb2.addMouseListener(new PulsarTipusVo());
                cb2.addActionListener(new ModificarTipusA1());
                pan2.add(cb2);
                
                cb4 = new JComboBox();
                cb4.setMinimumSize(new Dimension(100,25));
                cb4.setMaximumSize(new Dimension(150,25));
                cb4.addItem("Tipus");
                cb4.setEnabled(false);
                cb4.addMouseListener(new PulsarTipusA());
                cb4.setBackground(Color.white);
                pan2.add(cb4);
                
                text4 = new JTextField("Pàg/Cat destí");
                text4.setMinimumSize(new Dimension(100,25));
                text4.setMaximumSize(new Dimension(150,25));
                text4.addMouseListener(new PulsarVd());
                pan2.add(text4);
                
                cb3 = new JComboBox();
                cb3.setMinimumSize(new Dimension(100,25));
                cb3.setMaximumSize(new Dimension(150,25));
                for (int i = 0; i < llistaVd.length; i++) {
                        cb3.addItem(llistaVd[i]);
                }
                cb3.setBackground(Color.white);
                cb3.addMouseListener(new PulsarTipusVd());
                cb3.addActionListener(new ModificarTipusA2());
                pan2.add(cb3);
            
                etiq7 = new JLabel("Operacions per la Relació:");
                pan2.add(etiq7);
            
                AfegirA = new JButton("Afegir relació");
                AfegirA.setMinimumSize(new Dimension(100,25));
                AfegirA.setMaximumSize(new Dimension(150,25));
                AfegirA.addActionListener(new AfegirAresta());
                pan2.add(AfegirA);
                
                EsborrarA = new JButton("Eliminar relació");
                EsborrarA.setMinimumSize(new Dimension(100,25));
                EsborrarA.setMaximumSize(new Dimension(150,25));
                EsborrarA.addActionListener(new EsborrarAresta());
                pan2.add(EsborrarV);
                
                ModAresta = new JButton("Modificar relació");
                ModAresta.setMinimumSize(new Dimension(150,25));
                ModAresta.setMaximumSize(new Dimension(200,25));
                ModAresta.addActionListener(new ModificarAresta());
                pan2.add(ModAresta);
                
                etiq9 = new JLabel();
                etiq9.setMinimumSize(new Dimension(400,25));
                etiq9.setMaximumSize(new Dimension(400,25));
                etiq9.setForeground(Color.red);
                pan2.add(etiq9);
                
                add(pan2);
                
                GroupLayout pan2layout = new GroupLayout(pan2);
                pan2.setLayout(pan2layout);
                pan2layout.setHorizontalGroup(
                        pan2layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(pan2layout.createSequentialGroup()
                                .addGap(10,15,20)
                                .addGroup(pan2layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addComponent(text1)
                                        .addComponent(AfegirV)
                                        .addComponent(AfegirA))
                                .addGap(10,15,20)
                                .addGroup(pan2layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addComponent(cb1)
                                        .addComponent(EsborrarV)
                                        .addComponent(EsborrarA))
                                .addGap(10,15,20)
                                .addGroup(pan2layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addComponent(text2)
                                        .addComponent(ModificarNomV)
                                        .addComponent(ModAresta))
                                .addContainerGap())
                        .addGroup(pan2layout.createSequentialGroup()
                                .addGap(10,15,20)
                                .addComponent(text3)
                                .addGap(10,15,20)
                                .addComponent(cb2)
                                .addGap(10,15,20)
                                .addComponent(cb4)
                                .addGap(10,15,20)
                                .addComponent(text4)
                                .addGap(10,15,20)
                                .addComponent(cb3)
                                .addGap(10,15,20)
                                .addContainerGap())
                        .addGroup(pan2layout.createSequentialGroup()
                                .addGap(10,15,20)
                                .addComponent(etiq4)
                                .addGap(10,15,20))
                        .addGroup(pan2layout.createSequentialGroup()
                                .addGap(10,15,20)
                                .addComponent(etiq5)
                                .addGap(10,15,20))
                        .addGroup(pan2layout.createSequentialGroup()
                                .addGap(10,15,20)
                                .addComponent(etiq6)
                                .addGap(10,15,20))
                        .addGroup(pan2layout.createSequentialGroup()
                                .addGap(10,15,20)
                                .addComponent(etiq7)
                                .addGap(10,15,20))
                        .addGroup(pan2layout.createSequentialGroup()
                                .addGap(10,20,100)
                                .addComponent(etiq8)
                                .addGap(10,20,100))
                        .addGroup(pan2layout.createSequentialGroup()
                                .addGap(10,20,100)
                                .addComponent(etiq9)
                                .addGap(10,20,100))
                );
                pan2layout.setVerticalGroup(
                        pan2layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(pan2layout.createSequentialGroup()
                                .addGap(10,50,100)
                                .addComponent(etiq4)
                                .addGap(10,20,30)
                                .addGroup(pan2layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addComponent(text1)
                                        .addComponent(cb1)
                                        .addComponent(text2))
                                .addGap(10,50,100)
                                .addComponent(etiq5)
                                .addGap(10,20,30)
                                .addGroup(pan2layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addComponent(AfegirV)
                                        .addComponent(EsborrarV)
                                        .addComponent(ModificarNomV))
                                .addGap(10,20,30)
                                .addComponent(etiq8)
                                .addGap(10,50,100)
                                .addComponent(etiq6)
                                .addGap(10,20,30)
                                .addGroup(pan2layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addComponent(text3)
                                        .addComponent(cb2)
                                        .addComponent(cb4)
                                        .addComponent(text4)
                                        .addComponent(cb3))
                                .addGap(10,50,100)
                                .addComponent(etiq7)
                                .addGap(10,20,30)
                                .addGroup(pan2layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addComponent(AfegirA)
                                        .addComponent(EsborrarA)
                                        .addComponent(ModAresta))
                                .addGap(10,20,30)
                                .addComponent(etiq9)
                                .addGap(10,50,100)
                                .addContainerGap())
                );
                
                // Panell de llistes
                pan1 = new JPanel();
                
                ent1 = -1;
                ent2 = -1;
                
                etiq1 = new JLabel("Pàgines:");
                etiq1.setMinimumSize(new Dimension(100,25));
                etiq1.setMaximumSize(new Dimension(200,25));
                pan1.add(etiq1);
                
                etiq2 = new JLabel("Categories:");
                etiq2.setMinimumSize(new Dimension(100,25));
                etiq2.setMaximumSize(new Dimension(200,25));
                pan1.add(etiq2);
                
                etiq3 = new JLabel("Relacions:");
                etiq3.setMinimumSize(new Dimension(100,25));
                etiq3.setMaximumSize(new Dimension(200,25));
                pan1.add(etiq3);
                
                llista1 = new JList();
                llista1.addListSelectionListener(new CopiarPagina());
                llista1.addMouseListener(new CopiarPaginaAresta());
                scroll2 = new JScrollPane(llista1);
                sb1 = new JScrollBar();
                sb1.addAdjustmentListener(new Scrolling1());
                scroll2.setVerticalScrollBar(sb1);
                pan1.add(scroll2);
                
                llista2 = new JList();
                llista2.addListSelectionListener(new CopiarCategoria());
                llista2.addMouseListener(new CopiarCategoriaAresta());
                scroll1 = new JScrollPane(llista2);
                sb2 = new JScrollBar();
                sb2.addAdjustmentListener(new Scrolling2());
                scroll1.setVerticalScrollBar(sb2);
                pan1.add(scroll1);
                
                llr = new JList();
                llr.addListSelectionListener(new CopiarAresta());
                spr = new JScrollPane(llr);
                sbr = new JScrollBar();
                sbr.addAdjustmentListener(new Scrolling3());
                spr.setVerticalScrollBar(sbr);
                pan1.add(spr);
                
                CarregarInici();
                
                add(pan1);
                
                GroupLayout pan1layout = new GroupLayout(pan1);
                pan1.setLayout(pan1layout);
                pan1layout.setHorizontalGroup(
                        pan1layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(pan1layout.createSequentialGroup()
                                .addGap(10, 20, 30)
                                .addGroup(pan1layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addComponent(etiq1)
                                        .addComponent(scroll2))
                                .addGap(10,20,30)
                                .addGroup(pan1layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addComponent(etiq2)
                                        .addComponent(scroll1))
                                .addGap(10,20,30)
                                .addGroup(pan1layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addComponent(etiq3)
                                        .addComponent(spr))
                                .addGap(10, 20, 30))
                );
                pan1layout.setVerticalGroup(
                        pan1layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(pan1layout.createSequentialGroup()
                                .addGap(10,50,100)
                                .addGroup(pan1layout.createParallelGroup()
                                        .addComponent(etiq1)
                                        .addComponent(etiq2)
                                        .addComponent(etiq3))
                                .addGap(10,20,30)
                                .addGroup(pan1layout.createParallelGroup()
                                        .addComponent(scroll2)
                                        .addComponent(scroll1)
                                        .addComponent(spr))
                                .addGap(10,50,100))
                );
                
                GroupLayout layout = new GroupLayout(this);
                setLayout(layout);
                layout.setHorizontalGroup(
                        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup()
                                    .addComponent(pan1))
                            .addGroup(layout.createParallelGroup()
                                    .addComponent(pan2))
                            .addContainerGap())
                );
                layout.setVerticalGroup(
                        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(30,30,30)
                                .addGroup(layout.createParallelGroup()
                                        .addComponent(pan1)
                                        .addComponent(pan2))
                                .addContainerGap())
                );
        }
        
        class AfegirNode implements ActionListener {
        
                public void actionPerformed(ActionEvent e) {
                        
                        etiq8.setText("");
                        String t = new String();
                        boolean b = true;
                        if (cb1.getSelectedIndex() == 0) {
                                cb1.setBackground(Color.red);
                                b = false;
                        }
                        else if (cb1.getSelectedIndex() == 1) {
                                t = "page";
                        }
                        else {
                                t = "cat";
                        }
                        
                        if (b) {
                                String n = text1.getText();
                                n = TractarNom(n);
                                if (!n.isEmpty()) {
                                        try {
                                                CPG.AfegirNode(n,t);
                                                RecarregarInici();
                                                etiq8.setForeground(Color.green);
                                                etiq8.setText("Element '" + n + "' afegit satisfactòriament");
                                                
                                        }
                                        catch (DadesIncorrectesException de) {
                                                etiq8.setForeground(Color.red);
                                                etiq8.setText(de.getMessage());
                                        }
                                }
                                else {
                                        text1.setBackground(Color.red);
                                }
                        }
                        
		}
		
        }
        
        class EsborrarNode implements ActionListener {
	
		public void actionPerformed(ActionEvent e) {
                        
                        etiq8.setText("");
                        String t = new String();
                        boolean b = true;
                        if (cb1.getSelectedIndex() == 0) {
                                cb1.setBackground(Color.red);
                                b = false;
                        }
                        else if (cb1.getSelectedIndex() == 1) {
                                t = "page";
                        }
                        else {
                                t = "cat";
                        }
                        
                        if (b) {
                                String n = text1.getText();
                                n = TractarNom(n);
                                if (!n.isEmpty()) {
                                        try {
                                                CPG.EsborrarNode(n,t);
                                                RecarregarInici();
                                                etiq8.setForeground(Color.green);
                                                etiq8.setText("Element '" + n + "' esborrat satisfactòriament");
                                        }
                                        catch (DadesIncorrectesException de) {
                                                etiq8.setForeground(Color.red);
                                                etiq8.setText(de.getMessage());
                                        }
                                }
                                else {
                                        text1.setBackground(Color.red);
                                }
                        }
		}
		
        }
        
        class ModificarNode implements ActionListener {
	
		public void actionPerformed(ActionEvent e) {
                        
                        etiq8.setText("");
                        String t = new String();
                        boolean b = true;
                        if (cb1.getSelectedIndex() == 0) {
                                cb1.setBackground(Color.red);
                                b = false;
                        }
                        else if (cb1.getSelectedIndex() == 1) {
                                t = "page";
                        }
                        else {
                                t = "cat";
                        }
                        
                        if (b) {
                                String n = text1.getText();
                                n = TractarNom(n);
                                String nou = text2.getText();
                                nou = TractarNom(nou);
                                if (!n.isEmpty()) {
                                        try {
                                                CPG.ModificarNode(n,t,nou);
                                                RecarregarInici();
                                                etiq8.setForeground(Color.green);
                                                etiq8.setText("Element '" + n + "' modificat per '" + nou + "' satisfactòriament");
                                        }
                                        catch (DadesIncorrectesException de) {
                                                etiq8.setForeground(Color.red);
                                                etiq8.setText(de.getMessage());
                                        }
                                }
                                else {
                                        text1.setBackground(Color.red);
                                }
                        }
		}
		
        }
        
        class AfegirAresta implements ActionListener {
        
                public void actionPerformed(ActionEvent e) {
                        
                        etiq9.setText("");
                        String t1 = new String();
                        boolean b1 = true;
                        if (cb4.getSelectedItem().toString().equals("Tipus")) {
                                cb4.setBackground(Color.red);
                                b1 = false;
                        }
                        else if (cb4.getSelectedItem().toString().equals("CsubC")) {
                                t1 = "CsubC";
                        }
                        else if (cb4.getSelectedItem().toString().equals("CsupC")) {
                                t1 = "CsupC";
                        }
                        else if (cb4.getSelectedItem().toString().equals("CP")) {
                                t1 = "CP";
                        }
                        else if (cb4.getSelectedItem().toString().equals("PC")) {
                                t1 = "PC";
                        }
                        else {
                                t1 = "PP";
                        }
                        
                        boolean b2 = true;
                        String t2 = new String();
                        if (cb2.getSelectedIndex() == 0) {
                                cb2.setBackground(Color.red);
                                b2 = false;
                        }
                        else if (cb2.getSelectedIndex() == 1) {
                                t2 = "page";
                        }
                        else {
                                t2 = "cat";
                        }
                        
                        boolean b3 = true;
                        String t3 = new String();
                        if (cb3.getSelectedIndex() == 0) {
                                cb3.setBackground(Color.red);
                                b3 = false;
                        }
                        else if (cb3.getSelectedIndex() == 1) {
                                t3 = "page";
                        }
                        else {
                                t3 = "cat";
                        }
                        
                        if (b1 && b2 && b3) {
                        
                                String no = text3.getText();
                                if (no.isEmpty()) {
                                        text3.setBackground(Color.red);
                                }
                                String nd = text4.getText();
                                if (nd.isEmpty()) {
                                        text4.setBackground(Color.red);
                                }
                                if (!no.isEmpty() && !nd.isEmpty()) {
                                        no = TractarNom(no);
                                        nd = TractarNom(nd);
                                        try {
                                                CPG.AfegirAresta(no,t2,t1,nd,t3);
                                                RecarregarInici();
                                                etiq9.setForeground(Color.green);
                                                etiq9.setText("Aresta '" + no + "' -> '" + nd + "' afegida satisfactòriament");
                                        }
                                        catch (DadesIncorrectesException de) {
                                                etiq9.setForeground(Color.red);
                                                etiq9.setText(de.getMessage());
                                        }
                                }
                        }
                        

                }
        
        }
        
        class EsborrarAresta implements ActionListener {
        
                public void actionPerformed(ActionEvent e) {
                        
                        etiq9.setText("");
                        String t1 = new String();
                        boolean b1 = true;
                        if (cb4.getSelectedItem().toString().equals("Tipus")) {
                                cb4.setBackground(Color.red);
                                b1 = false;
                        }
                        else if (cb4.getSelectedItem().toString().equals("CsubC")) {
                                t1 = "CsubC";
                        }
                        else if (cb4.getSelectedItem().toString().equals("CsupC")) {
                                t1 = "CsupC";
                        }
                        else if (cb4.getSelectedItem().toString().equals("CP")) {
                                t1 = "CP";
                        }
                        else if (cb4.getSelectedItem().toString().equals("PC")) {
                                t1 = "PC";
                        }
                        else {
                                t1 = "PP";
                        }
                        
                        boolean b2 = true;
                        String t2 = new String();
                        if (cb2.getSelectedIndex() == 0) {
                                cb2.setBackground(Color.red);
                                b2 = false;
                        }
                        else if (cb2.getSelectedIndex() == 1) {
                                t2 = "page";
                        }
                        else {
                                t2 = "cat";
                        }
                        
                        boolean b3 = true;
                        String t3 = new String();
                        if (cb3.getSelectedIndex() == 0) {
                                cb3.setBackground(Color.red);
                                b3 = false;
                        }
                        else if (cb3.getSelectedIndex() == 1) {
                                t3 = "page";
                        }
                        else {
                                t3 = "cat";
                        }
                        
                        if (b1 && b2 && b3) {
                        
                                String no = text3.getText();
                                if (no.isEmpty()) {
                                        text3.setBackground(Color.red);
                                }
                                String nd = text4.getText();
                                if (nd.isEmpty()) {
                                        text4.setBackground(Color.red);
                                }
                                if (!no.isEmpty() && !nd.isEmpty()) {
                                        no = TractarNom(no);
                                        nd = TractarNom(nd);
                                        try {
                                                CPG.EsborrarAresta(no,t2,t1,nd,t3);
                                                RecarregarInici();
                                                etiq9.setForeground(Color.green);
                                                etiq9.setText("Aresta '" + no + "' -> '" + nd + "' esborrada satisfactòriament");
                                        }
                                        catch (DadesIncorrectesException de) {
                                                etiq9.setForeground(Color.red);
                                                etiq9.setText(de.getMessage());
                                        }
                                }
                        }
                }
        
        }
        
        class ModificarAresta implements ActionListener {
        
                public void actionPerformed(ActionEvent e) {
                        
                        etiq9.setText("");
                        String t1 = new String();
                        boolean b1 = true;
                        if (cb4.getSelectedItem().toString().equals("Tipus")) {
                                cb4.setBackground(Color.red);
                                b1 = false;
                        }
                        else if (cb4.getSelectedItem().toString().equals("CsubC")) {
                                t1 = "CsubC";
                        }
                        else if (cb4.getSelectedItem().toString().equals("CsupC")) {
                                t1 = "CsupC";
                        }
                        else if (cb4.getSelectedItem().toString().equals("CP")) {
                                t1 = "CP";
                        }
                        else if (cb4.getSelectedItem().toString().equals("PC")) {
                                t1 = "PC";
                        }
                        else {
                                t1 = "PP";
                        }
                        
                        boolean b2 = true;
                        String t2 = new String();
                        if (cb2.getSelectedIndex() == 0) {
                                cb2.setBackground(Color.red);
                                b2 = false;
                        }
                        else if (cb2.getSelectedIndex() == 1) {
                                t2 = "page";
                        }
                        else {
                                t2 = "cat";
                        }
                        
                        boolean b3 = true;
                        String t3 = new String();
                        if (cb3.getSelectedIndex() == 0) {
                                cb3.setBackground(Color.red);
                                b3 = false;
                        }
                        else if (cb3.getSelectedIndex() == 1) {
                                t3 = "page";
                        }
                        else {
                                t3 = "cat";
                        }
                        
                        if (b1 && b2 && b3) {
                        
                                String no = text3.getText();
                                if (no.isEmpty()) {
                                        text3.setBackground(Color.red);
                                }
                                String nd = text4.getText();
                                if (nd.isEmpty()) {
                                        text4.setBackground(Color.red);
                                }
                                if (!no.isEmpty() && !nd.isEmpty()) {
                                        no = TractarNom(no);
                                        nd = TractarNom(nd);
                                        try {
                                                CPG.ModificarAresta(no,t2,t1,nd,t3);
                                                RecarregarInici();
                                                etiq9.setForeground(Color.green);
                                                etiq9.setText("Aresta '" + no + "' -> '" + nd + "' modificada satisfactòriament");
                                        }
                                        catch (DadesIncorrectesException de) {
                                                etiq9.setForeground(Color.red);
                                                etiq9.setText(de.getMessage());
                                        }
                                }
                        }
                }
        
        }
    
        class CopiarPagina implements ListSelectionListener {
        
                public void valueChanged(ListSelectionEvent e) {
                        
                        if (llista1.getSelectedIndex() > -1) {
                                text1.setText(llista1.getSelectedValue().toString());
                                cb1.setSelectedIndex(1);
                        }
                        
                }
        
        }
        
        class CopiarPaginaAresta implements MouseListener {
        
                public void mousePressed(MouseEvent e) {
                        
                        String s = llista1.getSelectedValue().toString();
                        if (llista1.getSelectedIndex() == ent1 && (cb2.equals("page") || cb3.equals("page"))) {
                                if (cb3.getSelectedIndex() != 0) {
                                        String aux = text3.getText();
                                        text3.setText(text4.getText());
                                        text4.setText(aux);
                                        int aux2 = cb2.getSelectedIndex();
                                        cb2.setSelectedIndex(cb3.getSelectedIndex());
                                        cb3.setSelectedIndex(aux2);
                                }
                        }
                        else {
                                if (cb2.getSelectedIndex() != 0) {
                                        text4.setText(text3.getText());
                                        cb3.setSelectedIndex(cb2.getSelectedIndex());
                                }
                                text3.setText(s);
                                cb2.setSelectedIndex(1);
                        }
                        ent1 = llista1.getSelectedIndex();
                        
                }
                public void mouseEntered(MouseEvent e){}
                public void mouseExited(MouseEvent e){}
                public void mouseReleased(MouseEvent e){}
                public void mouseClicked(MouseEvent e){}
            
        }
        
        class CopiarCategoria implements ListSelectionListener {
        
                public void valueChanged(ListSelectionEvent e) {
                        
                        if (llista2.getSelectedIndex() > -1) {
                                text1.setText(llista2.getSelectedValue().toString());
                                cb1.setSelectedIndex(2);
                        }
                        
                }
        
        }
        
        class CopiarCategoriaAresta implements MouseListener {
        
                public void mousePressed(MouseEvent e) {
                        
                        String s = llista2.getSelectedValue().toString();
                        if (llista2.getSelectedIndex() == ent2 && (cb2.equals("cat") || cb3.equals("cat"))) {
                                if (cb3.getSelectedIndex() != 0) {
                                        String aux = text3.getText();
                                        text3.setText(text4.getText());
                                        text4.setText(aux);
                                        int aux2 = cb2.getSelectedIndex();
                                        cb2.setSelectedIndex(cb3.getSelectedIndex());
                                        cb3.setSelectedIndex(aux2);
                                }
                        }
                        else {
                                if (cb2.getSelectedIndex() != 0) {
                                        text4.setText(text3.getText());
                                        cb3.setSelectedIndex(cb2.getSelectedIndex());
                                }
                                text3.setText(s);
                                cb2.setSelectedIndex(2);
                        }
                        ent2 = llista2.getSelectedIndex();
                }
                public void mouseEntered(MouseEvent e){}
                public void mouseExited(MouseEvent e){}
                public void mouseReleased(MouseEvent e){}
                public void mouseClicked(MouseEvent e){}
            
        }
        
        class CopiarAresta implements ListSelectionListener {
        
                public void valueChanged(ListSelectionEvent e) {
                
                        if (llr.getSelectedIndex() > -1) {
                                String s = llr.getSelectedValue().toString();
                                String[] linies = s.split(" ");
                                text3.setText(linies[0]);
                                text4.setText(linies[3]);
                                if (linies[1].equals("page")) cb2.setSelectedIndex(1);
                                else cb2.setSelectedIndex(2);
                                if (linies[4].equals("page")) cb3.setSelectedIndex(1);
                                else cb3.setSelectedIndex(2);
                                if (linies[2].equals("CsubC")) {
                                        cb4.removeAllItems();
                                        cb4.addItem("CsubC");
                                        cb4.addItem("CsupC");
                                        cb4.setSelectedIndex(0);
                                        cb4.setEnabled(true);
                                }
                                else if (linies[2].equals("CsupC")) {
                                        cb4.removeAllItems();
                                        cb4.addItem("CsubC");
                                        cb4.addItem("CsupC");
                                        cb4.setSelectedIndex(1);
                                        cb4.setEnabled(true);
                                }
                                else if (linies[2].equals("CP")) {
                                        cb4.removeAllItems();
                                        cb4.addItem("CP");
                                        cb4.setSelectedIndex(0);
                                        cb4.setEnabled(false);
                                }
                                else if (linies[2].equals("PC")) {
                                        cb4.removeAllItems();
                                        cb4.addItem("PC");
                                        cb4.setSelectedIndex(0);
                                        cb4.setEnabled(false);
                                }
                                else {
                                        cb4.removeAllItems();
                                        cb4.addItem("PP");
                                        cb4.setSelectedIndex(0);
                                        cb4.setEnabled(false);
                                }
                        }
                
                }
        
        }
        
        private String TractarNom(String nom) {
                String[] linies = nom.split(" ");
                nom = new String(); 
                for (int i = 0; i < linies.length; i++) {
                        if (!linies[i].isEmpty()) {
                                if (!nom.isEmpty()) {
                                        nom += "_" + linies[i];
                                }
                                else nom += linies[i];
                        }
                }
                return nom;
        }
    
        private void CarregarInici() {
                visibleA = 70;
                visibleP = 70;
                visibleC = 70;
                int mida = CPG.mida();
                int ordrePags = CPG.ordrePags();
                int ordreCats = CPG.ordreCats();
                if (mida > 0) {
                        ll3 = new String[mida];
                        for (int i = 0; i < mida; i++) ll3[i] = "-";
                        String s = CPG.CarregarLlistaArestes();
                        String[] linies = s.split("\n");
                        for (int i = 0; i < linies.length; i++) {
                                ll3[i] = linies[i];
                        }
                        llr.setListData(ll3);
                        spr.updateUI();
                }
                spr.setMinimumSize(new Dimension(200,300));
                spr.setPreferredSize(new Dimension(300,800));
                spr.setMaximumSize(new Dimension(400,1000));
                if (ordrePags > 0) {
                        ll1 = new String[ordrePags];
                        for (int i = 0; i < ordrePags; i++) ll1[i] = "-";
                        String s = CPG.CarregarLlistaPagines();
                        String[] linies = s.split("\n");
                        for (int i = 0; i < linies.length; i++) {
                                ll1[i] = linies[i];
                        }
                        llista1.setListData(ll1);
                        scroll2.updateUI();
                }
                scroll2.setMinimumSize(new Dimension(100,300));
                scroll2.setPreferredSize(new Dimension(200,800));
                scroll2.setMaximumSize(new Dimension(300,1000));
                if (ordreCats > 0) {
                        ll2 = new String[ordreCats];
                        for (int i = 0; i < ordreCats; i++) ll2[i] = "-";
                        String s = CPG.CarregarLlistaCategories();
                        String[] linies = s.split("\n");
                        for (int i = 0; i < linies.length; i++) {
                                ll2[i] = linies[i];
                        }
                        llista2.setListData(ll2);
                        scroll1.updateUI();
                }
                scroll1.setMinimumSize(new Dimension(100,300));
                scroll1.setPreferredSize(new Dimension(200,800));
                scroll1.setMaximumSize(new Dimension(300,1000));
        }
        
        private void RecarregarInici() {
                sbr.setValue(0);
                sb1.setValue(0);
                sb2.setValue(0);
                visibleA = 70;
                visibleP = 70;
                visibleC = 70;
                int mida = CPG.mida();
                int ordrePags = CPG.ordrePags();
                int ordreCats = CPG.ordreCats();
                if (mida > 0) {
                        ll3 = new String[mida];
                        for (int i = 0; i < mida; i++) ll3[i] = "-";
                        String s = CPG.CarregarLlistaArestes();
                        String[] linies = s.split("\n");
                        for (int i = 0; i < linies.length; i++) {
                                ll3[i] = linies[i];
                        }
                        llr.setListData(ll3);
                        spr.updateUI();
                }
                else {
                        ll3 = new String[0];
                        llr.setListData(ll3);
                        spr.updateUI();
                }
                if (ordreCats > 0) {
                        ll2 = new String[ordreCats];
                        for (int i = 0; i < ordreCats; i++) ll2[i] = "-";
                        String s = CPG.CarregarLlistaCategories();
                        String[] linies = s.split("\n");
                        for (int i = 0; i < linies.length; i++) {
                                ll2[i] = linies[i];
                        }
                        llista2.setListData(ll2);
                        scroll1.updateUI();
                }
                else {
                        ll2 = new String[0];
                        llista2.setListData(ll2);
                        scroll1.updateUI();
                }
                if (ordrePags > 0) {
                        ll1 = new String[ordrePags];
                        for (int i = 0; i < ordrePags; i++) ll1[i] = "-";
                        String s = CPG.CarregarLlistaPagines();
                        String[] linies = s.split("\n");
                        for (int i = 0; i < linies.length; i++) {
                                ll1[i] = linies[i];
                        }
                        llista1.setListData(ll1);
                        scroll2.updateUI();
                }
                else {
                        ll1 = new String[0];
                        llista1.setListData(ll1);
                        scroll2.updateUI();
                }
        }
        
        private void CarregarMesInfoArestes() {

                String s = CPG.CarregarLlistaArestesNext();
                String[] linies = s.split("\n");
                for (int i = 0; i < linies.length; i++) {
                        ll3[i+visibleA+30] = linies[i];
                }
                llr.setListData(ll3);
                spr.updateUI();
                visibleA += 100;
        
        }
        
        private void CarregarMesInfoPagines() {

                String s = CPG.CarregarLlistaPaginesNext();
                String[] linies = s.split("\n");
                for (int i = 0; i < linies.length; i++) {
                        ll1[i+visibleP+30] = linies[i];
                }
                llista1.setListData(ll1);
                scroll2.updateUI();
                visibleP += 100;
        }
        
        private void CarregarMesInfoCategories() {

                String s = CPG.CarregarLlistaCategoriesNext();
                String[] linies = s.split("\n");
                for (int i = 0; i < linies.length; i++) {
                        ll2[i+visibleC+30] = linies[i];
                }
                llista2.setListData(ll2);
                scroll1.updateUI();
                visibleC += 100;
        }
        
        class PulsarNomV implements MouseListener{
            public void mousePressed(MouseEvent e) {
		String s = text2.getText();
		if (s.equals("")) {
		    text2.setText("Nom nou");
		}
		s = text3.getText();
		if (s.equals("")) {
		    text3.setText("Pàg/cat origen");
		}
		s = text4.getText();
		if (s.equals("")) {
		    text4.setText("Pàg/cat destí");
		}
                text1.setText("");
                text1.setBackground(Color.white);
            }
            public void mouseEntered(MouseEvent e){}
            public void mouseExited(MouseEvent e){}
            public void mouseReleased(MouseEvent e){}
            public void mouseClicked(MouseEvent e){}
        }
        
        class PulsarNouNomV implements MouseListener{
            public void mousePressed(MouseEvent e) {
		String s = text1.getText();
		if (s.equals("")) {
		    text1.setText("Nom");
		}
		s = text3.getText();
		if (s.equals("")) {
		    text3.setText("Pàg/cat origen");
		}
		s = text4.getText();
		if (s.equals("")) {
		    text4.setText("Pàg/cat destí");
		}
		text2.setText("");
		text2.setBackground(Color.white);
            }
            public void mouseEntered(MouseEvent e){}
            public void mouseExited(MouseEvent e){}
            public void mouseReleased(MouseEvent e){}
            public void mouseClicked(MouseEvent e){}
        }
        
        class PulsarVo implements MouseListener{
            public void mousePressed(MouseEvent e) {
		String s = text1.getText();
		if (s.equals("")) {
		    text1.setText("Nom");
		}
		s = text2.getText();
		if (s.equals("")) {
		    text2.setText("Nou Nom");
		}
		s = text4.getText();
		if (s.equals("")) {
		    text4.setText("Pàg/cat destí");
		}
		text3.setText("");
		text3.setBackground(Color.white);
            }
            public void mouseEntered(MouseEvent e){}
            public void mouseExited(MouseEvent e){}
            public void mouseReleased(MouseEvent e){}
            public void mouseClicked(MouseEvent e){}
        }
        
        class PulsarVd implements MouseListener{
            public void mousePressed(MouseEvent e) {
                String s = text1.getText();
                if (s.equals("")) {
		    text1.setText("NomV");
		}
		s = text3.getText();
		if (s.equals("")) {
		    text3.setText("Pàg/cat origen");
		}
		s = text2.getText();
		if (s.equals("")) {
		    text2.setText("Pàg/cat origen");
		}
		text4.setText("");
		text4.setBackground(Color.white);
            }
            public void mouseEntered(MouseEvent e){}
            public void mouseExited(MouseEvent e){}
            public void mouseReleased(MouseEvent e){}
            public void mouseClicked(MouseEvent e){}
        }
        
        class PulsarTipusV implements MouseListener{
            public void mousePressed(MouseEvent e) {
                cb1.setBackground(Color.white);
            }
            public void mouseEntered(MouseEvent e){}
            public void mouseExited(MouseEvent e){}
            public void mouseReleased(MouseEvent e){}
            public void mouseClicked(MouseEvent e){}
        }
        
        class PulsarTipusVo implements MouseListener{
            public void mousePressed(MouseEvent e) {
                cb2.setBackground(Color.white);
            }
            public void mouseEntered(MouseEvent e){}
            public void mouseExited(MouseEvent e){}
            public void mouseReleased(MouseEvent e){}
            public void mouseClicked(MouseEvent e){}
        }
        
        class PulsarTipusA implements MouseListener{
            public void mousePressed(MouseEvent e) {
                cb4.setBackground(Color.white);
            }
            public void mouseEntered(MouseEvent e){}
            public void mouseExited(MouseEvent e){}
            public void mouseReleased(MouseEvent e){}
            public void mouseClicked(MouseEvent e){}
        }
        
        class PulsarTipusVd implements MouseListener{
            public void mousePressed(MouseEvent e) {
                cb3.setBackground(Color.white);
            }
            public void mouseEntered(MouseEvent e){}
            public void mouseExited(MouseEvent e){}
            public void mouseReleased(MouseEvent e){}
            public void mouseClicked(MouseEvent e){}
        }

        class Scrolling1 implements AdjustmentListener {
        
                public void adjustmentValueChanged(AdjustmentEvent e) {
                        if (visibleP < llista1.getLastVisibleIndex() && CPG.ordrePags() > visibleP + 30) {
                                CarregarMesInfoPagines();
                        }
                }
        
        }
        
        class Scrolling2 implements AdjustmentListener {
        
                public void adjustmentValueChanged(AdjustmentEvent e) {
                        if (visibleC < llista2.getLastVisibleIndex() && CPG.ordreCats() > visibleC + 30) {
                                CarregarMesInfoCategories();
                        }
                }
        
        }
        
        class Scrolling3 implements AdjustmentListener {
        
                public void adjustmentValueChanged(AdjustmentEvent e) {
                        if (visibleA < llr.getLastVisibleIndex() && CPG.mida() > visibleA + 30) {
                                CarregarMesInfoArestes();
                        }
                }
        
        }
        
        class ModificarTipusA1 implements ActionListener {
        
                public void actionPerformed(ActionEvent e) {
                
                        if (cb3.getSelectedIndex() == 0 || cb2.getSelectedIndex() == 0) {
                                cb4.removeAllItems();
                                cb4.addItem("Tipus");
                                cb4.setEnabled(false);
                        }
                        else if (cb2.getSelectedIndex() == 1) {
                                cb4.removeAllItems();
                                if (cb3.getSelectedIndex() == 1) {
                                        cb4.addItem("PP");
                                        cb4.setEnabled(false);
                                }
                                else {
                                        cb4.addItem("PC");
                                        cb4.setEnabled(false);
                                }
                        }
                        else {
                                cb4.removeAllItems();
                                if (cb3.getSelectedIndex() == 1) {
                                        cb4.addItem("CP");
                                        cb4.setEnabled(false);
                                }
                                else {
                                        cb4.addItem("CsubC");
                                        cb4.addItem("CsupC");
                                        cb4.setEnabled(true);
                                }
                        }
                
                }
        
        }
        
        class ModificarTipusA2 implements ActionListener {
        
                public void actionPerformed(ActionEvent e) {
                
                        if (cb2.getSelectedIndex() == 0 || cb3.getSelectedIndex() == 0) {
                                cb4.removeAllItems();
                                cb4.addItem("Tipus");
                                cb4.setEnabled(false);
                        }
                        else if (cb3.getSelectedIndex() == 1) {
                                cb4.removeAllItems();
                                if (cb2.getSelectedIndex() == 1) {
                                        cb4.addItem("PP");
                                        cb4.setEnabled(false);
                                }
                                else {
                                        cb4.addItem("CP");
                                        cb4.setEnabled(false);
                                }
                        }
                        else {
                                cb4.removeAllItems();
                                if (cb2.getSelectedIndex() == 1) {
                                        cb4.addItem("PC");
                                        cb4.setEnabled(false);
                                }
                                else {
                                        cb4.addItem("CsubC");
                                        cb4.addItem("CsupC");
                                        cb4.setEnabled(true);
                                }
                        }
                
                }
        
        }
        
}
