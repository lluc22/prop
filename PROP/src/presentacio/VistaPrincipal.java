package presentacio;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class VistaPrincipal extends JFrame {
    JPanel panel;
    CtrlPresentacio CP;
    
    public VistaPrincipal(CtrlPresentacio cp) {
	CP = cp;
	Inici();
    }
    
    private void Inici() {      
      //Inici menu i toolbar
      
     //MENU
      JMenuBar mb=new JMenuBar();
      JMenu fileMenu = new JMenu("Opcions");
      fileMenu.setMnemonic(KeyEvent.VK_O);
      mb.add(fileMenu);
      JMenu MenuArxiu = new JMenu("Arxiu");
      fileMenu.setMnemonic(KeyEvent.VK_A);
      mb.add(MenuArxiu);
      JMenu MenuAjuda = new JMenu("Ajuda");
      fileMenu.setMnemonic(KeyEvent.VK_J);
      mb.add(MenuAjuda);
      
      JMenuItem newMenuItem = new JMenuItem("Modificar Graf", KeyEvent.VK_M);
      newMenuItem.addMouseListener(new NouPanelGG());
      fileMenu.add(newMenuItem);
      JMenuItem newMenuItem2 = new JMenuItem("Buscar Comunitat", KeyEvent.VK_B);
      newMenuItem2.addMouseListener(new NouPanelC());
      fileMenu.add(newMenuItem2);
      JMenuItem newMenuItem3 = new JMenuItem("Càlcul distàncies", KeyEvent.VK_C);
      newMenuItem3.addMouseListener(new NouPanelC());
      fileMenu.add(newMenuItem3);
      JMenuItem newMenuItem4 = new JMenuItem("Modificar Sortida", KeyEvent.VK_S);
      newMenuItem4.addMouseListener(new NouPanelC());
      fileMenu.add(newMenuItem4);
      JMenuItem newMenuItem5 = new JMenuItem("Consultar Graf", KeyEvent.VK_G);
      newMenuItem5.addMouseListener(new NouPanelG());
      fileMenu.add(newMenuItem5);
      
      JMenuItem newMenuItem6 = new JMenuItem("Importar", KeyEvent.VK_I);
      newMenuItem6.addMouseListener(new NouPanelF());
      MenuArxiu.add(newMenuItem6);
      JMenuItem newMenuItem7 = new JMenuItem("Exportar", KeyEvent.VK_E);
      newMenuItem7.addMouseListener(new NouPanelF());
      MenuArxiu.add(newMenuItem7);
      JMenuItem newMenuItem8 = new JMenuItem("...", KeyEvent.VK_A);
     
      final JPanel ajuda = new JPanel();
      JTextArea help = new JTextArea();
      help.setMinimumSize(new Dimension(500,500));
      afegirtext(help);
      ajuda.add(help);
      
      
      GroupLayout ajudalayout = new GroupLayout(ajuda);
      ajuda.setLayout(ajudalayout);
      ajudalayout.setHorizontalGroup(
            ajudalayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(ajudalayout.createSequentialGroup()
                    .addGroup(ajudalayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(help))
                    .addContainerGap())
        );
        ajudalayout.setVerticalGroup(
                ajudalayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(ajudalayout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(ajudalayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(help))
                        .addContainerGap())
        );
      
      newMenuItem8.addMouseListener(new MouseListener() {
	  public void mousePressed(MouseEvent e) {
		remove(panel);
		panel = ajuda;
		add(panel);
		repaint();
		panel.updateUI();
	  }
	  public void mouseEntered(MouseEvent e){}
	  public void mouseExited(MouseEvent e){}
	  public void mouseReleased(MouseEvent e){}
	  public void mouseClicked(MouseEvent e){}
      });

      MenuAjuda.add(newMenuItem8);
      setJMenuBar(mb);
      
      //TOOLBAR
      final JToolBar barra = new JToolBar("barra");
      JButton primer = new JButton("Gestió Graf");
      JButton segon = new JButton("Fitxer");
      JButton tercer = new JButton("Cerca Comunitats");
      JButton quart = new JButton("Graf");
      barra.add(primer);
      barra.add(segon); 
      barra.add(tercer);
      barra.add(quart);
      barra.setFloatable(false); //Per fixar el ToolBar en la Pantalla
      barra.setBounds(0,0,2000,30);
      add(barra);
      
      panel = new JPanel();
      panel = CP.getCPGG().getPanel();     
      add(panel);
      
      setSize(950,700);
      setVisible(true);
      setDefaultCloseOperation(EXIT_ON_CLOSE);
      
      //Accions de menu i toolbar 
      primer.addActionListener(new ActionListener() {
	  public void actionPerformed(ActionEvent e) {
		remove(panel);
		panel = CP.getCPGG().getPanel();
		add(panel);
		setVisible(true);
		repaint();
            }
      });
      
      segon.addActionListener(new ActionListener() {
	  public void actionPerformed(ActionEvent e) {
		remove(panel);
		panel = CP.getCPF().getPanel();
		add(panel);
		setVisible(true);
		repaint();
            }
      });
      
      tercer.addActionListener(new ActionListener() {
	  public void actionPerformed(ActionEvent e) {
		remove(panel);
		panel = CP.getCPC().getPanel();
		add(panel);
		setVisible(true);
		repaint();
            }
      });
      
      quart.addActionListener(new ActionListener() {
	  public void actionPerformed(ActionEvent e) {
		remove(panel);
		panel = CP.getCPG().getPanel(CP.getCPC().ValorComun());
		add(panel);
		setVisible(true);
		repaint();
            }
      });
    }
    
    public void afegirtext(JTextArea help) {
	help.append("GESTIO GRAF: \n");
	help.append("   -Afegir/Eliminar pàg/cat: Necessita el nom del vèrtex(nom) i el tipus (Pàgina o Categoria)\n");
	help.append("   -Modificar pàg/cat: Nomér es pot modificar el nom del vèrtex, el nom que tindrà a partir d'ara és el que hi ha a Nom Nou\n");
	help.append("   -Afegir/Eliminar Relació: Necessita el nom i tipus del vèrtex origen, el nom i tipus del vèrtex desti i el tipus d'enllaç \n");
	help.append("        (No tots els tipus de relació funcionen per a dos tipus de vertexs donats)\n");
	help.append("   -Modificar Relació: Només per passar una relació de CsubC a CsupC i a l'inreves\n");
	help.append("\nFITXER\n");
	help.append("   -Importar: Per carregar un graf que tenim guardat\n");
	help.append("   -Exportar: Per guardar un graf o una comunitat de es que tenim en el programa\n");
	help.append("\nCERCA COMUNITATS:\n");
	help.append("   -Per buscar comunitats s'ha de triar un algorisme de distància i un algorisme de detecció de comunitats.\n");
	help.append("    Alguns dels algorismes necessiten uns paràmetres d'entrada (valors numèrics)\n");
    }
    class NouPanelGG implements MouseListener{
            public void mousePressed(MouseEvent e) {
                remove(panel);
                panel = CP.getCPGG().getPanel();
                add(panel);
                setVisible(true);
                repaint();
            }
            public void mouseEntered(MouseEvent e){}
            public void mouseExited(MouseEvent e){}
            public void mouseReleased(MouseEvent e){}
            public void mouseClicked(MouseEvent e){}
        }
        
    class NouPanelF implements MouseListener{
            public void mousePressed(MouseEvent e) {
                remove(panel);
                panel = CP.getCPF().getPanel();
                add(panel);
                setVisible(true);
                repaint();
            }
            public void mouseEntered(MouseEvent e){}
            public void mouseExited(MouseEvent e){}
            public void mouseReleased(MouseEvent e){}
            public void mouseClicked(MouseEvent e){}
        }
        
        class NouPanelC implements MouseListener{
            public void mousePressed(MouseEvent e) {
                remove(panel);
                panel = CP.getCPC().getPanel();
                add(panel);
                setVisible(true);
                repaint();
            }
            public void mouseEntered(MouseEvent e){}
            public void mouseExited(MouseEvent e){}
            public void mouseReleased(MouseEvent e){}
            public void mouseClicked(MouseEvent e){}
        }
        
        class NouPanelG implements MouseListener{
            public void mousePressed(MouseEvent e) {
                remove(panel);
                panel = CP.getCPG().getPanel(CP.getCPC().ValorComun());
                add(panel);
                setVisible(true);
                repaint();
            }
            public void mouseEntered(MouseEvent e){}
            public void mouseExited(MouseEvent e){}
            public void mouseReleased(MouseEvent e){}
            public void mouseClicked(MouseEvent e){}
        }
		
}
