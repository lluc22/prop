package presentacio;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import domini.ControladorWiki;
import domini.DadesIncorrectesException;


public class CtrlPresentacioGestio {

    
    ControladorWiki CW;
    CtrlPresentacioGrafPanell CPGP;
    
    public CtrlPresentacioGestio (ControladorWiki cw, CtrlPresentacioGrafPanell cpgp) {
            CW = cw;
            CPGP = cpgp;
    }
    
    public Gestio getPanel() {
	Gestio g = new Gestio(this);
	return g;
    }
    
    public void AfegirNode(String nom, String tipus) throws DadesIncorrectesException {
            try {
                    CW.AfegirNode(nom,tipus);
            }
            catch (DadesIncorrectesException e) {
                    throw e;
            }
            CPGP.visualitzar_node(nom,tipus);
    }
    
    public void EsborrarNode(String nom, String tipus) throws DadesIncorrectesException {
            try {
                    CW.EliminarNode(nom,tipus);
            }
            catch (DadesIncorrectesException e) {
                    throw e;
            }
            CPGP.No_visualitzar_node(nom,tipus);
    }
    
    public void ModificarNode(String nom, String tipus, String nomnou) throws DadesIncorrectesException {
            try {
                    CW.ModificarVertex(nom,tipus,nomnou);
            }
            catch (DadesIncorrectesException e) {
                    throw e;
            }
            CPGP.Canviar_nom_node(nom, tipus, nomnou);
    }
    
    public void AfegirAresta(String NodeOrigen, String TipusNodeOrigen, String TipusRelacio, String NodeDesti, String TipusNodeDesti) throws DadesIncorrectesException {
            try {
                    CW.AfegirAresta(NodeOrigen,TipusNodeOrigen,TipusRelacio,NodeDesti,TipusNodeDesti);
            }
            catch (DadesIncorrectesException e) {
                    throw e;
            }
            CPGP.visualitzar_aresta(NodeOrigen,TipusNodeOrigen,TipusRelacio,NodeDesti,TipusNodeDesti);
    }
    
    public void EsborrarAresta(String NodeOrigen, String TipusNodeOrigen, String TipusRelacio, String NodeDesti, String TipusNodeDesti) throws DadesIncorrectesException {
            try {
                    CW.EsborrarAresta(NodeOrigen,TipusNodeOrigen,TipusRelacio,NodeDesti,TipusNodeDesti);
            }
            catch (DadesIncorrectesException e) {
                    throw e;
            }
            CPGP.No_visualitzar_aresta(NodeOrigen,TipusNodeOrigen,TipusRelacio,NodeDesti,TipusNodeDesti);
    }
    
    public void ModificarAresta(String NodeOrigen, String TipusNodeOrigen, String TipusRelacio, String NodeDesti, String TipusNodeDesti) throws DadesIncorrectesException {
            try {
                    CW.ModificarAresta(NodeOrigen,TipusNodeOrigen,TipusRelacio,NodeDesti,TipusNodeDesti);
            }
            catch (DadesIncorrectesException e) {
                    throw e;
            }
            CPGP.Modificar_aresta(NodeOrigen,TipusNodeOrigen,TipusRelacio,NodeDesti,TipusNodeDesti);
    }
    
    public String CarregarLlistaArestes() {
            return CW.CarregarPrimerBlocLinies();
    }
    
    public String CarregarLlistaArestesNext() {
            return CW.CarregarBlocLiniesNext();
    }
    
    public int mida() {
            return CW.mida();
    }
    
    public String CarregarLlistaPagines() {
            return CW.CarregarPrimerBlocPagines();
    }
    
    public String CarregarLlistaPaginesNext() {
            return CW.CarregarBlocPaginesNext();
    }
    
    public int ordrePags() {
            return CW.ordrePags();
    }
    
    public String CarregarLlistaCategories() {
            return CW.CarregarPrimerBlocCategories();
    }
    
    public String CarregarLlistaCategoriesNext() {
            return CW.CarregarBlocCategoriesNext();
    }
    
    public int ordreCats() {
            return CW.ordreCats();
    }
    
    //public String[] bloc

}
